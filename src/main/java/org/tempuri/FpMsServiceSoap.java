/**
 * FpMsServiceSoap.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package org.tempuri;

public interface FpMsServiceSoap extends java.rmi.Remote {
    public String sendSMSwithUserData(String dstAddr, String text, String user, String pass, String scheduleTime, String stoptime, boolean isLong, String data1, String data2, String data3, String data4) throws java.rmi.RemoteException;
    public String sendSMS(String dstAddr, String text, String user, String pass, String scheduleTime, String stoptime, boolean isLong) throws java.rmi.RemoteException;
    public String sendDyncSMSwithUserData(String dstAddr, String text, String user, String pass, String scheduleTime, String stoptime, String srcCell, boolean isLong, String data1, String data2, String data3, String data4) throws java.rmi.RemoteException;
    public String sendDyncSMS(String dstAddr, String text, String user, String pass, String scheduleTime, String stoptime, String srcCell, boolean isLong) throws java.rmi.RemoteException;
    public String sendDyncSMSwithUserDataAndPriority(String dstAddr, String text, String user, String pass, String scheduleTime, String stoptime, String srcCell, boolean isLong, String data1, String data2, String data3, String data4, boolean hiPriority) throws java.rmi.RemoteException;
    public String sendDyncSMSWithPriority(String dstAddr, String text, String user, String pass, String scheduleTime, String stoptime, String srcCell, boolean isLong, boolean hiPriority) throws java.rmi.RemoteException;
    public String querySMS(String user, String pass, String seq_no) throws java.rmi.RemoteException;
    public String SMSQueryDr(String user, String pass, String seq_no) throws java.rmi.RemoteException;
    public String querySMSWithSendTime(String user, String pass, String seq_no) throws java.rmi.RemoteException;
    public String querySMSStatusByTimeRange(String user, String pass, String startTime, String endTime) throws java.rmi.RemoteException;
    public String queryReceivedSMS(String user, String pass, String cellId) throws java.rmi.RemoteException;
    public String queryReceivedSMSByTimeRange(String user, String pass, String cellId, String startTime, String endTime) throws java.rmi.RemoteException;
    public String queryUserPoint(String user, String pass) throws java.rmi.RemoteException;
    public String scheduleSMSCancel(String user, String pass, String seq_no) throws java.rmi.RemoteException;
}
