package org.tempuri;

public class FpMsServiceSoapProxy implements org.tempuri.FpMsServiceSoap {
  private String _endpoint = null;
  private org.tempuri.FpMsServiceSoap fpMsServiceSoap = null;
  
  public FpMsServiceSoapProxy() {
    _initFpMsServiceSoapProxy();
  }
  
  public FpMsServiceSoapProxy(String endpoint) {
    _endpoint = endpoint;
    _initFpMsServiceSoapProxy();
  }
  
  private void _initFpMsServiceSoapProxy() {
    try {
      fpMsServiceSoap = (new org.tempuri.FpMsServiceLocator()).getfpMsServiceSoap();
      if (fpMsServiceSoap != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)fpMsServiceSoap)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)fpMsServiceSoap)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (fpMsServiceSoap != null)
      ((javax.xml.rpc.Stub)fpMsServiceSoap)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public org.tempuri.FpMsServiceSoap getFpMsServiceSoap() {
    if (fpMsServiceSoap == null)
      _initFpMsServiceSoapProxy();
    return fpMsServiceSoap;
  }
  
  public String sendSMSwithUserData(String dstAddr, String text, String user, String pass, String scheduleTime, String stoptime, boolean isLong, String data1, String data2, String data3, String data4) throws java.rmi.RemoteException{
    if (fpMsServiceSoap == null)
      _initFpMsServiceSoapProxy();
    return fpMsServiceSoap.sendSMSwithUserData(dstAddr, text, user, pass, scheduleTime, stoptime, isLong, data1, data2, data3, data4);
  }

  public String sendSMS(String dstAddr, String text, String user, String pass, String scheduleTime, String stoptime, boolean isLong) throws java.rmi.RemoteException{
    if (fpMsServiceSoap == null)
      _initFpMsServiceSoapProxy();
    return fpMsServiceSoap.sendSMS(dstAddr, text, user, pass, scheduleTime, stoptime, isLong);
  }

  public String sendDyncSMSwithUserData(String dstAddr, String text, String user, String pass, String scheduleTime, String stoptime, String srcCell, boolean isLong, String data1, String data2, String data3, String data4) throws java.rmi.RemoteException{
    if (fpMsServiceSoap == null)
      _initFpMsServiceSoapProxy();
    return fpMsServiceSoap.sendDyncSMSwithUserData(dstAddr, text, user, pass, scheduleTime, stoptime, srcCell, isLong, data1, data2, data3, data4);
  }

  public String sendDyncSMS(String dstAddr, String text, String user, String pass, String scheduleTime, String stoptime, String srcCell, boolean isLong) throws java.rmi.RemoteException{
    if (fpMsServiceSoap == null)
      _initFpMsServiceSoapProxy();
    return fpMsServiceSoap.sendDyncSMS(dstAddr, text, user, pass, scheduleTime, stoptime, srcCell, isLong);
  }

  public String sendDyncSMSwithUserDataAndPriority(String dstAddr, String text, String user, String pass, String scheduleTime, String stoptime, String srcCell, boolean isLong, String data1, String data2, String data3, String data4, boolean hiPriority) throws java.rmi.RemoteException{
    if (fpMsServiceSoap == null)
      _initFpMsServiceSoapProxy();
    return fpMsServiceSoap.sendDyncSMSwithUserDataAndPriority(dstAddr, text, user, pass, scheduleTime, stoptime, srcCell, isLong, data1, data2, data3, data4, hiPriority);
  }

  public String sendDyncSMSWithPriority(String dstAddr, String text, String user, String pass, String scheduleTime, String stoptime, String srcCell, boolean isLong, boolean hiPriority) throws java.rmi.RemoteException{
    if (fpMsServiceSoap == null)
      _initFpMsServiceSoapProxy();
    return fpMsServiceSoap.sendDyncSMSWithPriority(dstAddr, text, user, pass, scheduleTime, stoptime, srcCell, isLong, hiPriority);
  }

  public String querySMS(String user, String pass, String seq_no) throws java.rmi.RemoteException{
    if (fpMsServiceSoap == null)
      _initFpMsServiceSoapProxy();
    return fpMsServiceSoap.querySMS(user, pass, seq_no);
  }

  public String SMSQueryDr(String user, String pass, String seq_no) throws java.rmi.RemoteException{
    if (fpMsServiceSoap == null)
      _initFpMsServiceSoapProxy();
    return fpMsServiceSoap.SMSQueryDr(user, pass, seq_no);
  }

  public String querySMSWithSendTime(String user, String pass, String seq_no) throws java.rmi.RemoteException{
    if (fpMsServiceSoap == null)
      _initFpMsServiceSoapProxy();
    return fpMsServiceSoap.querySMSWithSendTime(user, pass, seq_no);
  }

  public String querySMSStatusByTimeRange(String user, String pass, String startTime, String endTime) throws java.rmi.RemoteException{
    if (fpMsServiceSoap == null)
      _initFpMsServiceSoapProxy();
    return fpMsServiceSoap.querySMSStatusByTimeRange(user, pass, startTime, endTime);
  }

  public String queryReceivedSMS(String user, String pass, String cellId) throws java.rmi.RemoteException{
    if (fpMsServiceSoap == null)
      _initFpMsServiceSoapProxy();
    return fpMsServiceSoap.queryReceivedSMS(user, pass, cellId);
  }

  public String queryReceivedSMSByTimeRange(String user, String pass, String cellId, String startTime, String endTime) throws java.rmi.RemoteException{
    if (fpMsServiceSoap == null)
      _initFpMsServiceSoapProxy();
    return fpMsServiceSoap.queryReceivedSMSByTimeRange(user, pass, cellId, startTime, endTime);
  }

  public String queryUserPoint(String user, String pass) throws java.rmi.RemoteException{
    if (fpMsServiceSoap == null)
      _initFpMsServiceSoapProxy();
    return fpMsServiceSoap.queryUserPoint(user, pass);
  }

  public String scheduleSMSCancel(String user, String pass, String seq_no) throws java.rmi.RemoteException{
    if (fpMsServiceSoap == null)
      _initFpMsServiceSoapProxy();
    return fpMsServiceSoap.scheduleSMSCancel(user, pass, seq_no);
  }
  
  
}