package tw.tw360.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Map;

import javax.annotation.Resource;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import org.springside.modules.orm.Page;

import tw.tw360.dto.ExchangeRecord;
import tw.tw360.dto.MemberHome;
import tw.tw360.dto.view.GoodStuffs;

@Repository
public class ExchangeDao extends BaseDao {
	
	@Resource(name = "ds8888playQry")
	private DataSource ds8888playQry;

	@Resource(name = "ds8888playUpd")
	private DataSource ds8888playUpd;

	@Autowired
	public ExchangeDao(@Qualifier("ds8888playUpd") DataSource dataSource) {
		setDataSource(dataSource);
	}

	public String exchange(String uid, long id, String deviceId) {
		return null;
	}

	public int userTotalBonus(String uid) {
		String sql = "SELECT COALESCE(SUM(`cost`),0) FROM exchange_record  WHERE userId=?";
		return super.queryForInt(ds8888playQry, sql, new Object[]{uid});
	}

	public ExchangeRecord addExchangeRecord(ExchangeRecord exchangeRecord) {
		String sql = "INSERT INTO `exchange_record` (`prizeId`, `prizeSnId`, `userId`, `cost`, `sourceIp`, `deviceId`, `createTime`) VALUES (?, ?, ?, ?, ?, ?, NOW())";
		return (ExchangeRecord)super.addForObject(ds8888playUpd, sql, exchangeRecord, new Object[]{exchangeRecord.getPrizeId(),exchangeRecord.getPrizeSnId(),
				exchangeRecord.getUserId(),exchangeRecord.getCost(),exchangeRecord.getSourceIp(),exchangeRecord.getDeviceId()});
	}
		
	@SuppressWarnings("unchecked")
	public Page<GoodStuffs> findAll(Page<GoodStuffs> page, int pageNo, int pageSize, String uid, int type) {
		StringBuffer strSQL = new StringBuffer("SELECT (@row_number :=@row_number + 1) `order`, a.* FROM ( SELECT psn.id, p.gameCode, psn.sn, p.`desc`, p.limitTime, ( SELECT gameName FROM t_games WHERE gameCode = p.gameCode ) `gameName`, er.createTime FROM prize p, prize_sn psn, exchange_record er WHERE psn.prizeId = p.id AND er.prizeId = p.id AND er.userId=? AND psn.userId=? ");
		
		if(type > 0){
			strSQL.append(" and p.type="+type);
		}
				
		strSQL.append(" group by p.gameCode, psn.sn ) a, (SELECT @row_number := 0) t order by  a.createTime desc");
		
		StringBuilder pageSQL = new StringBuilder("SELECT COUNT(*) FROM (").append(strSQL).append(") x");
		
		strSQL.append(String.format(" limit %s, %s", (pageNo - 1) * pageSize, pageSize));

		page = queryForPage(ds8888playQry, page, strSQL.toString(), pageSQL.toString(), new Object[] { uid, uid }, new GoodStuffsMapper());

		return page;
	}

	@SuppressWarnings("unchecked")
	public Map<String, Object> findExchangeDetail(String uid, long prizeId) {
		String sql = "SELECT prize.gameCode, gameName name, iconUrl, cost, content giftContent, `usage`, "
				+ "CASE IFNULL(d.id,'-1') WHEN '-1' THEN '1' ELSE '2' END AS `status` "
				+ " FROM prize LEFT JOIN (SELECT id, prizeId FROM exchange_record WHERE userId = ? AND prizeId = ?) d ON prize.id = d.prizeId JOIN "
				+ "(SELECT gameCode, gameName FROM t_games) g ON prize.gameCode = g.gameCode WHERE prize.id = ?";
		return super.queryForMap(ds8888playQry, sql, new Object[] { uid, prizeId, prizeId });
	}

	public Integer getBonusBalence(String uid) {
		String sqlCredits = "SELECT COALESCE(sum(bonus),0) bonus FROM (SELECT bonus FROM bonus_record br WHERE br.userId = ?)r";
		MemberHome credits = (MemberHome) queryForObject(ds8888playQry, sqlCredits, new Object[] { uid }, new BeanPropertyRowMapper<MemberHome>(
				MemberHome.class));
		String sqlDebits = "SELECT COALESCE(sum(cost),0) bonus FROM (SELECT cost FROM exchange_record br WHERE br.userId = ?)r";
		MemberHome debits = (MemberHome) queryForObject(ds8888playQry, sqlDebits, new Object[] { uid }, new BeanPropertyRowMapper<MemberHome>(MemberHome.class));
		if (null != debits.getBonus()) {
			return credits.getBonus() - debits.getBonus();
		} else {
			return credits.getBonus();
		}
	}
	
	@SuppressWarnings("unchecked")
	public Page<tw.tw360.dto.ExchangeList> exchangeList(String uid, Integer pageNo, Integer pageSize) {
		StringBuffer sql = new StringBuffer(
				"SELECT p.id id, (@row_number:=@row_number + 1) `order`, p.gameCode, p.iconUrl, IF (er.cost, er.cost, p.cost) cost, p.`DESC` NAME, p.content giftContent, IF (er.cost, 2, 1) STATUS FROM (select @row_number:=0) t, prize p LEFT JOIN exchange_record er ON p.id = er.prizeId AND er.userId = ? WHERE p.type = 3 ");
		StringBuffer sqlCnt = new StringBuffer(
				"SELECT COUNT(p.id) FROM prize p LEFT JOIN exchange_record er ON p.id = er.prizeId AND er.userId = ? WHERE p.type = 3 ");
		sql.append(" LIMIT " + (pageNo - 1) * pageSize + "," + pageSize);
		Page<tw.tw360.dto.ExchangeList> page = new Page<tw.tw360.dto.ExchangeList>(pageSize);
		page.setPageNo(pageNo);
		super.queryForPage(ds8888playQry, page, sql.toString(), sqlCnt.toString(), new Object[] { uid }, new BeanPropertyRowMapper<tw.tw360.dto.ExchangeList>(
				tw.tw360.dto.ExchangeList.class));
		return page;
	}
	
	@SuppressWarnings("unchecked")
	public ExchangeRecord findExchangeRecord(String uid, long prizeId) {
		String sql = "select * from exchange_record where prizeId=? and userId=? limit 1";
		
		return (ExchangeRecord) queryForObject(ds8888playQry, sql, new Object[]{prizeId, uid}, ExchangeRecord.class);
	}
	
	private class GoodStuffsMapper implements RowMapper<Object> {
		public GoodStuffs mapRow(ResultSet rs, int rowNum) throws SQLException {
			GoodStuffs view = new GoodStuffs();

			view.setId(rs.getLong("id"));
			view.setOrder(rs.getInt("order"));
			view.setGameCode(rs.getString("gameCode"));
			view.setGameName(rs.getString("gameName"));
			view.setDesc(rs.getString("desc"));
			view.setSn(rs.getString("sn"));
			view.setTime(rs.getDate("limitTime"));

			return view;
		}
	}
	
}