package tw.tw360.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.sql.DataSource;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import org.springside.modules.orm.Page;

import tw.tw360.constants.Config;
import tw.tw360.dto.view.Announce;
import tw.tw360.dto.view.Announces;

@SuppressWarnings("rawtypes")
@Repository
public class AnnouncementDao extends BaseDao {
	
	@Resource(name = "ds8888playQry")
	private DataSource ds8888playQry;

	@Resource(name = "ds8888playUpd")
	private DataSource ds8888playUpd;

	@Autowired
	public AnnouncementDao(@Qualifier("ds8888playUpd") DataSource dataSource) {
		setDataSource(dataSource);
	}
	
	@SuppressWarnings({ "unchecked" })
	public Page<Announces> search(Page<Announces> page, int pageNo, int pageSize, int type, String gameCode) {
		StringBuffer query = new StringBuffer();
		StringBuffer count = new StringBuffer();
		
		List<Object> params = new ArrayList<Object>();
		StringBuffer where = new StringBuffer();
		query.append("select a.* from (select g.gameName, an.* , (@row_number:=@row_number + 1) `order` from announcement an left join (select DISTINCT gameName, gameCode from t_games where isOnline='是') g on an.gameCode=g.gameCode, (select @row_number:=0) t) a where ifnull(a.gameName,'') != '' and a.status=1 and a.startTime <= now() and (a.endTime is null or a.endTime >= now())");
		count.append("select count(*) from (select g.gameName, an.* from announcement an left join (select DISTINCT gameName, gameCode from t_games where isOnline='是') g on an.gameCode=g.gameCode, (select @row_number:=0) t ) a where ifnull(a.gameName,'') != '' and a.status=1 and a.startTime <= now() and (a.endTime is null or a.endTime >= now())");
		
		if(type != 0) {
			where.append(" and a.`type`=?");
			params.add(type);
		}
		
		if(StringUtils.isNotBlank(gameCode)) {
			where.append(" and a.gameCode=?");
			params.add(gameCode);
		}
		
		query.append(where);
		count.append(where);
		
		if(pageNo <= 0) {
			pageNo = 1;
		}
		
		query.append(" order by a.startTime desc");
		query.append(String.format(" limit %s, %s", (pageNo - 1) * pageSize, pageSize));
		
		return queryForPage(ds8888playQry, page, query.toString(), count.toString(), params.toArray(), new AnnouncesMapper());				
	}
	
	public Announce get(int id, String announceUrl) {
		String sql = "select (select gameName from t_games where gameCode=a.gameCode) gameName, a.* from announcement a where id=?";
		
		return (Announce) queryForObject(ds8888playQry, sql, new Object[]{id}, new AnnounceMapper(announceUrl));
	}
	
	@SuppressWarnings("unchecked")
	public List<Announces> queryHomeAnnouncement(){
		String sql = "SELECT g.gameName, (@row_number :=@row_number + 1) `order`, a.* FROM announcement a left join (select distinct gameName, gameCode from t_games where isOnline='是') g on a.gameCode=g.gameCode, (SELECT @row_number := 0) t WHERE STATUS = 1 and IFNULL(g.gameName,'') != '' AND startTime <= now() AND (endTime IS NULL OR endTime >= now()) ORDER BY COALESCE (orderIndex, 999),	createTime DESC LIMIT ?";
		return this.queryForList(ds8888playQry, sql, new Object[]{Config.ANNOUNCEMENT_SIZE}, new AnnouncesMapper());
	}
	
	private class AnnouncesMapper implements RowMapper<Object> {
		public Announces mapRow(ResultSet rs, int rowNum) throws SQLException {
			Announces view = new Announces();
			
			view.setId(rs.getLong("id"));
			view.setType(rs.getInt("type"));
			view.setOrder(rs.getInt("order"));
			view.setTitle(String.format("%s", rs.getString("title")));
			view.setDate(rs.getDate("startTime"));
			view.setGameName(rs.getString("gameName"));
			
			return view;
		}
	}
	
	private class AnnounceMapper implements RowMapper<Object> {
		
		private String announceUrl;
		
		public AnnounceMapper(String announceUrl) {
			this.announceUrl = announceUrl;
		}
		
		public Announce mapRow(ResultSet rs, int rowNum) throws SQLException {
			Announce view = new Announce();
			
			view.setType(rs.getInt("type"));
			view.setTitle(rs.getString("title"));
			// thisId=3361&gameCode=xajh
			view.setContentUrl(String.format("%s?thisId=%s&gameCode=%s", announceUrl, rs.getLong("id"), rs.getString("gameCode")));
			view.setDate(rs.getDate("startTime"));
			view.setGameName(rs.getString("gameName"));
			
			return view;
		}
	}
}