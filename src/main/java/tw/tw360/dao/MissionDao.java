package tw.tw360.dao;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.annotation.Resource;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;
import org.springside.modules.orm.Page;

import tw.tw360.dto.Mission;

@Repository
@PropertySource("classpath:mission.properties")
public class MissionDao extends BaseDao {

	@Resource(name = "ds8888playQry")
	private DataSource ds8888playQry;

	@Resource(name = "ds8888playUpd")
	private DataSource ds8888playUpd;

	@Autowired
	private Environment environment;

	@Autowired
	public MissionDao(@Qualifier("ds8888playUpd") DataSource dataSource) {
		setDataSource(dataSource);
	}

	public Page<Mission> findAllMissionByUserid(String uid, Integer pageNo, Integer pageSize) {

		String dailyBonusId = environment.getProperty("dailyBonus");
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		String currentDay = dateFormat.format(new Date());

		StringBuffer sql = new StringBuffer(
				"SELECT @rownum := @rownum + 1 `order`, t.* FROM (SELECT m.id, m.order_index, m.iconUrl, IF (br.bonus, br.bonus, m.bonus) bonus, m.content, IF (br.bonus, 2, 1) STATUS, m.gameCode, gr.iosUrl FROM mission m LEFT JOIN bonus_record br ON m.id = br.missionId AND br.userId = ? LEFT JOIN game_record gr ON m.gameCode = gr.gameCode WHERE m.id <> "
						+ dailyBonusId
						+ " UNION ALL SELECT m.id, m.order_index, m.iconUrl, IF (br.bonus, br.bonus, m.bonus) bonus, m.content, IF (br.bonus, 2, 1) STATUS, m.gameCode, gr.iosUrl FROM mission m LEFT JOIN bonus_record br ON m.id = br.missionId AND br.userId = ? LEFT JOIN game_record gr ON m.gameCode = gr.gameCode WHERE m.id = "
						+ dailyBonusId
						+ " AND br.createTime BETWEEN '"
						+ currentDay
						+ " 00:00:00' AND '"
						+ currentDay
						+ " 23:59:59') t, (SELECT @rownum := 0) r");
		StringBuffer sqlCnt = new StringBuffer(
				"SELECT COUNT(*) FROM (SELECT m.id, m.order_index, m.iconUrl, IF (br.bonus, br.bonus, m.bonus) bonus, m.content, IF (br.bonus, 2, 1) STATUS, m.gameCode, gr.iosUrl FROM mission m LEFT JOIN bonus_record br ON m.id = br.missionId AND br.userId = ? LEFT JOIN game_record gr ON m.gameCode = gr.gameCode WHERE m.id <> "
						+ dailyBonusId
						+ " UNION ALL SELECT m.id, m.order_index, m.iconUrl, IF (br.bonus, br.bonus, m.bonus) bonus, m.content, IF (br.bonus, 2, 1) STATUS, m.gameCode, gr.iosUrl FROM mission m LEFT JOIN bonus_record br ON m.id = br.missionId AND br.userId = ? LEFT JOIN game_record gr ON m.gameCode = gr.gameCode WHERE m.id = "
						+ dailyBonusId + " AND br.createTime BETWEEN '" + currentDay + " 00:00:00' AND '" + currentDay + " 23:59:59' ) a");
		sql.append(" LIMIT " + (pageNo - 1) * pageSize + "," + pageSize);
		Page<Mission> page = new Page<Mission>(pageSize);
		page.setPageNo(pageNo);
		super.queryForPage(ds8888playQry, page, sql.toString(), sqlCnt.toString(), new Object[] { uid, uid }, new BeanPropertyRowMapper(Mission.class));
		return page;
	}

	public String findTypeByGameCode(String gameCode) {
		StringBuffer sql = new StringBuffer("SELECT TYPE FROM game_record WHERE gameCode = ?");
		String type = super.queryForString(ds8888playQry, sql.toString(), new Object[] { gameCode });
		return type;
	}

	public boolean isDiamondVip(String uid) {
		StringBuffer sql = new StringBuffer("SELECT vipLevel FROM t_pf_vip WHERE uid = ?");
		String type = super.queryForString(ds8888playQry, sql.toString(), new Object[] { uid });
		if ("鑽石VIP".equals(type)) {
			return true;
		}
		return false; 
	}
	
	public int getPhoneFirstBindMission(){
		String sql = "select id from mission where status = 1 and type=4 order by createTime DESC limit 1";
		return this.queryForInt(ds8888playQry, sql, null);
	}
	
}