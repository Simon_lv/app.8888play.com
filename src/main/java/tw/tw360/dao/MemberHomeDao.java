package tw.tw360.dao;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;

import tw.tw360.utils.MD5Util;
import tw.tw360.constants.Config;
import tw.tw360.dto.BaseDto;
import tw.tw360.dto.MemberHome;
import tw.tw360.dto.view.AppVersionControl;
import tw.tw360.dto.view.User;
import tw.tw360.exception.NoBondFailException;

import javax.annotation.Resource;
import javax.sql.DataSource;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

@Repository
public class MemberHomeDao extends BaseDao<Object> {
	
	private static final Logger LOG = LoggerFactory.getLogger(MemberHomeDao.class);

	@Resource(name = "ds8888playQry")
	private DataSource ds8888playQry;

	@Resource(name = "ds8888playUpd")
	private DataSource ds8888playUpd;

	@Autowired
	public MemberHomeDao(@Qualifier("ds8888playUpd") DataSource dataSource) {
		setDataSource(dataSource);
	}

	public MemberHome findInfo(String uid) {
		String sql = "SELECT IFNULL (u.userName, '') userName , IFNULL(v.vipLevel, '一般') `level`, IFNULL (u.telephone, '') mobile, IFNULL (u.email, '') email FROM t_users u LEFT JOIN t_pf_vip v ON v.uid = u.userId WHERE u.userid=? LIMIT 1";
		return (MemberHome) queryForObject(ds8888playQry, sql, new Object[] { uid }, new BeanPropertyRowMapper<MemberHome>(MemberHome.class));
	}
	
	public MemberHome findNotVIPInfo(String uid) {
		String sql = "SELECT * FROM t_users WHERE userid = ?";
		return (MemberHome) queryForObject(ds8888playQry, sql, new Object[] { uid }, new BeanPropertyRowMapper<MemberHome>(MemberHome.class));
	}

	public MemberHome findBonus(String uid) {
		String sql = "SELECT bonus FROM bonus_record br WHERE br.userId = ? LIMIT 1";
		MemberHome result = (MemberHome) queryForObject(ds8888playQry, sql, new Object[] { uid }, new BeanPropertyRowMapper<MemberHome>(MemberHome.class));
		return result;
	}

	public Integer getBonus(String missionId) {
		String sql = "SELECT bonus FROM mission m WHERE m.id = ? LIMIT 1";
		MemberHome result = (MemberHome) queryForObject(ds8888playQry, sql, new Object[] { missionId }, new BeanPropertyRowMapper<MemberHome>(MemberHome.class));
		return result.getBonus();
	}

	public void addBonus(String uid, String missionId, Integer bonus) {
		String sql = "INSERT INTO bonus_record (missionId, userId, bonus, createTime) VALUES (?, ?, ?, NOW())";
		addForObject(ds8888playUpd, sql, new BaseDto(), new Object[] { missionId, uid, bonus });
	}

	public Integer getBonusBalence(String uid) {
		String sqlCredits = "SELECT COALESCE(sum(bonus),0) bonus FROM (SELECT bonus FROM bonus_record br WHERE br.userId = ?)r";
		MemberHome credits = (MemberHome) queryForObject(ds8888playQry, sqlCredits, new Object[] { uid }, new BeanPropertyRowMapper<MemberHome>(
				MemberHome.class));
		String sqlDebits = "SELECT COALESCE(sum(cost),0) bonus FROM (SELECT cost FROM exchange_record br WHERE br.userId = ?)r";
		MemberHome debits = (MemberHome) queryForObject(ds8888playQry, sqlDebits, new Object[] { uid }, new BeanPropertyRowMapper<MemberHome>(MemberHome.class));
		if (null != debits.getBonus()) {
			return credits.getBonus() - debits.getBonus();
		} else {
			return credits.getBonus();
		}
	}

	public MemberHome findAddBonusToday(String uid) {
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		String currentDay = dateFormat.format(new Date());
		String sql = "SELECT bonus FROM bonus_record br WHERE br.userId = ? AND createTime BETWEEN '" + currentDay + " 00:00:00' AND '" + currentDay
				+ " 23:59:59' LIMIT 1";
		MemberHome result = (MemberHome) queryForObject(ds8888playQry, sql, new Object[] { uid }, new BeanPropertyRowMapper<MemberHome>(MemberHome.class));
		return result;
	}

	public boolean updateUser(Integer userId, String email, String phone) {
		StringBuffer sql= new StringBuffer("UPDATE t_users SET modifiedTime=NOW(),bindTime=NOW()");
		ArrayList<Object> params = new ArrayList<Object>();
		
		if(StringUtils.isNotBlank(email)){
			sql.append(",email=?");
			params.add(email);
		}
		if(StringUtils.isNotBlank(phone)){
			sql.append(",telephone=?");
			params.add(phone);
		}
		
		sql.append(" where userid=?");
		params.add(userId);
		
		if(this.updateForObject(ds8888playUpd,sql.toString(),params.toArray())==1){
			return true;
		}else {
			return false;
		}
	}
	
	public User checkUser(String loginName, String email, String phone) throws Exception {
		StringBuilder sql = new StringBuilder("SELECT userId, userName, COALESCE(email,'') email, COALESCE(telephone,'') telephone FROM t_users WHERE userName=? LIMIT 1");
		
		User user = (User) this.queryForObject(ds8888playQry, sql.toString(), new Object[]{loginName}, new BeanPropertyRowMapper<User>(User.class));
		
		if(user != null){
			if((!email.equals("") && user.getEmail().equals("")) || (!phone.equals("") && user.getTelephone().equals("")) ){
				throw new NoBondFailException();
			}else{
				if(!email.equals("") && !user.getEmail().equals(email)){
					return null;
				}
				if(!phone.equals("") && !user.getTelephone().equals(phone)){
					return null;
				}
				
				return user;
			}			
		}else{
			return null;
		}
	}

	public boolean updateUserEmail(Integer userId, String email) {
		String sql="UPDATE t_users SET email=? ,modifiedTime=NOW() where userid=?";
		if(this.updateForObject(ds8888playUpd,sql,new Object[]{email,userId})==1){
			return true;
		}else {
			return false;
		}
	}

	public String getAuthCode(Integer userId) {
		String sql="select msnqq from t_users where userid=? ";
		return this.queryForString(ds8888playQry,sql,new Object[]{userId});
	}

	public Integer updateAuthCode(Long i, String randomCode) {
		String sql="UPDATE t_users SET  msnqq=?,modifiedTime=now(),bindTime=NOW() where userid=?";
		return this.updateForObject(ds8888playUpd,sql,new Object[]{randomCode,i});
	}
	
	public boolean updatePwd(Long userId, boolean sendSMS){
		StringBuffer sql = new StringBuffer("UPDATE t_users SET userPwd=?, modifiedTime=NOW() ");
		
		if(sendSMS){
			sql.append(", bindTime=NOW() ");
		}
		
		sql.append(" WHERE userId=?");
		
		String pwd = MD5Util.crypt(Config.FORGOT_PWD).toUpperCase();
		return (this.updateForObject(ds8888playUpd, sql.toString(), new Object[]{pwd, userId})>0);
	}
	
	public boolean checkAuthCode(Long userId) {
		String sql="select uid from t_users where userid=? and date_add(bindTime, INTERVAL 30 MINUTE) >= NOW()";
		Long uid = this.queryForLong(ds8888playQry, sql, new Object[]{userId});
		
		if(uid>0){			
			return true;
		}else {
			return  false;
		}
	}
	
	public String checkAppVersion(String gameCode, String version){
		String sql = "select * from app_version_control WHERE gameCode=? AND version>? ORDER BY version DESC LIMIT 1";
		AppVersionControl appVersionControl = (AppVersionControl)this.queryForObject(ds8888playQry, sql, new Object[]{gameCode, version}, new BeanPropertyRowMapper<AppVersionControl>(AppVersionControl.class));
		
		if(appVersionControl == null){
			return "";
		}else{
			if(appVersionControl.getIsUpgrade() == 1){
				return appVersionControl.getStoreUrl();
			}else{
				return "";
			}			
		}
		
		
	}
	
}
