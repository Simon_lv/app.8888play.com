package tw.tw360.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import javax.annotation.Resource;
import javax.sql.DataSource;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import org.springside.modules.orm.Page;

import tw.tw360.dto.view.App;
import tw.tw360.dto.view.Apps;
import tw.tw360.dto.view.Games;
import tw.tw360.dto.view.Recomms;

@SuppressWarnings("rawtypes")
@Repository
public class GameRecordDao extends BaseDao {

	@Resource(name = "ds8888playQry")
	private DataSource ds8888playQry;

	@Resource(name = "ds8888playUpd")
	private DataSource ds8888playUpd;

	@Autowired
	public GameRecordDao(@Qualifier("ds8888playUpd") DataSource dataSource) {
		setDataSource(dataSource);
	}
	
	@SuppressWarnings("unchecked")
	public List<Apps> queryAll(String uid) {
		String sql = "select (@row_number:=@row_number + 1) `order`, gr.*, tg.gameName, (select name from game_type where id=gr.gameTypeId) gameType, case when gr.type=3 then (select id from mission where gameCode=gr.gameCode and `status`=1 and type=2) else null end mid, (select bonus from mission where id=mid) bonus "
				+ (StringUtils.isEmpty(uid) ? "" : ",case when (select count(*) from bonus_record br where br.missionId=mid and br.userId='" + uid + "' limit 1)=0 then 1 else 2 end br_status")
				+ " from game_record gr, t_games tg, (select @row_number:=0) t where tg.gameCode=gr.gameCode and gr.type in (1,3) and gr.`status`=1 order by COALESCE(order_index,999), createTime desc LIMIT 3";		
		return queryForList(ds8888playQry, sql, null, new AppsMapper(uid));
	}
	
	public App getByGameCode(String gameCode) {
		String sql = "select gr.*, tg.gameName, (select name from game_type where id = gr.gameTypeId) gameType from game_record gr, t_games tg where tg.gameCode=gr.gameCode and gr.gameCode=? AND STATUS = 1";		
		return (App) queryForObject(ds8888playQry, sql, new Object[]{gameCode}, new AppMapper());
	}
	
	@SuppressWarnings("unchecked")
	public Page<Games> search(Page<Games> page, int pageNo, int pageSize){
		StringBuffer query = new StringBuffer();
		StringBuffer count = new StringBuffer();
		
		query.append("SELECT (@row_number :=@row_number + 1) `order`, (select gameName from t_games where gameCode=gr.gameCode) gameName, (select `name` from game_type where id=gr.gameTypeId) gameType, gr.* ");
		query.append("FROM game_record gr, (SELECT @row_number := 0) t WHERE `status` = 1 ORDER BY COALESCE(order_index,999), createTime DESC");
		count.append("SELECT COUNT(*) FROM game_record WHERE status=1");
		
		if(pageNo <= 0) {
			pageNo = 1;
		}
		
		query.append(String.format(" limit %s, %s", (pageNo - 1) * pageSize, pageSize));		
		return queryForPage(ds8888playQry, page, query.toString(), count.toString(), null, new GamesMapper());
	}
	
	@SuppressWarnings("unchecked")
	public List<Recomms> findRecomms(){
		String sql = "SELECT * FROM game_recomm WHERE STATUS=1 ORDER BY createTime DESC LIMIT 4";
		return super.queryForList(ds8888playQry, sql, null, new RecommsMapper());
	}
	
	private class AppsMapper implements RowMapper<Object> {
		private String uid;
		
		public AppsMapper(String uid) {
			this.uid = uid;
		}
		
		public Apps mapRow(ResultSet rs, int rowNum) throws SQLException {
			Apps view = new Apps();
			
			Long missionId = rs.getLong("mid");
			boolean isNull = rs.wasNull();
			view.setName(rs.getString("gameName"));
			view.setSubTitle(rs.getString("subTitle"));
			view.setGameCode(rs.getString("gameCode"));
			view.setType(rs.getInt("type"));
			view.setPackageName(rs.getString("packageName"));
			view.setIconUrl(rs.getString("iconUrl"));
			view.setApkUrl(rs.getString("apkUrl"));
			view.setIosUrl(rs.getString("iosUrl"));
			view.setContent(isNull ? rs.getString("content") : String.format("%s 紅利點數:%s", rs.getString("content"), rs.getInt("bonus")));
			view.setId(isNull ? null : missionId);
			view.setOrder(rs.getInt("order"));
			view.setStatus(StringUtils.isEmpty(uid) ? 1 : rs.getInt("br_status"));
			
			return view;
		}
	}
	
	private class AppMapper implements RowMapper<Object> {
		public App mapRow(ResultSet rs, int rowNum) throws SQLException {
			App view = new App();
			
			view.setName(rs.getString("gameName")==null?"":rs.getString("gameName"));
			view.setGameType(rs.getString("gameType")==null?"":rs.getString("gameType"));
			view.setTitle(rs.getString("title")==null?"":rs.getString("title"));
			view.setIconUrl(rs.getString("iconUrl")==null?"":rs.getString("iconUrl"));
			view.setApkUrl(rs.getString("apkUrl")==null?"":rs.getString("apkUrl"));
			view.setIosUrl(rs.getString("iosUrl")==null?"":rs.getString("iosUrl"));
			view.setSize(rs.getFloat("size"));
			view.setImgUrl1(rs.getString("imgUrl1")==null?"":rs.getString("imgUrl1"));
			view.setImgUrl2(rs.getString("imgUrl2")==null?"":rs.getString("imgUrl2"));
			view.setImgUrl3(rs.getString("imgUrl3")==null?"":rs.getString("imgUrl3"));
			view.setImgUrl4(rs.getString("imgUrl4")==null?"":rs.getString("imgUrl4"));
			view.setImgUrl5(rs.getString("imgUrl5")==null?"":rs.getString("imgUrl5"));
			view.setVersion(rs.getString("version")==null?"":rs.getString("version"));
			
			StringBuilder sb = new StringBuilder("");
			sb.append("<style>body{background-color: #E8EAE0;} p{background-color: #E8EAE0;} span{background-color: #E8EAE0;} div{background-color: #E8EAE0;}</style>");
			sb.append("<div style='overflow:auto; width: 100%;'>");
			sb.append(rs.getString("content"));
			sb.append("</div>");
			view.setContent(sb.toString());
			
			if(StringUtils.isNotBlank(rs.getString("videoUrl"))){
				view.setVideoUrl(rs.getString("videoUrl"));
			}
			if(StringUtils.isNotBlank(rs.getString("videoImgUrl"))){
				view.setVideoImgUrl(rs.getString("videoImgUrl"));
			}
			
			return view;
		}
	}
	
	private class GamesMapper implements RowMapper<Object> {
		
		public Games mapRow(ResultSet rs, int rowNum) throws SQLException {
			Games view = new Games();
			
			view.setId(rs.getLong("id"));
			view.setGameName(rs.getString("gameName"));
			view.setGameType(rs.getString("gameType"));
			view.setGameCode(rs.getString("gameCode"));
			view.setApkUrl(rs.getString("apkUrl"));
			view.setIosUrl(rs.getString("iosUrl"));
			view.setType(new Integer(rs.getInt("type")));
			view.setSize(rs.getFloat("size"));
			view.setIconUrl(rs.getString("iconUrl"));			
			view.setTitle(rs.getString("title"));
			view.setPackageName(rs.getString("packageName"));
			
			return view;
		}
	}
	
	private class RecommsMapper implements RowMapper<Object> {
		public Recomms mapRow(ResultSet rs, int rowNum) throws SQLException {
			Recomms view = new Recomms();
			
			view.setId(rs.getLong("id"));
			view.setBannerUrl(rs.getString("bannerUrl"));
			view.setGameCode(rs.getString("gameCode"));
			
			return view;
		}
	}
	
}