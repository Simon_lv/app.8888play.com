package tw.tw360.dao;

import java.sql.Timestamp;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.sql.DataSource;

import org.springframework.stereotype.Repository;

import tw.tw360.dto.SMSRecord;




/**
 * 簡訊紀錄
 * @version 1.0
 * */
@Repository
public class SMSRecordDao extends BaseDao<SMSRecord> {
	@Resource(name = "ds8888playUpd")
	private DataSource ds8888playUpd;

	@Resource(name = "ds8888playQry")
	private DataSource ds8888playQry;

	@PostConstruct
	private void initialize() {
		setDataSource(ds8888playUpd);
	}

	public SMSRecord findByMessageId(String messageId) {
		SMSRecord smsRecord = (SMSRecord) queryForObject(ds8888playQry,"SELECT m.* FROM sms_record as m where message_id = ? limit 1",new Object[]{messageId}, SMSRecord.class);
		return smsRecord;
	}
	
	public SMSRecord add(final SMSRecord smsRecord) {
		final String sql = 
				"INSERT INTO sms_record("
				+"message_id"
				+",used_credit"
				+",member_id"
				+",credit"
				+",mobile_no"
				+",status"
				+",create_time"
				+") values "+
				"(?,?,?,?,?,?,?)";
		
		return (SMSRecord) addForObject(ds8888playUpd, sql, smsRecord, new Object[] { 
				smsRecord.getMessageId(),
				smsRecord.getUsedCredit(),
				smsRecord.getMemberId(),
				smsRecord.getCredit(),
				smsRecord.getMobileNo(),
				smsRecord.getStatus(),
				smsRecord.getCreateTime()
		});
	}
	
	public int updateReport(String messageId,String reportStatus,String sourceProdId,String sourceMessageId, Timestamp reportTime) {
		final String sql = "update sms_record set report_status=?,source_prod_id=?,source_message_id=?,report_time=? where message_id=?";
		return updateForObject(ds8888playUpd, sql, new Object[] { reportStatus, sourceProdId, sourceMessageId,reportTime, messageId });
	}

}
