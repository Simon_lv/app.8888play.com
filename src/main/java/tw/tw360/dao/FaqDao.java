package tw.tw360.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import org.springside.modules.orm.Page;

import tw.tw360.dto.view.Faq;

@SuppressWarnings("rawtypes")
@Repository
public class FaqDao extends BaseDao {

	@Resource(name = "ds8888playQry")
	private DataSource ds8888playQry;

	@Resource(name = "ds8888playUpd")
	private DataSource ds8888playUpd;

	@Autowired
	public FaqDao(@Qualifier("ds8888playUpd") DataSource dataSource) {
		setDataSource(dataSource);
	}
	
	@SuppressWarnings("unchecked")
	public Page<Faq> search(Page<Faq> page, int pageNo, int pageSize, int type){
		StringBuffer query = new StringBuffer();
		StringBuffer count = new StringBuffer();
		
		List<Object> params = new ArrayList<Object>();
	
		query.append("select * from faq where `status`=1");
		count.append("select * from faq where `status`=1");
		
		if(type > 0) {
			query.append(" and `type`=?");
			count.append(" and `type`=?");
			params.add(type);
		}
		
		query.append(" ORDER BY COALESCE(orderIndex,999), createTime DESC");
		
		if(pageNo <= 0) {
			pageNo = 1;
		}
		
		query.append(String.format(" limit %s, %s", (pageNo - 1) * pageSize, pageSize));
		
		return queryForPage(ds8888playQry, page, query.toString(), count.toString(), params.toArray(), new FaqMapper());
	}
	
	@SuppressWarnings("unchecked")
	public List<Faq> findTitle(int type){
		String sql = "select * from faq where `status`=1 and type=? ORDER BY COALESCE(orderIndex,999), createTime DESC";		
		return queryForList(ds8888playQry, sql, new Object[]{type}, new FaqMapper());
	}
	
//	public Faq get(int id){
//		String sql = "select * from faq where id=?";
//		return (Faq)super.queryForObject(ds8888playQry, sql, new Object[]{id}, new FaqMapper());
//	}
	
	private class FaqMapper implements RowMapper<Object> {
		
		public Faq mapRow(ResultSet rs, int rowNum) throws SQLException {
			Faq view = new Faq();
			
			view.setId(rs.getLong("id"));			
			view.setTitle(rs.getString("title"));				
			return view;
		}
	}
	
}
