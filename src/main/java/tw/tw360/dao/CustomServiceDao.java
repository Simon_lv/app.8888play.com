package tw.tw360.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import org.springside.modules.orm.Page;

import tw.tw360.dto.CustomServiceImage;
import tw.tw360.dto.CustomServiceProblem;
import tw.tw360.dto.GamePlayerProblem;
import tw.tw360.dto.view.ReplyDetail;
import tw.tw360.dto.view.ReplyList;
import tw.tw360.utils.DateUtil;


@SuppressWarnings("rawtypes")
@Repository
public class CustomServiceDao extends BaseDao {

	@Resource(name = "ds8888playQry")
	private DataSource ds8888playQry;

	@Resource(name = "ds8888playUpd")
	private DataSource ds8888playUpd;

	@Autowired
	public CustomServiceDao(@Qualifier("ds8888playUpd") DataSource dataSource) {
		setDataSource(dataSource);
	}

	public int getCsNotify(String uid) {
		String sql = "select count(*) from t_cusservice_problem where item1='cs' and `status`=1 "
				+ "and tgppid in (select tgppid from t_gameplayer_problem where uid=?)";;

		return queryForInt(ds8888playQry, sql, new Object[] { uid });
	}

	public void addQuestion(final GamePlayerProblem problem) {
		String sql = "INSERT INTO t_gameplayer_problem (`gameCode`, `serverCode`, `roleid`, `palayName`, `uid`, `questionsTitle`, `theQuestions`, `questionType`, `createTime`, `item1`, `item3`, `item4`, `item7`, `item10`, deviceOs, deviceOsVer, deviceModel) VALUES "
				+ "(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ? ,?, ?)";
		Object[] args = new Object[] { problem.getGameCode(), problem.getServerCode(), problem.getRoleid(), problem.getPalayName(), problem.getUid(),
				problem.getQuestionsTitle(), problem.getTheQuestions(), problem.getQuestionType(), problem.getCreateTime(), problem.getItem1(), problem.getItem3(), problem.getItem4(),
				problem.getCreateTime(), problem.getItem10(), problem.getDeviceOs(), problem.getDeviceVer(), problem.getDeviceModel() };
		addForObject(ds8888playUpd, sql, problem, args);
	}

	public CustomServiceProblem addReply(final CustomServiceProblem problem) {
		String sql = "INSERT INTO t_cusservice_problem (`gameCode`, `serverCode`, `tgppid`, `replyTime`, `replyUid`, `replyContent`, `plateform`, `item1`) VALUES "
				+ "(?, ?, ?, ?, ?, ?, ?, ?)";
		Object[] args = new Object[] { problem.getGameCode(), problem.getServerCode(), problem.getTgppid(), problem.getReplyTime(), problem.getReplyUid(),
				problem.getReplyContent(), problem.getPlateform(), problem.getItem1() };
		return (CustomServiceProblem) addForObject(ds8888playUpd, sql, problem, args);
	}

	public CustomServiceImage addImage(final CustomServiceImage image) {
		String sql = "INSERT INTO t_cp_image (`tcspid`, `url`, `createTime`) VALUES (?, ?, ?)";
		Object[] args = new Object[] { image.getTcspid(), image.getUrl(), image.getCreateTime() };
		return (CustomServiceImage) addForObject(ds8888playUpd, sql, image, args);
	}

	@SuppressWarnings("unchecked")
	public Page<ReplyList> queryByUid(Page<ReplyList> page, int pageNo, int pageSize, String uid) {
		StringBuffer query = new StringBuffer();
		StringBuffer count = new StringBuffer();

		List<Object> params = new ArrayList<Object>();
		query.append("select (@row_number:=@row_number + 1) `order`, tgpp.tgppid id, tgpp.questionsTitle title, tgpp.createTime `date`, (select case when (select ifnull(count(*),0) from t_cusservice_problem where tgppid=tgpp.tgppid and item1='cs' and `status`=1) > 0 then 1 else 2 end) `status` from t_gameplayer_problem tgpp, (select @row_number:=0) t where tgpp.uid=?");
		count.append("select count(*) from t_gameplayer_problem where uid=?");

		if (pageNo <= 0) {
			pageNo = 1;
		}

		params.add(uid);

		query.append(" order by tgpp.createTime desc");
		query.append(String.format(" limit %s, %s", (pageNo - 1) * pageSize, pageSize));

		return queryForPage(ds8888playQry, page, query.toString(), count.toString(), params.toArray(), new ReplyListMapper());
	}

	@SuppressWarnings("unchecked")
	public GamePlayerProblem queryProblemById(int id) {
		String sql = "select * from t_gameplayer_problem where tgppid=?";

		List<Object> params = new ArrayList<Object>();
		params.add(id);

		return (GamePlayerProblem) queryForObject(ds8888playQry, sql, params.toArray(), GamePlayerProblem.class);
	}

	@SuppressWarnings("unchecked")
	public List<ReplyDetail> queryReplyDetailById(int id) {
		String sql = "select (@row_number:=@row_number + 1) `order`, tcsp.replyContent `content`, tcsp.replyTime `date`, tcsp.replyName, "
				+ "(case tcsp.item1 when 'cs' then 1 when 'player' then 2 end) `type`, "
				+ "(select GROUP_CONCAT(url ORDER BY url ASC SEPARATOR ',') from t_cp_image where tcspid=tcsp.tcspid) `url` "
				+ "from t_cusservice_problem tcsp, (select @row_number:=1) t " + // 第一筆放原問題
				"where tcsp.tgppid=? order by replyTime";

		return queryForList(ds8888playQry, sql, new Object[] { id }, new ReplyDetailMapper());
	}

	@SuppressWarnings("unchecked")
	public CustomServiceProblem queryReplyById(int id) {
		String sql = "select * from t_cusservice_problem where tcspid=?";

		List<Object> params = new ArrayList<Object>();
		params.add(id);

		return (CustomServiceProblem) queryForObject(ds8888playQry, sql, params.toArray(), CustomServiceProblem.class);
	}

	public int updateStatus(long id, int status) {
		final String sql = "update t_cusservice_problem set `status`=? where tgppid=?";
		return updateForObject(ds8888playUpd, sql, new Object[] { status, id });
	}

	public int updateScore(long id, int score, boolean isEmpyContent) {		
		final String sql = "update t_gameplayer_problem set " + (isEmpyContent ? "" : "replyState=null,") + "score=?,item7=now() where tgppid=?";
		return updateForObject(ds8888playUpd, sql, new Object[] { score, id });		
	}
	
	private class ReplyListMapper implements RowMapper<Object> {
		public ReplyList mapRow(ResultSet rs, int rowNum) throws SQLException {
			ReplyList view = new ReplyList();

			view.setId(rs.getLong("id"));
			view.setTitle(rs.getString("title"));
			view.setDate(rs.getDate("date"));
			view.setOrder(rs.getInt("order"));
			view.setStatus(rs.getInt("status"));
			;

			return view;
		}
	}

	private class ReplyDetailMapper implements RowMapper<Object> {
		public ReplyDetail mapRow(ResultSet rs, int rowNum) throws SQLException {
			ReplyDetail view = new ReplyDetail();

			view.setContent(rs.getString("content"));
			view.setDate(DateUtil.format(rs.getDate("date"), "yyyy-MM-dd HH:mm:ss"));
			view.setImgUrl(rs.getString("url"));
			view.setType(rs.getInt("type"));
			view.setOrder(rs.getInt("order"));
			view.setReplyName(rs.getString("replyName"));

			return view;
		}
	}
	
	public int updateReplyState(int id, int score){
		String sql = "UPDATE t_gameplayer_problem SET replyState=1, score=? WHERE tgppid=?";
		return this.updateForObject(ds8888playUpd, sql, new Object[]{score, id});
	}
	
}