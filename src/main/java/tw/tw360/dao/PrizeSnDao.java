package tw.tw360.dao;

import java.util.ArrayList;

import javax.annotation.Resource;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import tw.tw360.dto.PrizeSn;

@Repository
public class PrizeSnDao extends BaseDao {
	
	@Resource(name = "ds8888playQry")
	private DataSource ds8888playQry;

	@Resource(name = "ds8888playUpd")
	private DataSource ds8888playUpd;

	@Autowired
	public PrizeSnDao(@Qualifier("ds8888playUpd") DataSource dataSource) {
		setDataSource(dataSource);
	}

	@SuppressWarnings("unchecked")
	public PrizeSn findOneByPrizeId(long id) {
		String sql = "SELECT * FROM prize_sn WHERE prizeId = ? AND COALESCE(userId,'')='' LIMIT 1";
		return (PrizeSn) super.queryForObject(ds8888playQry, sql, new Object[]{id}, PrizeSn.class);
	}

	public int update(Long id, String deviceId, String ip, String uid) {
		return super.updateForObject(ds8888playUpd, "UPDATE `prize_sn` SET `userId`=?, `receiveTime`=NOW(), `sourceIp`=?, `deviceId`=? WHERE (`id`=?)",
				new Object[]{uid, ip, deviceId, id});
	}	
	
	@SuppressWarnings("unchecked")
	public PrizeSn getPrizeSn(long prizeId, String uid, String deviceId, String sourceIp) {
		String sqlS = "SELECT * FROM prize_sn WHERE prizeId=? AND COALESCE(userId,'') = '' LIMIT 1";
		PrizeSn sn = (PrizeSn) this.queryForObject(ds8888playQry, sqlS, new Object[]{prizeId}, PrizeSn.class);
		
		if(sn != null){
			String sql = "update prize_sn set userid=?, receiveTime=NOW(), deviceId=?, sourceIp=? WHERE id=?";		
			ArrayList<Object> params = new ArrayList<Object>();		
			params.add(uid);
			params.add(deviceId);
			params.add(sourceIp);
			params.add(sn.getId());
			if(this.updateForObject(ds8888playUpd, sql, params.toArray())>0){
				return sn;
			}else{
				return null;
			}
		}else{
			return null;
		}		
	}	
	
}