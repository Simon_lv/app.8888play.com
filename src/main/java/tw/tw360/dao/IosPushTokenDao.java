package tw.tw360.dao;

import java.util.List;

import javax.annotation.Resource;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import tw.tw360.dto.IosPushToken;

@Repository
public class IosPushTokenDao extends BaseDao {

	@Resource(name = "ds8888playQry")
	private DataSource ds8888playQry;

	@Resource(name = "ds8888playUpd")
	private DataSource ds8888playUpd;

	@Autowired
	public IosPushTokenDao(@Qualifier("ds8888playUpd") DataSource dataSource) {
		setDataSource(dataSource);
	}
	
	public List<IosPushToken> queryAll() {
		final String sql = "select `id`, MAX(`version`) version, `deviceType`, `deviceToken`, `createTime` from ios_push_token GROUP BY deviceToken";
		return (List<IosPushToken>)this.queryForList(ds8888playQry, sql, new Object[] { }, IosPushToken.class);
	}

	public IosPushToken add(final IosPushToken token) {
		final String sql = "INSERT INTO ios_push_token(version,deviceType,deviceToken,createTime) values " + "(?,?,?,?)";

		return (IosPushToken) addForObject(ds8888playUpd, sql, token,
				new Object[] { token.getVersion(), token.getDeviceType(), token.getDeviceToken(), token.getCreateTime() });
	}

	public IosPushToken findOneByDeviceTokenAndVersion(String version, String deviceToken) {
		String sql = "SELECT * FROM ios_push_token WHERE version=? AND deviceToken=?";
		Object[] args = new Object[]{version, deviceToken};
		return (IosPushToken) super.queryForObject(ds8888playQry, sql, args, IosPushToken.class);
	}	
}