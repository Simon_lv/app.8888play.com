package tw.tw360.dao;

import javax.annotation.Resource;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

@Repository
public class UserDao extends BaseDao {

	@Resource(name = "ds8888playQry")
	private DataSource ds8888playQry;

	@Resource(name = "ds8888playUpd")
	private DataSource ds8888playUpd;

	@Autowired
	public UserDao(@Qualifier("ds8888playUpd") DataSource dataSource) {
		setDataSource(dataSource);
	}
	
	public boolean isUser(long uid) {
		String sql = "select count(*) from t_users where userid=?";		
		return queryForInt(ds8888playQry, sql, new Object[]{uid}) == 0 ? false : true;
	}
	
	public boolean isVIP(long uid) {
		String sql = "select count(*) from t_pf_vip where uid=?";		
		return queryForInt(ds8888playQry, sql, new Object[]{uid}) == 0 ? false : true;
	}		
	
	public int updateContactUser(long uid, String phone, String email) {
		String sql = "update t_users set telephone=COALESCE(NULLIF(?, ''), telephone), email=COALESCE(NULLIF(?, ''), email) where userid=?";
		return updateForObject(ds8888playUpd, sql, new Object[] {phone, email, uid});
	}
	
	public int updateContactVIP(long uid, String phone, String email) {
		String sql = "update t_pf_vip set phoneNo=COALESCE(NULLIF(?, ''), phoneNo), email=COALESCE(NULLIF(?, ''), email) where uid=?";
		return updateForObject(ds8888playUpd, sql, new Object[] {phone, email, uid});
	}
}