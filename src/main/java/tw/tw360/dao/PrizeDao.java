package tw.tw360.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import org.springside.modules.orm.Page;

import tw.tw360.dto.Prize;
import tw.tw360.dto.view.Detail;
import tw.tw360.dto.view.FreeList;

@Repository
public class PrizeDao extends BaseDao {
	
	@Resource(name = "ds8888playQry")
	private DataSource ds8888playQry;

	@Resource(name = "ds8888playUpd")
	private DataSource ds8888playUpd;

	@Autowired
	public PrizeDao(@Qualifier("ds8888playUpd") DataSource dataSource) {
		setDataSource(dataSource);
	}
	
	@SuppressWarnings("unchecked")
	public Page<FreeList> queryByUid(Page<FreeList> page, int pageNo, int pageSize, String uid) {
		StringBuffer query = new StringBuffer();
		StringBuffer count = new StringBuffer();
		
		List<Object> params = new ArrayList<Object>();
		
		query.append("SELECT @rownum := @rownum + 1 `order`, t.* FROM (select (select gameName from t_games where gameCode=p.gameCode) `name`, "
				+ "p.id, p.gameCode, p.iconUrl, p.order_index, p.content giftContent, p.desc giftDesc, "
				+ "(case when er.id is null then 1 else 2 end) `status`, p.cost from prize p "
				+ "left join exchange_record er on er.userId=? and er.prizeId=p.id where p.type=1 and status = 1");
		
		count.append("select count(*) from prize p "
				+ "left join exchange_record er on er.userId=? and er.prizeId=p.id where p.type=1 and status = 1");
		
		params.add(uid);		
				
		if(pageNo <= 0) {
			pageNo = 1;
		}
		
		query.append(" order by COALESCE(p.order_index,999), p.createTime desc");
		query.append(String.format(" limit %s, %s", (pageNo - 1) * pageSize, pageSize));
		query.append(") t, (SELECT @rownum := 0) r");
		
		return queryForPage(ds8888playQry, page, query.toString(), count.toString(), params.toArray(), new FreeListMapper());
	}		
	
	public Detail queryByUid(int id, String uid) {
		StringBuffer query = new StringBuffer();
		
		List<Object> params = new ArrayList<Object>();
		
		query.append("select (select gameName from t_games where gameCode=p.gameCode) `name`, "
				+ "p.gameCode, p.iconUrl, p.content giftContent, p.desc giftDesc, p.endTime timeLimit, p.`usage`, "
				+ "(case when er.id is null then 1 else 2 end) `status` from prize p "
				+ "left join exchange_record er on er.userId=? and er.prizeId=p.id where p.id=?");
		
		params.add(uid);
		params.add(id);
						
		return (Detail) queryForObject(ds8888playQry, query.toString(), params.toArray(), new DetailMapper());			
	}	
	
	@SuppressWarnings("unchecked")
	public Prize queryById(long id) {	
		return (Prize) queryForObject(ds8888playQry, "SELECT * FROM prize where status=1 and id=? and if(startTime IS NOT NULL,NOW()>=startTime,1=1) and if(endTime IS NOT NULL,NOW()<=endTime,1=1)", new Object[]{id}, Prize.class);
	}

	private class FreeListMapper implements RowMapper<Object> {
		public FreeList mapRow(ResultSet rs, int rowNum) throws SQLException {
			FreeList view = new FreeList();
			
			view.setId(rs.getLong("id"));
			view.setOrder(rs.getInt("order"));
			view.setGameCode(rs.getString("gameCode"));
			view.setName(rs.getString("name"));			
			view.setIconUrl(rs.getString("iconUrl"));
			view.setGiftDesc(rs.getString("giftDesc"));
			view.setGiftContent(rs.getString("giftContent"));
			view.setStatus(rs.getInt("status"));
			view.setCost(rs.getInt("cost"));
			
			return view;
		}
	}
	
	private class DetailMapper implements RowMapper<Object> {
		public Detail mapRow(ResultSet rs, int rowNum) throws SQLException {
			Detail view = new Detail();
			
			view.setGameCode(rs.getString("gameCode"));
			view.setName(rs.getString("name"));			
			view.setIconUrl(rs.getString("iconUrl"));
			view.setGiftDesc(rs.getString("giftDesc"));
			view.setGiftContent(rs.getString("giftContent"));
			view.setUsage(rs.getString("usage"));
			view.setStatus(rs.getInt("status"));
			view.setTimeLimit(rs.getDate("timeLimit"));
			
			return view;
		}
	}	
	
	@SuppressWarnings("unchecked")
	public Prize findOneById(long id) {
		String sql = "SELECT * FROM prize WHERE id=? AND IF(startTime IS NOT NULL, NOW() >= startTime, 1=1) AND IF(endTime IS NOT NULL, NOW() <= endTime,1=1)";
		Object[] args = new Object[]{id};
		return (Prize) super.queryForObject(ds8888playQry, sql, args, Prize.class);
	}	
}