package tw.tw360.dao;

import javax.annotation.Resource;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import tw.tw360.dto.PushRecord;

@Repository
public class PushRecordDao extends BaseDao<PushRecord> {
	
	@Resource(name = "ds8888playQry")
	private DataSource ds8888playQry;

	@Resource(name = "ds8888playUpd")
	private DataSource ds8888playUpd;

	@Autowired
	public PushRecordDao(@Qualifier("ds8888playUpd") DataSource dataSource) {
		setDataSource(dataSource);
	}

	public PushRecord findByIdForPush(long id) {
		String sql = "SELECT * FROM push_record WHERE id=? AND `status`=1 AND runTime IS NULL";
		Object[] args = new Object[]{id};
		return (PushRecord) super.queryForObject(ds8888playQry, sql, args, PushRecord.class);
	}

	public int updateBeforeSend(int id) {//, int status
		String sql = "UPDATE push_record SET runTime = NOW(), updateTime=NOW() WHERE id = ?"; //`status` = ?, 
		Object[] args = new Object[]{id};//status, 
		return super.updateForObject(ds8888playUpd, sql, args);
	}

	public PushRecord findByIdForIosPush(int id) {
		String sql = "SELECT * FROM push_record WHERE id=? AND `status`=1 AND iosRunTime IS NULL";
		Object[] args = new Object[]{id};
		return (PushRecord) super.queryForObject(ds8888playQry, sql, args, PushRecord.class);
	}

	public int updateBeforeIosSend(int id) {
		String sql = "UPDATE push_record SET iosRunTime = NOW(), updateTime=NOW() WHERE id = ?"; //`status` = ?, 
		Object[] args = new Object[]{id};//status, 
		return super.updateForObject(ds8888playUpd, sql, args);
	}

}
