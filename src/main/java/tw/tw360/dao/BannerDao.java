package tw.tw360.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import javax.annotation.Resource;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import tw.tw360.dto.view.Banners;

@SuppressWarnings("rawtypes")
@Repository
public class BannerDao extends BaseDao {

	@Resource(name = "ds8888playQry")
	private DataSource ds8888playQry;

	@Resource(name = "ds8888playUpd")
	private DataSource ds8888playUpd;

	@Autowired
	public BannerDao(@Qualifier("ds8888playUpd") DataSource dataSource) {
		setDataSource(dataSource);
	}
	
	@SuppressWarnings("unchecked")
	public List<Banners> queryAll() {
		String sql = "select * from banner where `status`=1 and coalesce(gameCode,'') ='' and startTime <= now() and (endTime is null or endTime >= now()) ORDER BY COALESCE(orderIndex,999), createTime DESC LIMIT 6";		
		return queryForList(ds8888playQry, sql, null, new BannerMapper());
	}
	
	private class BannerMapper implements RowMapper<Object> {
		public Banners mapRow(ResultSet rs, int rowNum) throws SQLException {
			Banners view = new Banners();
			
			view.setId(rs.getLong("id"));
			view.setOrder(rs.getInt("orderIndex"));
			view.setImgUrl(rs.getString("bannerUrl"));
			view.setPageUrl(rs.getString("linkUrl"));
			view.setGameCode(rs.getString("gameCode"));			
			
			return view;
		}
	}
}