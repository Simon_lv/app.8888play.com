package tw.tw360.dao;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;
import tw.tw360.dto.view.Games;

import javax.annotation.Resource;
import javax.sql.DataSource;

import java.util.ArrayList;
import java.util.List;

@Repository
public class GamesDao extends BaseDao {

	@Resource(name = "ds8888playQry")
	private DataSource ds8888playQry;

	@Resource(name = "ds8888playUpd")
	private DataSource ds8888playUpd;

	@Autowired
	public GamesDao(@Qualifier("ds8888playUpd") DataSource dataSource) {
		setDataSource(dataSource);
	}
	
	public String getAppKey(String gameCode) {
		String sql = "select appkey from t_games where `gameCode`=? limit 1";		
		return queryForString(ds8888playQry, sql, new Object[]{gameCode});
	}

	@SuppressWarnings("unchecked")
	public List<Games> getGameCodeAndName(String uid) {
		ArrayList<Object> param = new ArrayList<Object>();
		String sql;
		
		if(StringUtils.isBlank(uid)){
			sql = "SELECT g.gameCode, g.gameName FROM t_games g WHERE g.pid=1 AND g.gameCode NOT LIKE '%ios' AND g.flag='是' AND g.isonline='是' AND g.isgame='是' and g.gpid=1 and if(g.loginAllow!='是', g.loginAllow!='是' and g.mstartTime<=NOW() and g.mendTime>=NOW(), g.loginAllow='是')";
		}else{
			sql ="SELECT g.gameCode, g.gameName FROM t_games g WHERE g.pid=1 AND g.flag='是' AND g.isonline='是' AND g.isgame='是' and g.gpid=1 and g.gameCode in (select ur.gameCode from t_users_relation ur where ur.gameCode not like '%ios' and ur.userid =? ) and if(g.loginAllow!='是', g.loginAllow!='是' and g.mstartTime<=NOW() and g.mendTime>=NOW(), g.loginAllow='是') order by g.gameName";
			param.add(uid);
		}
		
		return this.queryForList(ds8888playQry,sql,param.toArray(),Games.class);
	}
}