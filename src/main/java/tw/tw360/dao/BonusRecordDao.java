package tw.tw360.dao;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.sql.DataSource;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;

import tw.tw360.dto.BaseDto;
import tw.tw360.dto.BonusRecord;
import tw.tw360.dto.MemberHome;
import tw.tw360.dto.Mission;
import tw.tw360.dto.view.BonusRecordAPI29;

@Repository
public class BonusRecordDao extends BaseDao {

	@Resource(name = "ds8888playQry")
	private DataSource ds8888playQry;

	@Resource(name = "ds8888playUpd")
	private DataSource ds8888playUpd;

	@Autowired
	public BonusRecordDao(@Qualifier("ds8888playUpd") DataSource dataSource) {
		setDataSource(dataSource);
	}

	public List<BonusRecordAPI29> list(int pageNo, int pageSize, String uid) {
		String sql = "SELECT @rownum := @rownum+1 AS 'order', id, `date`, point, content FROM "
				+ "(SELECT bonus_record.id id,    DATE_FORMAT(createTime, '%Y-%m-%d') 'date', bonus point, content, createTime FROM bonus_record  "
				+ "LEFT JOIN (SELECT id, content FROM mission) mission ON bonus_record.missionId = mission.id WHERE userId=? UNION ALL "
				+ "SELECT exchange_record.id id, DATE_FORMAT(createTime, '%Y-%m-%d') 'date', cost*(-1)  point, content, createTime FROM exchange_record "
				+ "LEFT JOIN (SELECT id, content FROM prize  ) prize   ON exchange_record.prizeId = prize.id WHERE userId=?) a,(SELECT @rownum := 0) r "
				+ "ORDER BY createTime DESC LIMIT " + ((pageNo - 1) * pageSize) + "," + pageSize;
		return super.queryForList(ds8888playQry, sql, new Object[] { uid, uid }, BonusRecordAPI29.class);
	}

	public int userTotalBonus(String uid) {
		String sql = "SELECT COALESCE(SUM(bonus),0) FROM bonus_record  WHERE userId=?";
		return super.queryForInt(ds8888playQry, sql, new Object[] { uid });
	}

	public MemberHome findDailyBonus(String uid, String mid) {
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		String currentDay = dateFormat.format(new Date());
		String sql = "SELECT br.bonus FROM bonus_record br JOIN mission m ON br.missionId = m.id WHERE br.userId = ? AND m.id = ? AND br.createTime BETWEEN '"
				+ currentDay + " 00:00:00' AND '" + currentDay + " 23:59:59' LIMIT 1";
		MemberHome result = (MemberHome) queryForObject(ds8888playQry, sql, new Object[] { uid, mid }, new BeanPropertyRowMapper<MemberHome>(MemberHome.class));
		return result;
	}

	public MemberHome findBonus(String uid, String mid, String appGameCode) {
		String sql = "SELECT br.bonus FROM bonus_record br JOIN mission m ON br.missionId = m.id WHERE br.userId = ? AND m.id = ? AND m.gameCode = ? LIMIT 1";
		MemberHome result = (MemberHome) queryForObject(ds8888playQry, sql, new Object[] { uid, mid, appGameCode }, new BeanPropertyRowMapper<MemberHome>(
				MemberHome.class));
		return result;
	}

	public void addBonus(String uid, String mid, String sourceIp) {
		String sqlBonus = "SELECT bonus FROM mission WHERE id = ? LIMIT 1";
		BonusRecord result = (BonusRecord) queryForObject(ds8888playQry, sqlBonus, new Object[] { mid }, new BeanPropertyRowMapper<BonusRecord>(
				BonusRecord.class));

		String sqlAdd = "INSERT INTO bonus_record (missionId, userId, bonus, sourceIp, createTime) VALUES (?, ?, ?, ?, NOW())";
		addForObject(ds8888playUpd, sqlAdd, new BaseDto(), new Object[] { mid, uid, result.getBonus(), sourceIp });
	}

	public Mission checkMidExist(String mid) {
		String sql = "SELECT * FROM mission WHERE id = ? LIMIT 1";
		Mission a = (Mission) queryForObject(ds8888playQry, sql, new Object[] { mid }, new BeanPropertyRowMapper<Mission>(Mission.class));
		return a;
	}
	
	public Mission checkMissionExist(String uid, String mid, String gameCode){
		StringBuilder sql = new StringBuilder("SELECT m.* FROM mission m WHERE m.id=? AND  LEFT JOIN bonus_record br ON m.id = br.missionId WHERE m.type=3 AND m.`status`=1 AND br.userId=? ");
		ArrayList<Object> param = new ArrayList<Object>();
		param.add(mid);
		param.add(uid);
		
		if(StringUtils.isNotBlank(gameCode)){
			sql.append(" AND m.gameCode=?");
			param.add(mid);
		}
		
		return (Mission)this.queryForObject(ds8888playQry, sql.toString(), param.toArray(), new BeanPropertyRowMapper<Mission>(Mission.class));
	}

	public Mission checkGameCodeExist(String appGameCode) {
		String sql = "SELECT * FROM mission WHERE gameCode = ? LIMIT 1";
		return (Mission) queryForObject(ds8888playQry, sql, new Object[] { appGameCode }, new BeanPropertyRowMapper<Mission>(Mission.class));
	}
	
	@SuppressWarnings("unchecked")
	public boolean bonusNotTaken(String uid, String mid){
		String sql = "select br.* from bonus_record br join mission m on br.missionId=m.id where br.userId=? and br.missionId=?";		
		BonusRecord br = (BonusRecord) this.queryForObject(ds8888playQry, sql, new Object[]{uid, mid}, BonusRecord.class);
		return br==null;
	}
	
}
