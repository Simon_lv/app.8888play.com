package tw.tw360.dao;

import javax.annotation.Resource;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

@Repository
@SuppressWarnings("rawtypes")
public class ConfigDao extends BaseDao {

	@Resource(name = "ds8888playQry")
	private DataSource ds8888playQry;

	@Resource(name = "ds8888playUpd")
	private DataSource ds8888playUpd;

	@Autowired
	public ConfigDao(@Qualifier("ds8888playUpd") DataSource dataSource) {
		setDataSource(dataSource);
	}
	
	public String getHomeVideoUrl(){
		String sql = "SELECT viedoUrl FROM 8888play_app_config WHERE status=1 ORDER BY createTime DESC LIMIT 1";
		return this.queryForString(ds8888playQry, sql, null);
	}
	
	@SuppressWarnings("unchecked")
	public tw.tw360.dto.view.Config getConfig(String gameCode){
		String sql ="";
		if(gameCode.equals("app")){
			sql = "SELECT * FROM 8888play_app_config WHERE status=1 and ifnull(appiosVersion,'')='' ORDER BY appVersion DESC LIMIT 1";
		}else if(gameCode.equals("appios")){
			sql = "SELECT * FROM 8888play_app_config WHERE status=1 and ifnull(appVersion,'')='' ORDER BY appiosVersion DESC LIMIT 1";
		}
				
		return (tw.tw360.dto.view.Config) this.queryForObject(ds8888playQry, sql, null, tw.tw360.dto.view.Config.class);
	}
	
	public tw.tw360.dto.view.Config getVersionConfig(String gameCode, String version){
		String sql = "";
		if(gameCode.equals("app")){
			sql = "SELECT * FROM 8888play_app_config WHERE status=1 and appVersion=? and ifnull(appiosVersion,'')='' ORDER BY createTime DESC LIMIT 1";
		}else if(gameCode.equals("appios")){
			sql = "SELECT * FROM 8888play_app_config WHERE status=1 and appiosVersion=? and ifnull(appVersion,'')='' ORDER BY createTime DESC LIMIT 1";
		}
		
		return (tw.tw360.dto.view.Config) this.queryForObject(ds8888playQry, sql, new Object[]{version}, tw.tw360.dto.view.Config.class);
	}
	
}
