package tw.tw360.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import org.springside.modules.orm.Page;

import tw.tw360.dto.view.Notify;
import tw.tw360.dto.view.NotifyList;

@Repository
public class NotifyDao extends BaseDao {

	@Resource(name = "ds8888playQry")
	private DataSource ds8888playQry;

	@Resource(name = "ds8888playUpd")
	private DataSource ds8888playUpd;

	@Autowired
	public NotifyDao(@Qualifier("ds8888playUpd") DataSource dataSource) {
		setDataSource(dataSource);
	}		
	
	public int getSysNotify(String uid) {
		String sql = "select count(*) from system_notify where userId=? and status=1";
		
		return queryForInt(ds8888playQry, sql, new Object[]{uid});
	}
	
	@SuppressWarnings("unchecked")
	public Page<NotifyList> queryByUid(Page<NotifyList> page, int pageNo, int pageSize, String uid) {
		StringBuffer query = new StringBuffer();
		StringBuffer count = new StringBuffer();
		
		List<Object> params = new ArrayList<Object>();
		query.append("select (@row_number:=@row_number + 1) `order`, sn.* from system_notify sn, (select @row_number:=0) t where sn.userId=?");
		count.append("select count(*) from system_notify where userId=?");
		
		if(pageNo <= 0) {
			pageNo = 1;
		}
		
		params.add(uid);
		
		query.append(" order by createTime desc");
		query.append(String.format(" limit %s, %s", (pageNo - 1) * pageSize, pageSize));
		
		return queryForPage(ds8888playQry, page, query.toString(), count.toString(), params.toArray(), new NotifyListMapper());				
	}
	
	public Notify queryById(long id) {
		String sql = "select * from system_notify where id=?";
		
		return (Notify) queryForObject(ds8888playQry, sql, new Object[]{id}, new NotifyMapper());
	}
	
	public int updateStatus(long id, int status) {
		final String sql = "update system_notify set `status`=? where id=?";
		return updateForObject(ds8888playUpd, sql, new Object[] {status, id});
	}
	
	private class NotifyListMapper implements RowMapper<Object> {
		public NotifyList mapRow(ResultSet rs, int rowNum) throws SQLException {
			NotifyList view = new NotifyList();
			
			view.setId(rs.getLong("id"));
			view.setTitle(rs.getString("title"));
			view.setDate(rs.getDate("createTime"));
			view.setOrder(rs.getInt("order"));
			view.setStatus(rs.getInt("status"));
			
			return view;
		}
	}
	
	private class NotifyMapper implements RowMapper<Object> {
		public Notify mapRow(ResultSet rs, int rowNum) throws SQLException {
			Notify view = new Notify();
			
			view.setTitle(rs.getString("title"));
			view.setContent(rs.getString("message"));
			view.setDate(rs.getDate("createTime"));
			
			return view;
		}
	}
}