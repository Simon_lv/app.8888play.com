package tw.tw360.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.sql.DataSource;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import tw.tw360.controller.web.MemberHomeAction;
import tw.tw360.dto.view.GoodStuff;
import tw.tw360.dto.view.GoodStuffs;

@SuppressWarnings("rawtypes")
@Repository
public class GoodStuffDao extends BaseDao {
	private static final Logger LOG = LoggerFactory.getLogger(MemberHomeAction.class);

	@Resource(name = "ds8888playQry")
	private DataSource ds8888playQry;

	@Resource(name = "ds8888playUpd")
	private DataSource ds8888playUpd;

	@Autowired
	public GoodStuffDao(@Qualifier("ds8888playUpd") DataSource dataSource) {
		setDataSource(dataSource);
	}
	
	@SuppressWarnings("unchecked")
	public List<GoodStuffs> queryAll(String uid, int type){
		StringBuffer query = new StringBuffer();
		ArrayList<Object> param = new ArrayList<Object>();
		
		query.append("SELECT a.id, a.`desc`, a.gameCode, a.iconUrl, a.snCount, a.endTime, a.cost, IF (a.takeIt = 0, 1, 2 ) status ");
		query.append("FROM ( SELECT p.*, ( SELECT gameName FROM t_games WHERE gameCode = p.gameCode ) gameName, ");
		
		if(StringUtils.isBlank(uid)){
			query.append(" 0 takeIt, ");
		}else{
			query.append(" ( SELECT count(*) FROM exchange_record WHERE prizeId = p.id AND userId=? ) takeIt, ");
			param.add(uid);
		}
		
		query.append("( SELECT count(*) FROM prize_sn ps WHERE ps.prizeId=p.id and COALESCE(ps.userId,'') = '' ) snCount ");
		query.append(" FROM prize p WHERE p.`status`=1 and p.mission_id = 0 and p.type="+type);
		query.append(" and IF(p.startTime IS NOT NULL,NOW()>=p.startTime,1=1) and if(p.endTime IS NOT NULL, NOW() <= p.endTime,1=1) ");
		query.append(" ) a WHERE a.snCount > 0 ");		
		query.append(" ORDER BY a.order_index, a.createTime DESC");
		
		return this.queryForList(ds8888playQry, query.toString(), param.toArray(), new GoodStuffsMapper(type));
	}
	
	@SuppressWarnings("unchecked")
	public List<GoodStuffs> findAll(String uid, int type){
		StringBuilder sql = new StringBuilder("");
		
		if(type == 1){
			sql.append("SELECT a.id, a.`desc`, a.gameCode, a.iconUrl, a.remain, IF (a.remain > 0 AND a.takeIt = 0, 1, 2 ) STATUS FROM ( SELECT p.*, ( SELECT gameName FROM t_games WHERE gameCode = p.gameCode ) gameName, ( SELECT count(*) FROM prize p1 LEFT JOIN prize_sn ps ON p1.id = ps.prizeId WHERE ps.userId IS NULL ) remain, ( SELECT count(*) FROM exchange_record WHERE prizeId = p.id AND userId=? ) takeIt FROM prize p WHERE p.`status`=1 AND p.type=1 ) a ORDER BY a.order_index, a.createTime DESC");
		}else if(type == 2){
			sql.append("SELECT a.id, a.`desc`, a.gameCode, a.iconUrl, a.remain, IF (a.takeIt = 0, 1, 2) STATUS FROM ( SELECT p.*, ( SELECT gameName FROM t_games WHERE gameCode = p.gameCode ) gameName, CONCAT( CAST( DATEDIFF(p.endTime, NOW()) AS CHAR (3) ), '天', cast( TIME_FORMAT( SEC_TO_TIME( TIME_TO_SEC(TIMEDIFF(p.endTime, now())) % (24 * 60 * 60) ), '%H:%i' ) AS CHAR (5) ) ) remain, ( SELECT count(*) FROM exchange_record WHERE prizeId = p.id AND userId=? ) takeIt FROM prize p WHERE p.`status`=1 AND NOW() BETWEEN p.startTime AND p.endTime ) a ORDER BY a.order_index, a.createTime DESC");
		}else if(type == 3){
			sql.append("SELECT a.id, a.`desc`, a.gameCode, a.iconUrl, a.cost remain, IF (a.takeIt = 0, 1, 2) `status` FROM ( SELECT p.*, ( SELECT count(*) FROM exchange_record WHERE prizeId = p.id AND userId=? ) takeIt FROM mission m LEFT JOIN prize p ON m.prize_id = p.id WHERE m.`status`=1 AND m.type=3 ) a ORDER BY a.order_index, a.createTime DESC");			
		}				
		
		return queryForList(ds8888playQry, sql.toString(), new Object[]{uid}, new GoodStuffsMapper(type));
	}
	
	public GoodStuff get(String uid, int id, int type){		
		ArrayList<Object> param = new ArrayList<Object>();
		StringBuilder sql = new StringBuilder("");
		sql.append("SELECT a.id, a.`desc`, a.gameCode, a.iconUrl, a.content, a.gameName, a.`usage`, a.snCount, a.endTime, a.cost, IF (a.takeIt = 0, 1, 2 ) status ");
		sql.append("FROM ( SELECT p.*, ( SELECT gameName FROM t_games WHERE gameCode = p.gameCode ) gameName, ");
		
		if(StringUtils.isBlank(uid)){
			sql.append(" 0 takeIt, ");
		}else{
			sql.append(" ( SELECT count(*) FROM exchange_record WHERE prizeId = p.id AND userId=? ) takeIt, ");
			param.add(uid);
		}
		
		sql.append("( SELECT count(*) FROM prize_sn ps WHERE ps.prizeId=p.id and COALESCE(ps.userId,'') = '' ) snCount ");
		sql.append(" FROM prize p WHERE p.`status`=1 and p.type="+type);
		sql.append(" and IF(p.startTime IS NOT NULL,NOW()>=p.startTime,1=1) and if(p.endTime IS NOT NULL, NOW() <= p.endTime,1=1) ");
		sql.append(" ) a WHERE a.id=?");
		
		param.add(id);
		return (GoodStuff)queryForObject(ds8888playQry, sql.toString(), param.toArray(), new GoodStuffMapper(type));
	}
	
	private class GoodStuffsMapper implements RowMapper<Object> {
		
		private int type;
		
		GoodStuffsMapper(int type){
			this.type = type;
		}
		
		public GoodStuffs mapRow(ResultSet rs, int rowNum) throws SQLException {
			GoodStuffs view = new GoodStuffs();
			
			view.setId(rs.getLong("id"));
			view.setDesc(rs.getString("desc"));			
			view.setGameCode(rs.getString("gameCode"));
			view.setIconUrl(rs.getString("iconUrl"));
			view.setStatus(rs.getInt("status"));
			
			switch(type){
			case 1:
				view.setRemain(String.format("剩餘%s份", String.valueOf(rs.getInt("snCount"))));
				break;
			case 2:
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");				
				Date now = new Date();
				Date endTime = null;
				
				try {
					endTime = sdf.parse(rs.getString("endTime"));
				} catch (ParseException e) {
					LOG.info(e.getMessage());
				}
				
				long diff = endTime.getTime()-now.getTime();
				
				long day = diff/(1000*60*60*24);
				long hour = (diff%(1000*60*60*24))/(1000*60*60);
				long minute = (diff%(1000*60*60))/(1000*60);
				
				DecimalFormat df = new DecimalFormat("00");
				
				if(day == 0 && hour == 0 && minute == 0){
					long second = diff%(1000*60)/1000;
					view.setRemain(day+"天"+df.format(hour)+":"+df.format(minute)+":"+df.format(second));
				}else{
					view.setRemain(day+"天"+df.format(hour)+":"+df.format(minute));
				}
				
				break;
			case 3:
				view.setRemain(String.format("%s紅利", String.valueOf(rs.getInt("cost"))));
				break;
			}
			return view;
		}
	}
	
	private class GoodStuffMapper implements RowMapper<Object> {
		
		private int type;
		
		GoodStuffMapper(int type){
			this.type = type;
		}
		
		public GoodStuff mapRow(ResultSet rs, int rowNum) throws SQLException {
			GoodStuff view = new GoodStuff();
			
			view.setId(rs.getLong("id"));
			view.setContent(rs.getString("content"));
			view.setDesc(rs.getString("desc"));
			view.setGameCode(rs.getString("gameCode"));
			view.setGameName(rs.getString("gameName"));
			view.setIconUrl(rs.getString("iconUrl"));
			view.setUsage(rs.getString("usage"));
			view.setStatus(rs.getInt("status"));
			
			switch(type){
			case 1:
				view.setRemain(String.format("剩餘%s份", String.valueOf(rs.getInt("snCount"))));
				break;
			case 2:
				Date now = new Date();				
				long diff = rs.getDate("endTime").getTime()-now.getTime();
				
				long day = diff/(1000*60*60*24);
				long hour = (diff%(1000*60*60*24))/(1000*60*60);
				long minute = (diff%(1000*60*60))/(1000*60);
				
				DecimalFormat df = new DecimalFormat("00");
				
				if(day == 0 && hour == 0 && minute == 0){
					long second = diff%(1000*60)/1000;
					view.setRemain(day+"天"+df.format(hour)+":"+df.format(minute)+":"+df.format(second));
				}else{
					view.setRemain(day+"天"+df.format(hour)+":"+df.format(minute));
				}
				break;
			case 3:
				view.setRemain(String.format("%s紅利", String.valueOf(rs.getInt("cost"))));
				break;
			}
			
			return view;
		}
	}
	
}
