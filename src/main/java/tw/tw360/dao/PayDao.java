package tw.tw360.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;
import org.springside.modules.orm.Page;

import tw.tw360.dto.PayDto;

import javax.annotation.Resource;
import javax.sql.DataSource;

@SuppressWarnings("rawtypes")
@Repository
public class PayDao extends BaseDao {

	@Resource(name = "ds8888playQry")
	private DataSource ds8888playQry;

	@Resource(name = "ds8888playUpd")
	private DataSource ds8888playUpd;

	@Autowired
	public PayDao(@Qualifier("ds8888playUpd") DataSource dataSource) {
		setDataSource(dataSource);
	}
	
	@SuppressWarnings("unchecked")
	public Page<PayDto> getPayList(Integer uid, Integer pageNo, Integer limit) {
		StringBuffer sql = new StringBuffer(
				"select gm.id,gm.efunOrderId orderId,gm.stone,gm.orderTime time FROM t_game_moneys gm WHERE uid=? ORDER BY gm.modifiedTime DESC");
		StringBuffer sqlCnt = new StringBuffer(
				"SELECT COUNT(*) FROM t_game_moneys gm WHERE uid=?");
		sql.append(" LIMIT " + (pageNo - 1) * limit + "," + limit);
		Page<PayDto> page = new Page<PayDto>(limit);
		page.setPageNo(pageNo);
		super.queryForPage(ds8888playQry, page, sql.toString(), sqlCnt.toString(), new Object[] { uid }, PayDto.class);
		return page;
	}	
	
}