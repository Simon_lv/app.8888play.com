package tw.tw360.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springside.modules.orm.Page;

import tw.tw360.dao.GameRecordDao;
import tw.tw360.dto.view.App;
import tw.tw360.dto.view.Apps;
import tw.tw360.dto.view.Games;
import tw.tw360.dto.view.Recomms;

@Service
@Transactional
public class GameRecordManager {

	@Autowired
    private GameRecordDao gameRecordDao;
	
	@Transactional(propagation=Propagation.NOT_SUPPORTED, readOnly=true)
	public List<Apps> queryAll(String uid) {
		return gameRecordDao.queryAll(uid);
	}
	
	@Transactional(propagation=Propagation.NOT_SUPPORTED, readOnly=true)
	public App getByGameCode(String gameCode) {
		return gameRecordDao.getByGameCode(gameCode);
	}
	
	@Transactional(propagation=Propagation.NOT_SUPPORTED, readOnly=true)
	public Page<Games> pageSearch(Page<Games> page, int pageNo, int pageSize){
		return gameRecordDao.search(page, pageNo, pageSize);
	}
	
	public List<Recomms> findRecomms(){
		return gameRecordDao.findRecomms();
	}
	
}