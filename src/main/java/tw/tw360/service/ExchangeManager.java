package tw.tw360.service;

import java.sql.Timestamp;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springside.modules.orm.Page;

import tw.tw360.constants.Config;
import tw.tw360.dao.BonusRecordDao;
import tw.tw360.dao.ExchangeDao;
import tw.tw360.dao.PrizeDao;
import tw.tw360.dao.PrizeSnDao;
import tw.tw360.dto.ExchangeRecord;
import tw.tw360.dto.Prize;
import tw.tw360.dto.PrizeSn;
import tw.tw360.dto.view.GoodStuffs;

@Service
@Transactional
public class ExchangeManager {
	
	@Autowired
	private ExchangeDao exchangeDao;
	@Autowired
	private BonusRecordDao bonusRecordDao;
	@Autowired
	private PrizeDao prizeDao;
	@Autowired
	private PrizeSnDao prizeSnDao;

	public String exchange(String uid, long id, String deviceId, String ip) {
		int bonus = exchangeDao.getBonusBalence(uid);		
		Prize prize = prizeDao.findOneById(id);
		PrizeSn prizeSn = prizeSnDao.findOneByPrizeId(id);
		
		if(prize == null) {
			return Config.PRIZE_FAIL;
		} else if((bonus - prize.getCost()) < 0) {
			return Config.BONUS_FAIL;
		} else if(prizeSn == null) {
			return Config.PRIZE_SN_FAIL;
		} else {
			int i = prizeSnDao.update(prizeSn.getId(), deviceId, ip, uid);
			if(i == 0) {
				return Config.PRIZE_SN_FAIL;
			} else {
				ExchangeRecord exchangeRecord = new ExchangeRecord();
				exchangeRecord.setCost(prize.getCost());
				exchangeRecord.setCreateTime(new Timestamp(System.currentTimeMillis()));
				exchangeRecord.setDeviceId(deviceId);
				exchangeRecord.setPrizeId(prize.getId().intValue());
				exchangeRecord.setPrizeSnId(prizeSn.getId());
				exchangeRecord.setSourceIp(ip);
				exchangeRecord.setUserId(uid);
				exchangeDao.addExchangeRecord(exchangeRecord);
				return prizeSn.getSn();
			}
		}
		
	}

	@Transactional(propagation=Propagation.NOT_SUPPORTED, readOnly=true)
	public int userTotalBonus(String uid){
		return exchangeDao.userTotalBonus(uid);
	}
	
	@Transactional(propagation=Propagation.NOT_SUPPORTED, readOnly=true)
	public Page<GoodStuffs> findAll(Page<GoodStuffs> page, int pageNo, int pageSize, String uid, int type) {
		return exchangeDao.findAll(page, pageNo, pageSize, uid, type);
	}
	
	@Transactional(propagation=Propagation.NOT_SUPPORTED, readOnly=true)
	public Map<String, Object> findExchangeDetail(String uid, long prizeId) {
		return exchangeDao.findExchangeDetail(uid, prizeId);
	}
	
	@Transactional(propagation=Propagation.NOT_SUPPORTED, readOnly=true)
	public ExchangeRecord findAll(String uid, long prizeId) {
		return exchangeDao.findExchangeRecord(uid, prizeId);
	}
	
	public Integer getBonusBalence(String uid) {
		return exchangeDao.getBonusBalence(uid);
	}
	
	public Page<tw.tw360.dto.ExchangeList> exchangeList(String uid, Integer pageNo, Integer pageSize) {
		return exchangeDao.exchangeList(uid, pageNo, pageSize);
	}
	
	public ExchangeRecord findExchangeRecord(String uid, long prizeId) {
		return exchangeDao.findExchangeRecord(uid, prizeId);
	}
}