package tw.tw360.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import tw.tw360.dao.GoodStuffDao;
import tw.tw360.dto.view.GoodStuff;
import tw.tw360.dto.view.GoodStuffs;

@Service
@Transactional
public class GoodStuffManager {

	@Autowired
	private GoodStuffDao goodStuffDao;
	
	@Transactional(propagation=Propagation.NOT_SUPPORTED, readOnly=true)
	public List<GoodStuffs> queryAll(String uid, int type){
		return goodStuffDao.queryAll(uid, type);
	}
	
	@Transactional(propagation=Propagation.NOT_SUPPORTED, readOnly=true)
	public List<GoodStuffs> findAll(String uid, int type) {
		return goodStuffDao.findAll(uid, type);
	}
	
	public GoodStuff get(String uid, int id, int type){
		return goodStuffDao.get(uid, id, type);
	}
	
}
