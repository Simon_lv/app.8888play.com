package tw.tw360.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import tw.tw360.dao.ConfigDao;

@Service
@Transactional
public class ConfigManager {

	@Autowired
	private ConfigDao configDao;
	
	@Transactional(propagation=Propagation.NOT_SUPPORTED, readOnly=true)
	public String getHomeVideoUrl(){
		return configDao.getHomeVideoUrl();
	}
	
	public tw.tw360.dto.view.Config getConfig(String gameCode){
		return configDao.getConfig(gameCode);
	}
	
	public tw.tw360.dto.view.Config getVersionConfig(String gameCode, String version){
		return configDao.getVersionConfig(gameCode, version);
	}
	
}
