package tw.tw360.service;

import java.io.File;

import org.apache.log4j.Logger;

import tw.tw360.common.net.FtpUtil;

public class FileUploadService {
	
	/** logger */
	protected static Logger logger = Logger.getLogger(FileUploadService.class);	

	public static void toFtp(File file, String serverPath) {
		try {
	        FtpUtil.upload(serverPath, file);
		} 
		catch(Exception e) {
			logger.warn("ftp upload fail", e);
		}		
	}	
}