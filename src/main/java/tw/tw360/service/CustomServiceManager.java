package tw.tw360.service;

import java.sql.Timestamp;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springside.modules.orm.Page;

import tw.tw360.dao.CustomServiceDao;
import tw.tw360.dto.BaseDto.STATUS;
import tw.tw360.dto.CustomServiceImage;
import tw.tw360.dto.CustomServiceProblem;
import tw.tw360.dto.GamePlayerProblem;
import tw.tw360.dto.view.ReplyDetail;
import tw.tw360.dto.view.ReplyList;

@Service
@Transactional
public class CustomServiceManager {
	
	@Autowired
    private CustomServiceDao customServiceDao;
	
	@Transactional(propagation=Propagation.NOT_SUPPORTED, readOnly=true)
	public int getCsNotify(String uid) {
		return customServiceDao.getCsNotify(uid) > 0 ? STATUS.NORMAL.getValue() : STATUS.NONE.getValue();
	}	

	public void question(String uid, int type, String gameCode, String serverCode, String roleid, 
			String roleName, String title, String content, String url,
			String phone, String email, String deviceVer, String diviceOs, String deviceModel) {
		GamePlayerProblem problem = new GamePlayerProblem();
		
		problem.setUid(uid);
		problem.setQuestionType(type);
		problem.setGameCode(gameCode);
		problem.setServerCode(serverCode);
		problem.setRoleid(roleid);
		problem.setQuestionsTitle(title);
		problem.setTheQuestions(content);
		problem.setCreateTime(new Timestamp(System.currentTimeMillis()));
		problem.setItem4(url);
		problem.setItem1(phone);
		problem.setItem3(email);
		problem.setItem10(roleName);
		problem.setDeviceVer(deviceVer);
		problem.setDeviceOs(diviceOs);
		problem.setDeviceModel(deviceModel);
		
		customServiceDao.addQuestion(problem);
		
		// User info
		/*
		long _uid = Long.parseLong(uid);
		boolean isVIP = userDao.isVIP(_uid);
		
		if(isVIP) {
			userDao.updateContactVIP(_uid, phone, email);
		}
		
		userDao.updateContactUser(_uid, phone, email);
		*/
	}
	
	@Transactional(propagation=Propagation.NOT_SUPPORTED, readOnly=true)
	public Page<ReplyList> queryByUid(Page<ReplyList> page, int pageNo, int pageSize, String uid) {
		return customServiceDao.queryByUid(page, pageNo, pageSize, uid);
	}
	
	public List<ReplyDetail> queryReplyDetailById(int id) {
		customServiceDao.updateStatus(id, STATUS.READ.getValue());
		return customServiceDao.queryReplyDetailById(id);
	}
	
	@Transactional(propagation=Propagation.NOT_SUPPORTED, readOnly=true)
	public GamePlayerProblem queryProblemById(int id) {
		return customServiceDao.queryProblemById(id);
	}
	
	@Transactional(propagation=Propagation.NOT_SUPPORTED, readOnly=true)
	public CustomServiceProblem queryReplyById(int id) {
		return customServiceDao.queryReplyById(id);
	}
	
	public void reply(String uid, String title, String content, String url, GamePlayerProblem history, int score) {
		if(StringUtils.isEmpty(content)) {
			// update t_gameplayer_problem set score=?,item7=now()
			customServiceDao.updateScore(history.getTgppid(), score, true);
		}
		else {
			// insert into t_cusservice_problem
			Timestamp current = new Timestamp(System.currentTimeMillis());
			CustomServiceProblem problem = new CustomServiceProblem();
			
			problem.setReplyUid(uid);
			problem.setGameCode(history.getGameCode());
			problem.setServerCode(history.getServerCode());
			problem.setTgppid(history.getTgppid());
			problem.setReplyContent(content);
			problem.setReplyTime(current);
			problem.setPlateform(history.getPlateform());
			problem.setItem1("player");
			
			CustomServiceProblem entity = customServiceDao.addReply(problem);
			entity.setTcspid(entity.getId());
			
			if(StringUtils.isNotEmpty(url)) {
				CustomServiceImage image = new CustomServiceImage();
				
				image.setTcspid(entity.getTcspid());
				image.setUrl(url);
				image.setCreateTime(current);		
				
				customServiceDao.addImage(image);
			}
			
			// update t_gameplayer_problem set replyState=null,score=?,item7=now()
			customServiceDao.updateScore(history.getTgppid(), score, false);
		}
	}
	
	public int updateReplyState(int id, int score){		
		return customServiceDao.updateReplyState(id, score);
	}
	
}