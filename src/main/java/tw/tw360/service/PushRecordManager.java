package tw.tw360.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import tw.tw360.dao.PushRecordDao;
import tw.tw360.dto.PushRecord;

@Service
@Transactional
public class PushRecordManager {
	@Autowired
    private PushRecordDao pushRecordDao;
	
	@Transactional(propagation=Propagation.NOT_SUPPORTED, readOnly=true)
	public PushRecord findByIdForPush(long id) {
		return pushRecordDao.findByIdForPush(id); 
	}

	public int updateBeforeSend(int id) {//, int status
		return pushRecordDao.updateBeforeSend(id);//, status
	}

	public PushRecord findByIdForIosPush(int id) {
		return pushRecordDao.findByIdForIosPush(id); 
	}

	public int updateBeforeIosSend(int id) {//, int status
		return pushRecordDao.updateBeforeIosSend(id);//, status
	}
}
