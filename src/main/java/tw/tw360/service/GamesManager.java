package tw.tw360.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import tw.tw360.dao.GamesDao;
import tw.tw360.dto.view.Games;

import java.util.List;

@Service
@Transactional
public class GamesManager {

	@Autowired
    private GamesDao gamesDao;
	
	@Transactional(propagation=Propagation.NOT_SUPPORTED, readOnly=true)
	public String getAppKey(String gameCode) {
		return gamesDao.getAppKey(gameCode);
	}

	@Transactional(propagation=Propagation.NOT_SUPPORTED, readOnly=true)
	public List<Games> getGameCodeAndName(String uid) {
		return gamesDao.getGameCodeAndName(uid);
	}
}