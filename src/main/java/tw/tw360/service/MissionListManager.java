package tw.tw360.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springside.modules.orm.Page;

import tw.tw360.dao.MissionDao;
import tw.tw360.dto.Mission;

@Service
@Transactional
public class MissionListManager {

	@Autowired
	private MissionDao missionDao;

	public Page<Mission> findByUserid(String uid, Integer pageNo, Integer pageSize) {
		return missionDao.findAllMissionByUserid(uid, pageNo, pageSize);
	}

	public String findTypeByGameCode(String gameCode) {
		return missionDao.findTypeByGameCode(gameCode);
	}

	public boolean isDiamondVip(String uid) {
		return missionDao.isDiamondVip(uid);
	}
}