package tw.tw360.service;

import java.sql.Timestamp;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import tw.tw360.dao.IosPushTokenDao;
import tw.tw360.dto.IosPushToken;

@Service
@Transactional
public class PushTokenManager {

	@Autowired
    private IosPushTokenDao iosPushTokenDao;
	
	public IosPushToken addIosPushToken(String version, String deviceType, String deviceToken) {
		IosPushToken token = new IosPushToken();
		
		token.setVersion(version);
		token.setDeviceType(deviceType);
		token.setDeviceToken(deviceToken);		
		token.setCreateTime(new Timestamp(System.currentTimeMillis()));
		
		return iosPushTokenDao.add(token);
	}	
	
	@Transactional(propagation=Propagation.NOT_SUPPORTED, readOnly=true)
	public List<IosPushToken> queryAll() {
		return iosPushTokenDao.queryAll();
	}

	@Transactional(propagation=Propagation.NOT_SUPPORTED, readOnly=true)
	public IosPushToken findOneByDeviceTokenAndVersion(String version, String deviceToken) {
		return iosPushTokenDao.findOneByDeviceTokenAndVersion(version, deviceToken);
	}
}