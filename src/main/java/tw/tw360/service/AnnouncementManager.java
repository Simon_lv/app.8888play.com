package tw.tw360.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springside.modules.orm.Page;

import tw.tw360.dao.AnnouncementDao;
import tw.tw360.dto.view.Announce;
import tw.tw360.dto.view.Announces;

@Service
@Transactional
public class AnnouncementManager {

	@Autowired
    private AnnouncementDao announcementDao;
	
	@Transactional(propagation=Propagation.NOT_SUPPORTED, readOnly=true)
	public Page<Announces> pageSearch(Page<Announces> page, int pageNo, int pageSize, int type, String gameCode) {
		return announcementDao.search(page, pageNo, pageSize, type, gameCode);
	}
	
	@Transactional(propagation=Propagation.NOT_SUPPORTED, readOnly=true)
	public Announce get(int id, String announceUrl) {
		return announcementDao.get(id, announceUrl);
	}
	
	@Transactional(propagation=Propagation.NOT_SUPPORTED, readOnly=true)
	public List<Announces> queryHomeAnnouncement(){
		return announcementDao.queryHomeAnnouncement();
	}
}