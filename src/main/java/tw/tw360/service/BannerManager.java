package tw.tw360.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import tw.tw360.dao.BannerDao;
import tw.tw360.dto.view.Banners;

@Service
@Transactional
public class BannerManager {

	@Autowired
    private BannerDao bannerDao;
	
	@Transactional(propagation=Propagation.NOT_SUPPORTED, readOnly=true)
	public List<Banners> queryAll() {
		return bannerDao.queryAll();
	}
}