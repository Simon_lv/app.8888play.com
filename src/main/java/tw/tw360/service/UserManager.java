package tw.tw360.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import tw.tw360.dao.UserDao;

@Service
@Transactional
public class UserManager {

	@Autowired
    private UserDao userDao;
	
	@Transactional(propagation=Propagation.NOT_SUPPORTED, readOnly=true)
	public boolean isUser(long uid) {
		return userDao.isUser(uid);
	}
	
	@Transactional(propagation=Propagation.NOT_SUPPORTED, readOnly=true)
	public boolean isVIP(long uid) {
		return userDao.isVIP(uid);
	}
	
	public void update(boolean isVIP, long uid, String phone, String email) {
		if(isVIP) {
			userDao.updateContactVIP(uid, phone, email);
			userDao.updateContactUser(uid, phone, email);
		}
		else {
			userDao.updateContactUser(uid, phone, email);
		}
	}
}