package tw.tw360.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springside.modules.orm.Page;

import tw.tw360.dao.PayDao;
import tw.tw360.dto.PayDto;

@Service
@Transactional
public class PayManager {

	@Autowired
    private PayDao payDao;
	
	@Transactional(propagation=Propagation.NOT_SUPPORTED, readOnly=true)
	public Page<PayDto> getPayList(Integer uid,Integer pageNo,Integer limit) {
		return payDao.getPayList(uid,pageNo,limit);
	}

}