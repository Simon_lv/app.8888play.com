package tw.tw360.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import tw.tw360.dao.BonusRecordDao;
import tw.tw360.dto.view.BonusRecordAPI29;

@Service
@Transactional
public class BonusRecordManager {
	@Autowired
	private BonusRecordDao bonusRecordDao;
	
	@Transactional(propagation=Propagation.NOT_SUPPORTED, readOnly=true)
	public List<BonusRecordAPI29> list(int pageNo, int pageSize, String uid) {
		return bonusRecordDao.list(pageNo, pageSize, uid);
	}
}
