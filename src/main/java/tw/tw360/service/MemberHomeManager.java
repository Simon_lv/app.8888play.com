package tw.tw360.service;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.alibaba.fastjson.JSONObject;

import tw.tw360.constants.Config;
import tw.tw360.dao.MemberHomeDao;
import tw.tw360.dao.MissionDao;
import tw.tw360.dto.MemberHome;
import tw.tw360.dto.view.User;
import tw.tw360.exception.AuthCodeFailException;
import tw.tw360.exception.SendSMSFailException;
import tw.tw360.utils.HttpUtil;
import tw.tw360.utils.IteSmsUtil;
import tw.tw360.utils.MD5Util;
import tw.tw360.utils.MailUtil;
import tw.tw360.utils.RandomStringUtil;
import tw.tw360.utils.URL;

import java.util.HashMap;
import java.util.Map;

@Service
@Transactional
public class MemberHomeManager {

	private static final Logger LOG = LoggerFactory.getLogger(MemberHomeManager.class);

	@Autowired
	private MemberHomeDao memberHomeDao;

	@Autowired
    private MissionDao missionDao;
	
	public MemberHome findInfo(String uid) {
		return memberHomeDao.findInfo(uid);
	}

	public MemberHome findNotVIPInfo(String uid) {
		return memberHomeDao.findNotVIPInfo(uid);
	}

	public boolean checkFirstBonus(String uid) {
		if (null == memberHomeDao.findBonus(uid)) {
			return true;
		}
		return false;
	}

	public Integer getBonus(String missionId) {
		return memberHomeDao.getBonus(missionId);
	}

	public void addBonus(String uid, String missionId, Integer bonus) {
		memberHomeDao.addBonus(uid, missionId, bonus);
	}

	public Integer getBonusBalence(String uid) {
		return memberHomeDao.getBonusBalence(uid);
	}

	public boolean checkAddBonusToday(String uid) {
		if (null != memberHomeDao.findAddBonusToday(uid)) {
			return true;
		}
		return false;
	}

	public boolean updateUser(Integer userId, String email, String phone, String authCode) throws Exception {
		if (StringUtils.isBlank(phone)) {
			memberHomeDao.updateUserEmail(userId, email);
			return true;
		} else {
			String msnqq = memberHomeDao.getAuthCode(userId);

			if (msnqq != null && msnqq.equals(authCode)) {
				memberHomeDao.updateUser(userId, email, phone);
				
				//查詢首次手機綁定贈送點數 mission的ID
	        	int mid = missionDao.getPhoneFirstBindMission();
	        	
	        	Map<String, String> map = new HashMap<String,String>();
	        	String uid = userId.toString();
	        	String id = String.valueOf(mid);
	        	map.put("uid", uid);
	        	map.put("id", id);
	        	map.put("token", MD5Util.crypt(uid+id+"null").toUpperCase());
	        	
	        	JSONObject result = (JSONObject) JSONObject.parse(HttpUtil.sendPost(URL.MISSION_BONUS_URL.toString(), map));
	        	String code = result.get("code").toString();
	        	
	        	if(code.equals("0000") || code.equals("9006")){//取得點數成功或已經拿過
	        		return true;
	        	}else{
	        		throw new Exception();
	        	}
			} else {
				LOG.info("[MemberHomeManager.updateUser][userId:" + userId + "AuthCodeFailException]");
				throw new AuthCodeFailException();
			}
		}
	}

	public User checkUser(String loginName, String email, String phone) throws Exception {
		return memberHomeDao.checkUser(loginName, email, phone);
	}

	public boolean updatePwd(Long userId, boolean sendSMS) {
		return memberHomeDao.updatePwd(userId, sendSMS);
	}

	public boolean sendPwdEmail(String email) throws Exception {
		notifyByEmail(new String[] { email }, "您的新密碼是:" + Config.FORGOT_PWD + ", 請記得登入後修改密碼");
		return true;
	}

	private void notifyByEmail(String[] sendTo, String content) throws Exception {
		String subject = "忘記密碼";

		if (sendTo.length == 0 || StringUtils.isEmpty(content)) {
			return;
		}

		MailUtil.sendMail(sendTo, content, subject);
	}

	public boolean sendPwd(Long userId, String phone) throws Exception {		
		IteSmsUtil isu = new IteSmsUtil();

		try {// 待测试
			Map<String, String> retHM = isu.sendSMS("您的新密碼是:" + Config.FORGOT_PWD + ", 請記得登入後修改密碼", phone, "7200");
			String theSmsRetStatus = retHM.get("status");

			if (theSmsRetStatus != null && theSmsRetStatus.equals("0")) {
				System.out.println("send SMS Success");
			} else {
				LOG.info("[MemberHomeManager.sendPwd][userId:" + userId + " SendSMSFailException]");
				throw new SendSMSFailException();
			}
		} catch (Exception e) {
			LOG.error(e.getMessage());			
			return false;
		}

		return true;		
	}

	public boolean checkAuthCode(Long userId){
		return memberHomeDao.checkAuthCode(userId);
	}
	
	public boolean sendAuthCode(Long i, String phone) throws Exception {
		String randomCode = RandomStringUtil.generateCheckCode();
		IteSmsUtil isu = new IteSmsUtil();
		
		try {
			Map<String, String> retHM = isu.sendSMS("您的簡訊驗證碼:" + randomCode + ", 30分鐘內有效", phone, "7200");
			String theSmsRetStatus = retHM.get("status");

			if (theSmsRetStatus != null && theSmsRetStatus.equals("0")) {
				memberHomeDao.updateAuthCode(i, randomCode);
				LOG.info("[MemberHomeManager.sendAuthCode][userId:" + i + " send SMS Success]");
			}else{
				return false;
			}
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;		
	}

	public String checkAppVersion(String gameCode, String version) {
		return memberHomeDao.checkAppVersion(gameCode, version);
	}

}
