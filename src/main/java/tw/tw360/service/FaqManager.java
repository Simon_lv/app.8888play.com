package tw.tw360.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springside.modules.orm.Page;

import tw.tw360.dao.FaqDao;
import tw.tw360.dto.view.Faq;

@Service
@Transactional
public class FaqManager {

	@Autowired
	private FaqDao faqDao;
	
	public Page<Faq> pageSearch(Page<Faq> page, int pageNo, int pageSize, int type){
		return faqDao.search(page, pageNo, pageSize, type);
	}
	
	public List<Faq> findTitle(int type){
		return faqDao.findTitle(type);
	}
	
//	public Faq get(int id){
//		return faqDao.get(id);
//	}
	
}
