package tw.tw360.service;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import tw.tw360.dao.BonusRecordDao;

@Service
@Transactional
public class MissionBonusManager {

	@Autowired
	private BonusRecordDao bonusRecordDao;

	public boolean checkBonus(String uid, String mid, String gameCode) {
		if (StringUtils.isBlank(gameCode)) {
			if (null == bonusRecordDao.findDailyBonus(uid, mid)) {
				return true;
			} else {
				return false;
			}
		} else {
			if (checkMissionGameCodeExist(mid, gameCode)) {
				if (null == bonusRecordDao.findBonus(uid, mid, gameCode)) {
					return true;
				} else {
					return false;
				}
			} else {
				return false;
			}
		}
	}
	
	public boolean bonusNotTaken(String uid, String mid){
		return bonusRecordDao.bonusNotTaken(uid, mid);
	}

	public boolean checkMissionGameCodeExist(String mid, String appGameCode) {
		boolean pass = false;
		if (null != bonusRecordDao.checkMidExist(mid)) {
			pass = true;
		}
		if (pass == true && null != bonusRecordDao.checkGameCodeExist(appGameCode)) {
			pass = true;
		} else {
			pass = false;
		}
		return pass;
	}

	public boolean checkMissionExist(String uid, String mid, String gameCode)	{
		boolean pass = false;
		
		if(null == bonusRecordDao.checkMissionExist(uid, mid, gameCode)){
			pass = true;
		}
		
		return pass;
	}
	
	public boolean checkMissionExist(String mid) {
		boolean pass = false;
		if (null != bonusRecordDao.checkMidExist(mid)) {
			pass = true;
		}
		return pass;
	}

	public void addBonus(String uid, String mid, String sourceIp) {
		bonusRecordDao.addBonus(uid, mid, sourceIp);
	}

}
