package tw.tw360.service;

import java.sql.Timestamp;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springside.modules.orm.Page;

import tw.tw360.dao.ExchangeDao;
import tw.tw360.dao.PrizeDao;
import tw.tw360.dao.PrizeSnDao;
import tw.tw360.dto.ExchangeRecord;
import tw.tw360.dto.Prize;
import tw.tw360.dto.PrizeSn;
import tw.tw360.dto.view.Detail;
import tw.tw360.dto.view.FreeList;

@Service
@Transactional
public class PrizeManager {
	
	private static final Logger LOG = LoggerFactory.getLogger(PrizeManager.class);
	
	@Autowired
    private PrizeDao prizeDao;
	@Autowired
    private PrizeSnDao prizeSnDao;
	@Autowired
    private ExchangeDao exchangeDao;
	
	@Transactional(propagation=Propagation.NOT_SUPPORTED, readOnly=true)
	public Page<FreeList> pageSearch(Page<FreeList> page, int pageNo, int pageSize, String uid) {
		return prizeDao.queryByUid(page, pageNo, pageSize, uid);
	}
	
	@Transactional(propagation=Propagation.NOT_SUPPORTED, readOnly=true)
	public Detail queryByUid(int id, String uid){
		return prizeDao.queryByUid(id, uid);
	}
		
	public String getPrizeSn(long prizeId, String uid, String deviceId, String sourceIp) {
		// 檢核 Prize 是否有效
		Prize prize = prizeDao.queryById(prizeId);
		
		if(prize == null) {
			LOG.info(String.format("PrizeId[%s] is not found or disabled", prizeId));
			return null;
		}
		// 取序號		
		PrizeSn sn = prizeSnDao.getPrizeSn(prizeId, uid, deviceId, sourceIp);
		
		if(sn == null) {
			LOG.info(String.format("UserId[%s] PrizeId[%s] get prize_sn fail", uid, prizeId));
			return null;
		}else {
			ExchangeRecord record = new ExchangeRecord();
			
			record.setPrizeId(prizeId);
			record.setPrizeSnId(sn.getPrizeId());
			record.setUserId(uid);
			record.setCost(prize.getCost());
			record.setDeviceId(deviceId);
			record.setSourceIp(sourceIp);
			record.setCreateTime(new Timestamp(System.currentTimeMillis()));
			
			exchangeDao.addExchangeRecord(record);			
			return sn.getSn();
		}
	}
}