package tw.tw360.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springside.modules.orm.Page;

import tw.tw360.dao.NotifyDao;
import tw.tw360.dto.BaseDto.STATUS;
import tw.tw360.dto.view.Notify;
import tw.tw360.dto.view.NotifyList;

@Service
@Transactional
public class SysManager {
		
	@Autowired
    private NotifyDao notifyDao;
		
	@Transactional(propagation=Propagation.NOT_SUPPORTED, readOnly=true)
	public int getSysNotify(String uid) {
		return notifyDao.getSysNotify(uid) > 0 ? STATUS.NORMAL.getValue() : STATUS.NONE.getValue();
	}
	
	@Transactional(propagation=Propagation.NOT_SUPPORTED, readOnly=true)
	public Page<NotifyList> queryByUid(Page<NotifyList> page, int pageNo, int pageSize, String uid) {
		return notifyDao.queryByUid(page, pageNo, pageSize, uid);
	}
	
	public Notify queryById(long id) {
		notifyDao.updateStatus(id, STATUS.READ.getValue());
		return notifyDao.queryById(id);
	}
}