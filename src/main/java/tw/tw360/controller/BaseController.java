package tw.tw360.controller;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import tw.tw360.exception.EmailFormatFailException;
import tw.tw360.exception.TokenException;
import tw.tw360.service.GamesManager;
import tw.tw360.utils.MD5Util;

import javax.servlet.http.HttpServletRequest;
import java.net.InetAddress;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Base Controller
 * 
 * @author Eric
 *
 */
public class BaseController {	
	
	/** logger */
	protected static Logger logger = Logger.getLogger(BaseController.class);
	/** NAT Header */
	private static final String[] HEADERS_TO_TRY = { 
	    "X-Forwarded-For",
	    "Proxy-Client-IP",
	    "WL-Proxy-Client-IP",
	    "HTTP_X_FORWARDED_FOR",
	    "HTTP_X_FORWARDED",
	    "HTTP_X_CLUSTER_CLIENT_IP",
	    "HTTP_CLIENT_IP",
	    "HTTP_FORWARDED_FOR",
	    "HTTP_FORWARDED",
	    "HTTP_VIA",
	    "REMOTE_ADDR" };
	
	@Autowired
	protected GamesManager gamesManager;
		
	/**
	 * 回傳 json 資料 
	 * 
	 * @param code
	 * @param message
	 * @param data
	 * @return
	 */
	protected Map<String, Object> result(String code, String message, Map<String, Object> data) {
		Map<String, Object> result = new LinkedHashMap<String, Object>();
		
		result.put("code", code);
		result.put("message", message);
		
		for(Entry<String, Object> e : data.entrySet()) {
			/*
			Object value = e.getValue();			
			// Null
			if(value == null) {
				throw new DataNotFoundException();
			}
			// Empty List or Map
			if((value instanceof Collection && CollectionUtils.isEmpty((Collection)(value))) 
					|| (value instanceof Map && ((Map)(value)).isEmpty())) {
				throw new DataNotFoundException();
			}			
			*/
			result.put(e.getKey(), e.getValue());
		}
		
		return result;
	}
	
	/**
	 * APP_KEY 檢核
	 * <p>
	 * 不用傳 app_key
	 * 
	 * @param token
	 * @param params
	 * @return
	 */
	protected void validate(String appGameCode, String token, Object... params) throws Exception {
		StringBuilder keys = new StringBuilder();		
		
		for(Object param : params) {
			keys.append(param);
		}
		
		
		keys.append(getAppKey(appGameCode));
		
		String md5 = MD5Util.crypt(keys.toString());
		logger.info(String.format("validate token: %s <=> %s(%s)", token, md5, keys));
		
		if(!StringUtils.equalsIgnoreCase(token, md5)) {
			throw new TokenException();
		}
	}

	protected void emailValidate(String email){
		Pattern p =  Pattern.compile("\\w+([-+.]\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*");//复杂匹配
		Matcher m = p.matcher(email);
		if(!m.matches()) {
			throw new EmailFormatFailException();
		}
	}
	
	/**
	 * 取得 Client IP
	 * 
	 * @param request
	 * @return
	 */
	protected String getClientIpAddress(HttpServletRequest request) {
	    for(String header : HEADERS_TO_TRY) {
	        String ip = request.getHeader(header);
	        
	        if(StringUtils.isNotEmpty(ip) && !StringUtils.equalsIgnoreCase(ip, "unknown")) {
	        	return ip;
	        }	        
	    }
	    
	    String ip = request.getRemoteAddr();
	    
	    if(StringUtils.equals(ip, "127.0.0.1")) {	    	
	    	try {
	    		ip = InetAddress.getLocalHost().getHostAddress();
	    	}
	    	catch(Exception e) {
	    		// Do Nothing
	    	}
	    }
	    
	    return ip;
	}
	
	/**
	 * 取得 app_key
	 * 
	 * @return
	 */
	protected String getAppKey(String appGameCode) {
		return gamesManager.getAppKey(appGameCode);
	}
}