package tw.tw360.controller;

import java.util.LinkedHashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.json.MappingJackson2JsonView;

import tw.tw360.constants.Config;
import tw.tw360.exception.DataNotFoundException;
import tw.tw360.exception.EmptyFileException;
import tw.tw360.exception.ExchangeTakenException;
import tw.tw360.exception.FileSizeLimitException;
import tw.tw360.exception.IDFAException;
import tw.tw360.exception.NoBondFailException;
import tw.tw360.exception.ParamRequiredException;
import tw.tw360.exception.TokenException;
import tw.tw360.exception.AuthCodeFailException;
import tw.tw360.exception.SendSMSTimeLimitException;
import tw.tw360.exception.SendSMSFailException;
import tw.tw360.exception.SendEmailFailException;
import tw.tw360.exception.EmailFormatFailException;

/**
 * Base Exception Handler
 * 
 * @author Eric
 *
 */
@ControllerAdvice
public class BaseExceptionHandler {
	
	/** logger */
	protected static Logger logger = Logger.getLogger(BaseExceptionHandler.class);
	
    /**
     * Token Exception
     * 
     * @param request
     * @param e
     * @return
     */
    @ExceptionHandler(value = {TokenException.class})
    public ModelAndView tokenErrorHandler(HttpServletRequest request, Exception e) {
    	ModelAndView mav = new ModelAndView(new MappingJackson2JsonView());
    	String code = Config.TOKEN_FAIL;
    	String message = getMessage(code);
    	
    	mav.addAllObjects(getAllObjects("code", code, "message", message));
                
        return mav;
    }
    
    /**
     * authCode error Exception
     * 
     * @param request
     * @param e
     * @return
     */
    @ExceptionHandler(value = {AuthCodeFailException.class})
    public ModelAndView authCodeErrorHandler(HttpServletRequest request, Exception e) {
    	ModelAndView mav = new ModelAndView(new MappingJackson2JsonView());
    	String code = Config.AUTHCODE_FAIL;
    	String message = getMessage(code);
    	
    	mav.addAllObjects(getAllObjects("code", code, "message", message));
                
        return mav;
    }
    
    /**
     * Send SMS Time Limit Error Exception
     * 
     * @param request
     * @param e
     * @return
     */
    @ExceptionHandler(value = {SendSMSTimeLimitException.class})
    public ModelAndView sendSMSTimeLimitErrorHandler(HttpServletRequest request, Exception e) {
    	ModelAndView mav = new ModelAndView(new MappingJackson2JsonView());
    	String code = Config.SEND_SMS_TIME_LIMIT_FAIL;
    	String message = getMessage(code);
    	
    	mav.addAllObjects(getAllObjects("code", code, "message", message));
                
        return mav;
    }
    
    /**
     * Send SMS Fail Exception
     * 
     * @param request
     * @param e
     * @return
     */
    @ExceptionHandler(value = {SendSMSFailException.class})
    public ModelAndView sendSMSFailErrorHandler(HttpServletRequest request, Exception e) {
    	ModelAndView mav = new ModelAndView(new MappingJackson2JsonView());
    	String code = Config.SEND_SMS_FAIL;
    	String message = getMessage(code);
    	
    	mav.addAllObjects(getAllObjects("code", code, "message", message));
                
        return mav;
    }
    
    /**
     * Send Email Fail Exception
     * 
     * @param request
     * @param e
     * @return
     */
    @ExceptionHandler(value = {SendEmailFailException.class})
    public ModelAndView sendEmailFailErrorHandler(HttpServletRequest request, Exception e) {
    	ModelAndView mav = new ModelAndView(new MappingJackson2JsonView());
    	String code = Config.SEND_EMAIL_FAIL;
    	String message = getMessage(code);
    	
    	mav.addAllObjects(getAllObjects("code", code, "message", message));
                
        return mav;
    }
    
    /**
     * Email Format Fail Exception
     * 
     * @param request
     * @param e
     * @return
     */
    @ExceptionHandler(value = {EmailFormatFailException.class})
    public ModelAndView emailFormatFailErrorHandler(HttpServletRequest request, Exception e) {
    	ModelAndView mav = new ModelAndView(new MappingJackson2JsonView());
    	String code = Config.EMAIL_FORMAT_FAIL;
    	String message = getMessage(code);
    	
    	mav.addAllObjects(getAllObjects("code", code, "message", message));
                
        return mav;
    }    
    
    /**
     * Upload File Over Size Exception
     * 
     * @param request
     * @param e
     * @return
     */
    @ExceptionHandler(value = {FileSizeLimitException.class})
    public ModelAndView overSizeErrorHandler(HttpServletRequest request, Exception e) {
    	ModelAndView mav = new ModelAndView(new MappingJackson2JsonView());
    	String code = Config.UPLOAD_OVERSIZED;
    	String message = getMessage(code);
    	
    	mav.addAllObjects(getAllObjects("code", code, "message", message));
                
        return mav;
    }
    
    /**
     * Data Not Found
     * 
     * @param request
     * @param e
     * @return
     */
    @ExceptionHandler(value = {DataNotFoundException.class})
    public ModelAndView dataNotFoundHandler(HttpServletRequest request, Exception e) {
    	ModelAndView mav = new ModelAndView(new MappingJackson2JsonView());
    	String code = Config.DATA_NOT_FOUND;
    	String message = getMessage(code);
    	
    	mav.addAllObjects(getAllObjects("code", code, "message", message));
                
        return mav;
    }
    
    @ExceptionHandler(value = {NoBondFailException.class})
    public ModelAndView noBondHandler(HttpServletRequest request, Exception e) {
    	ModelAndView mav = new ModelAndView(new MappingJackson2JsonView());
    	String code = Config.NO_BOND_FAIL;
    	String message = getMessage(code);
    	
    	mav.addAllObjects(getAllObjects("code", code, "message", message));
                
        return mav;
    }
    
    /**
     * Upload Empty File Exception
     * 
     * @param request
     * @param e
     * @return
     */
    @ExceptionHandler(value = {EmptyFileException.class})
    public ModelAndView uploadEmptyErrorHandler(HttpServletRequest request, Exception e) {
    	ModelAndView mav = new ModelAndView(new MappingJackson2JsonView());
    	String code = Config.UPLOAD_EMPTY;
    	String message = getMessage(code);
    	
    	mav.addAllObjects(getAllObjects("code", code, "message", message));
                
        return mav;
    }
    
    /**
     * Exchange already taken with uid + prize id
     * 
     * @param request
     * @param e
     * @return
     */
    @ExceptionHandler(value = {ExchangeTakenException.class})
    public ModelAndView exchangeTakenErrorHandler(HttpServletRequest request, Exception e) {
    	ModelAndView mav = new ModelAndView(new MappingJackson2JsonView());
    	String code = Config.EXCHANGE_TAKEN;
    	String message = getMessage(code);
    	
    	mav.addAllObjects(getAllObjects("code", code, "message", message));
                
        return mav;
    }
    
    /**
     * idfa 
     * @param request
     * @param e
     * @return
     */
    @ExceptionHandler(value = {IDFAException.class})
    public ModelAndView IDFAErrorHandler(HttpServletRequest request, Exception e) {
    	ModelAndView mav = new ModelAndView(new MappingJackson2JsonView());
    	String code = Config.IDFA_FAIL;
    	String message = getMessage(code);
    	
    	mav.addAllObjects(getAllObjects("code", code, "message", message));
                
        return mav;
    }
    
    /**
     * Param Required Missing
     * 
     * @param request
     * @param e
     * @return
     */
    @ExceptionHandler(value = {ParamRequiredException.class})
    public ModelAndView paramRequiredErrorHandler(HttpServletRequest request, Exception e) {
    	ModelAndView mav = new ModelAndView(new MappingJackson2JsonView());
    	String code = Config.PARAMETER_ERROR;
    	String message = getMessage(code);
    	
    	mav.addAllObjects(getAllObjects("code", code, "message", message));
                
        return mav;
    }
    
    /**
     * Default Exception
     * 
     * @param request
     * @param e
     * @return
     */
    @ExceptionHandler(value = {RuntimeException.class, Exception.class, RuntimeException.class})
    public ModelAndView defaultErrorHandler(HttpServletRequest request, Exception e) {
    	
    	logger.warn("Runtime Error...", e);
    	
    	ModelAndView mav = new ModelAndView(new MappingJackson2JsonView());
    	String code = Config.RETURN_SYSTEM_ERROR;
    	String message = getMessage(code);
    	
    	if(StringUtils.isEmpty(message)) {
    		message = e.getMessage();
    	}
        
    	mav.addAllObjects(getAllObjects("code", code, "message", message));
        
        return mav;
    }
    
    /**
     * json 資料集合
     * <p>
     * "key", value...
     * 
     * @param objects
     * @return
     */
    private Map<String, String> getAllObjects(String... objects) {
    	Map<String, String> allObjects = new LinkedHashMap<String, String>();
    	String key = "";
		String value = "";
    	
    	for(int i = 0; i < objects.length; i++) {
    		if(i % 2 == 0) {
    			key = objects[i];
    			continue;
    		}
    		else {
    			value = objects[i];
    		}
    		
    		allObjects.put(key, value);
    	}
    	
    	return allObjects;
    }
    
    /**
     * code 2 message
     * 
     * @param code
     * @return
     */
    private String getMessage(String code) {
    	return Config.RETURN_RESULT.get(code);
    }
}