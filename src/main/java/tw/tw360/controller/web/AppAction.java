package tw.tw360.controller.web;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springside.modules.orm.Page;

import tw.tw360.constants.Config;
import tw.tw360.controller.BaseController;
import tw.tw360.dto.view.Announces;
import tw.tw360.dto.view.App;
import tw.tw360.dto.view.Apps;
import tw.tw360.dto.view.Banners;
import tw.tw360.dto.view.Games;
import tw.tw360.dto.view.Recomms;
import tw.tw360.service.AnnouncementManager;
import tw.tw360.service.BannerManager;
import tw.tw360.service.GameRecordManager;
import tw.tw360.service.ConfigManager;

/**
 * AppXXX Action
 * 
 * @author Eric
 *
 */
@Controller
public class AppAction extends BaseController {
	
	@Autowired
	protected BannerManager bannerManager;
	@Autowired
	protected GameRecordManager gameRecordManager;		
	@Autowired
	protected AnnouncementManager announcementManager;
	@Autowired
	protected ConfigManager configManager;
	
	private Page<Games> page = new Page<Games>(Config.PAGE_SIZE);
	
	/**
	 * 首頁
	 * 
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="home", produces="application/json; charset=utf-8")
	public @ResponseBody Map<String, Object> home(@RequestParam(value="uid", required=false, defaultValue="") String uid) throws Exception {
		
		Map<String, Object> data = new LinkedHashMap<String, Object>();
		
		List<Banners> banner = bannerManager.queryAll();
		List<Apps> gameRecord = gameRecordManager.queryAll(uid);
		List<Announces> announce = announcementManager.queryHomeAnnouncement();
		String videoUrl = configManager.getHomeVideoUrl();
		
		data.put("banners", banner);
		data.put("apps", gameRecord);
		data.put("announces", announce);
		
		if(!videoUrl.equals("")){
			data.put("videoUrl", videoUrl);
		}
		
		return result(Config.RETURN_SUCCESS_DO, Config.RETURN_RESULT.get(Config.RETURN_SUCCESS_DO), data);						
	}
	
	/**
	 * 遊戲應用詳細資訊
	 * 
	 * @param gameCode
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="appDetail", produces="application/json; charset=utf-8")
	public @ResponseBody Map<String, Object> appDetail(@RequestParam("gameCode") String gameCode) throws Exception {
		
		Map<String, Object> data = new LinkedHashMap<String, Object>();
		
		App app = gameRecordManager.getByGameCode(gameCode);
		data.put("app", app);
		
		return result(Config.RETURN_SUCCESS_DO, Config.RETURN_RESULT.get(Config.RETURN_SUCCESS_DO), data);
	}
	
	/**
	 * 遊戲應用
	 * @param pageNo
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="appHome", produces="application/json; charset=utf-8")
	public @ResponseBody Map<String, Object> appHome(@RequestParam(value="page", required=false, defaultValue="1") int pageNo) throws Exception {
		Map<String, Object> data = new LinkedHashMap<String, Object>();
		
		page.setPageNo(pageNo);		
		page = gameRecordManager.pageSearch(page, pageNo, Config.PAGE_SIZE);
		
		List<Recomms> recomms = gameRecordManager.findRecomms();
		
		data.put("totalCount", page.getTotalCount());
		data.put("games", page.getResult());
		data.put("recomms", recomms);
		
		return result(Config.RETURN_SUCCESS_DO, Config.RETURN_RESULT.get(Config.RETURN_SUCCESS_DO), data);
	}
	
	/**
	 * 儲值鈕遮蔽
	 * 
	 * @param gameCode
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="payButton", produces="application/json; charset=utf-8")
	public @ResponseBody Map<String, Object> payButton(@RequestParam("appGameCode") String gameCode, @RequestParam("version") String version) throws Exception {
		
		Map<String, Object> data = new LinkedHashMap<String, Object>();
    	String ver = "";
//    	tw.tw360.dto.view.Config config = configManager.getConfig(gameCode);
    	
//		if(config == null){
//			data.put("isOpen", 0);
//		}else{
//			if(gameCode.equals("app")){
//				ver = config.getAppVersion();
//			}else{
//				ver = config.getAppiosVersion();
//			}
//			
//			if (version.compareTo(ver) >= 0) {
//				data.put("isOpen", 1);			
//			} else {
//				data.put("isOpen", 0);
//			}
//		}
		
		tw.tw360.dto.view.Config currentConfig = configManager.getVersionConfig(gameCode, version);
		
		if(currentConfig==null){
			data.put("isOpen", 0);
			data.put("isPay", 0);
			data.put("isBonus", 0);			
		}else{
			data.put("isOpen", 1);
			data.put("isPay", currentConfig.getIsPay());
			data.put("isBonus", currentConfig.getIsBonus());
		}
		return result(Config.RETURN_SUCCESS_DO, Config.RETURN_RESULT.get(Config.RETURN_SUCCESS_DO), data);
	}
	
}