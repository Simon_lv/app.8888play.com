package tw.tw360.controller.web;

import java.util.LinkedHashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springside.modules.orm.Page;

import tw.tw360.constants.Config;
import tw.tw360.controller.BaseController;
import tw.tw360.dto.view.Detail;
import tw.tw360.dto.view.ExchangeList;
import tw.tw360.dto.view.FreeList;
import tw.tw360.exception.ExchangeTakenException;
import tw.tw360.service.ExchangeManager;
import tw.tw360.service.PrizeManager;

/**
 * Free Action
 * 
 * @author Eric
 *
 */
@Controller
public class FreeAction extends BaseController {
	
	private Page<FreeList> freePage = new Page<FreeList>(Config.PAGE_SIZE);	    
	private Page<ExchangeList> exchangePage = new Page<ExchangeList>(Config.PAGE_SIZE);
	
	@Autowired
	protected PrizeManager prizeManager;
	@Autowired
	protected ExchangeManager exchangeManager;

	/**
	 * 免費好康列表
	 * 
	 * @param pageNo
	 * @param uid
	 * @param token
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="freeList", produces="application/json; charset=utf-8")
	public @ResponseBody Map<String, Object> freeList(@RequestParam(value="page", required=false, defaultValue="1") int pageNo,
			@RequestParam("appGameCode") String appGameCode, @RequestParam("uid") String uid, @RequestParam("token") String token) throws Exception {
		// token 檢核
		validate(appGameCode, token, uid, pageNo);
		
		Map<String, Object> data = new LinkedHashMap<String, Object>();
		
		freePage.setPageNo(pageNo);
		freePage = prizeManager.pageSearch(freePage, pageNo, Config.PAGE_SIZE, uid);
		
		data.put("totalCount", freePage.getTotalCount());
		data.put("freeList", freePage.getResult());
		
		
		return result(Config.RETURN_SUCCESS_DO, Config.RETURN_RESULT.get(Config.RETURN_SUCCESS_DO), data);						
	}
	
	/**
	 * 免費好康內頁
	 * 
	 * @param id
	 * @param uid
	 * @param token
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="freeDetail", produces="application/json; charset=utf-8")
	public @ResponseBody Map<String, Object> freeDetail(@RequestParam("id") int id, 
			@RequestParam("appGameCode") String appGameCode, @RequestParam("uid") String uid, @RequestParam("token") String token) throws Exception {
		// token 檢核
		validate(appGameCode, token, uid, id);
		
		Map<String, Object> data = new LinkedHashMap<String, Object>();
		Detail detail = prizeManager.queryByUid(id, uid);
		data.put("detail", detail);
		
		return result(Config.RETURN_SUCCESS_DO, Config.RETURN_RESULT.get(Config.RETURN_SUCCESS_DO), data);						
	}
	
	/**
	 * 免費好康領取
	 * 
	 * @param id
	 * @param uid
	 * @param deviceId
	 * @param token
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="freeReceive", produces="application/json; charset=utf-8")
	public @ResponseBody Map<String, Object> freeReceive(@RequestParam("id") long id, 
			@RequestParam("appGameCode") String appGameCode, @RequestParam("uid") String uid, @RequestParam("deviceId") String deviceId, 
			@RequestParam("token") String token, HttpServletRequest req) throws Exception {
		// token 檢核
		validate(appGameCode, token, uid, id);
		
		logger.info("deviceId:" + deviceId);
		
		// 用戶是否已領過該任務
		if(exchangeManager.findExchangeRecord(uid, id) != null) {
			throw new ExchangeTakenException(); 
		}		
		
		Map<String, Object> data = new LinkedHashMap<String, Object>();
		String sn = prizeManager.getPrizeSn(id, uid, deviceId, getClientIpAddress(req));
		
		if(StringUtils.isEmpty(sn)) {
			return result(Config.PRIZE_SN_FAIL, Config.RETURN_RESULT.get(Config.PRIZE_SN_FAIL), data);
		}
		
		data.put("sn", sn);
		
		return result(Config.RETURN_SUCCESS_DO, Config.RETURN_RESULT.get(Config.RETURN_SUCCESS_DO), data);						
	}
	
	/**
	 * 免費好康領取記錄
	 * 
	 * @param pageNo
	 * @param uid
	 * @return
	 * @throws Exception
	 */
//	@RequestMapping(value="freeRecord", produces="application/json; charset=utf-8")
//	public @ResponseBody Map<String, Object> freeRecord(@RequestParam(value="page", required=false, defaultValue="1") int pageNo,
//			@RequestParam("type") int type, @RequestParam("appGameCode") String appGameCode, @RequestParam("uid") String uid, 
//			@RequestParam("token") String token) throws Exception {
//		// token 檢核
//		validate(appGameCode, token, uid, pageNo);		
//		
//		Map<String, Object> data = new LinkedHashMap<String, Object>();
//						
//		exchangePage.setPageNo(pageNo);
//		exchangePage = exchangeManager.findAll(exchangePage, pageNo, Config.PAGE_SIZE, uid, type);
//		
//		data.put("totalCount", exchangePage.getTotalCount());
//		data.put("freeList", exchangePage.getResult());		
//		
//		return result(Config.RETURN_SUCCESS_DO, Config.RETURN_RESULT.get(Config.RETURN_SUCCESS_DO), data);						
//	}
}