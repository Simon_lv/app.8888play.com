package tw.tw360.controller.web;

import java.util.LinkedHashMap;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springside.modules.orm.Page;

import tw.tw360.constants.Config;
import tw.tw360.controller.BaseController;
import tw.tw360.dto.ExchangeList;
import tw.tw360.service.ExchangeManager;

@Controller
public class ExchangeListAction extends BaseController {
	
	@Autowired
	private ExchangeManager exchangeManager;
	
	@RequestMapping(value = "exchangeList", produces = "application/json; charset=utf-8")
	public @ResponseBody Map<String, Object> findInfo(@RequestParam(value = "uid", required = false, defaultValue = "") String uid,
			@RequestParam(value = "page", required = false, defaultValue = "1") String page,
			@RequestParam(value = "token", required = false, defaultValue = "") String token, 
			@RequestParam(value = "appGameCode", required = false, defaultValue = "") String appGameCode) throws Exception {
		
		Map<String, Object> data = new LinkedHashMap<String, Object>();
		
		if (StringUtils.isBlank(uid) || StringUtils.isBlank(token) || StringUtils.isBlank(appGameCode)) {
			return result(Config.PARAMETER_ERROR, Config.RETURN_RESULT.get(Config.PARAMETER_ERROR), data);
		}
					
		// token 檢核
		validate(appGameCode, token, uid, page);	
		
		if(!(Integer.parseInt(page) > 1)){
			data.put("bonus", exchangeManager.getBonusBalence(uid));
		}
		
		Page<ExchangeList> list= exchangeManager.exchangeList(uid, Integer.valueOf(page), 20);
		
		data.put("totalCount", list.getTotalCount());
		data.put("list", list.getResult());
		
		return result(Config.RETURN_SUCCESS_DO, Config.RETURN_RESULT.get(Config.RETURN_SUCCESS_DO), data);		
	}	
}