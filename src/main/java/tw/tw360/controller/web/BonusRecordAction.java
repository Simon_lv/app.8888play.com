package tw.tw360.controller.web;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import tw.tw360.constants.Config;
import tw.tw360.controller.BaseController;
import tw.tw360.dto.view.BonusRecordAPI29;
import tw.tw360.service.BonusRecordManager;

/**
*
*  API 29
* @author Beck
*
*/

@Controller
public class BonusRecordAction extends BaseController {
	
	@Autowired
	private BonusRecordManager bonusRecordManager;
	
	@RequestMapping(value = "bonusRecord", produces = "application/json; charset=utf-8")
	public @ResponseBody Map<String, Object> bonusRecord(String appGameCode, String uid, int page, String token) throws Exception {
		Map<String, Object> result = new HashMap<String, Object>();
		
		String resultCode = Config.RETURN_SUCCESS_DO;
		
		validate(appGameCode, token, uid, page);
		
		List<BonusRecordAPI29> list = bonusRecordManager.list(page, Config.PAGE_SIZE, uid);
		result.put("list", list);
		result.put("code", resultCode);
		result.put("totalCount", list.size());
		result.put("message", Config.RETURN_RESULT.get(resultCode));
		return result;		
	}
}