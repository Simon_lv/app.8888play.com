package tw.tw360.controller.web;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import tw.tw360.constants.Config;
import tw.tw360.controller.BaseController;
import tw.tw360.service.BannerApiManager;

@Controller
public class BannerApiAction extends BaseController {
	
	@Autowired
	private BannerApiManager bannerApiManager;
	
	@RequestMapping(value = "splash", produces = "application/json; charset=utf-8")
	public @ResponseBody Map<String, Object> find() {
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			String resultCode = Config.RETURN_SUCCESS_DO;
			result.put("url", bannerApiManager.queryAll());
			result.put("code", resultCode);
			result.put("message", Config.RETURN_RESULT.get(resultCode));
			return result;
		} catch (Exception e) {
			String resultCode = Config.RETURN_SYSTEM_ERROR;
			result.put("code", resultCode);
			result.put("message", Config.RETURN_RESULT.get(resultCode));
			return result;
		}
	}

}
