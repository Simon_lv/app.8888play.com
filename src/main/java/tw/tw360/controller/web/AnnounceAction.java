package tw.tw360.controller.web;

import java.util.LinkedHashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springside.modules.orm.Page;

import tw.tw360.constants.Config;
import tw.tw360.controller.BaseController;
import tw.tw360.dto.view.Announces;
import tw.tw360.service.AnnouncementManager;

/**
 * AnnounceXXX Action
 * 
 * @author Eric
 *
 */
@Controller
public class AnnounceAction extends BaseController {
	
	private Page<Announces> page = new Page<Announces>(Config.PAGE_SIZE);
	
	@Autowired
	protected AnnouncementManager announcementManager;

	/**
	 * 遊戲公告列表
	 * 
	 * @param pageNo
	 * @param gameCode
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="announceList", produces="application/json; charset=utf-8")
	public @ResponseBody Map<String, Object> announceList(@RequestParam(value="page", required=false, defaultValue="1") int pageNo,
			@RequestParam(value="gameCode", required=false, defaultValue="") String gameCode,
			@RequestParam("type") int type) throws Exception {
		
		Map<String, Object> data = new LinkedHashMap<String, Object>();
						
		page.setPageNo(pageNo);
		page = announcementManager.pageSearch(page, pageNo, Config.PAGE_SIZE, type, gameCode);
		
		data.put("totalCount", page.getTotalCount());
		data.put("announces", page.getResult());		
		
		return result(Config.RETURN_SUCCESS_DO, Config.RETURN_RESULT.get(Config.RETURN_SUCCESS_DO), data);						
	}
	
	/**
	 * 公告內容
	 * 
	 * @param id
	 * @return
	 */
	@RequestMapping(value="announce", produces="application/json; charset=utf-8")
	public @ResponseBody Map<String, Object> announce(@RequestParam("id") int thisId) {
		Map<String, Object> data = new LinkedHashMap<String, Object>();
						
//		Announce announce = announcementManager.get(thisId, URL.ANNOUNCE_URL.toString());
//		data.put("announce", announce);
		
		return result(Config.RETURN_SUCCESS_DO, Config.RETURN_RESULT.get(Config.RETURN_SUCCESS_DO), data);						
	}
}