package tw.tw360.controller.web;

import java.util.LinkedHashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springside.modules.orm.Page;

import tw.tw360.constants.Config;
import tw.tw360.controller.BaseController;
import tw.tw360.dto.view.Notify;
import tw.tw360.dto.view.NotifyList;
import tw.tw360.service.CustomServiceManager;
import tw.tw360.service.SysManager;

@Controller
public class SysAction extends BaseController {
	
	private Page<NotifyList> page = new Page<NotifyList>(Config.PAGE_SIZE);
	
	@Autowired
	protected SysManager sysManager;
	@Autowired
	protected CustomServiceManager customServiceManager;
	
	/**
	 * 客服新消息通知
	 * 
	 * @param uid
	 * @param receiveTime
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="serviceNotify", produces="application/json; charset=utf-8")
	public @ResponseBody Map<String, Object> serviceNotify(@RequestParam("uid") String uid) throws Exception {
		Map<String, Object> data = new LinkedHashMap<String, Object>();
		
		data.put("notify", customServiceManager.getCsNotify(uid));
		data.put("sysNotify", sysManager.getSysNotify(uid));
		
		return result(Config.RETURN_SUCCESS_DO, Config.RETURN_RESULT.get(Config.RETURN_SUCCESS_DO), data);						
	}
	
	/**
	 * 系統通知列表
	 * 
	 * @param pageNo
	 * @param uid
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="sysServiceList", produces="application/json; charset=utf-8")
	public @ResponseBody Map<String, Object> sysServiceList(@RequestParam(value="page", required=false, defaultValue="1") int pageNo,
			@RequestParam("uid") String uid) throws Exception {
		Map<String, Object> data = new LinkedHashMap<String, Object>();
						
		page.setPageNo(pageNo);
		page = sysManager.queryByUid(page, pageNo, Config.PAGE_SIZE, uid);
		
		data.put("totalCount", page.getTotalCount());
		data.put("list", page.getResult());		
		
		return result(Config.RETURN_SUCCESS_DO, Config.RETURN_RESULT.get(Config.RETURN_SUCCESS_DO), data);						
	}
	
	/**
	 * 系統消息內頁
	 * 
	 * @param id
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="sysServiceDetail", produces="application/json; charset=utf-8")
	public @ResponseBody Map<String, Object> sysServiceDetail(@RequestParam("id") long id) throws Exception {
		Map<String, Object> data = new LinkedHashMap<String, Object>();
				
		Notify notify = sysManager.queryById(id);
		data.put("detail", notify);
		
		return result(Config.RETURN_SUCCESS_DO, Config.RETURN_RESULT.get(Config.RETURN_SUCCESS_DO), data);						
	}
}