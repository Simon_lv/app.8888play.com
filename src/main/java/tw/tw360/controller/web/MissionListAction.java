package tw.tw360.controller.web;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springside.modules.orm.Page;

import tw.tw360.constants.Config;
import tw.tw360.controller.BaseController;
import tw.tw360.dto.BaseDto;
import tw.tw360.dto.Mission;
import tw.tw360.dto.MissionList;
import tw.tw360.dto.MissionListWithNoGameCode;
import tw.tw360.service.MissionListManager;

@Controller
public class MissionListAction extends BaseController {
	@Autowired
	private MissionListManager missionManager;

	@RequestMapping(value = "missionList", produces = "application/json; charset=utf-8")
	public @ResponseBody Map<String, Object> findInfo(@RequestParam(value = "uid", required = false, defaultValue = "") String uid,
			@RequestParam(value = "page", required = false, defaultValue = "") String page,
			@RequestParam(value = "token", required = false, defaultValue = "") String token,
			@RequestParam(value = "appGameCode", required = false, defaultValue = "") String appGameCode) throws Exception {

		Map<String, Object> data = new LinkedHashMap<String, Object>();

		if (StringUtils.isBlank(uid) || StringUtils.isBlank(token) || StringUtils.isBlank(page) || StringUtils.isBlank(appGameCode)) {
			return result(Config.PARAMETER_ERROR, Config.RETURN_RESULT.get(Config.PARAMETER_ERROR), data);
		}

		// token 檢核
		validate(appGameCode, token, uid, page);

//		if (missionManager.isDiamondVip(uid)) {
			Page<?> fromDB = missionManager.findByUserid(uid, Integer.valueOf(page), 20);
			@SuppressWarnings("unchecked")
			List<Mission> result = (List<Mission>) fromDB.getResult();
			List<BaseDto> output = new ArrayList<BaseDto>();
			for (Mission mission : result) {
				MissionList ml = new MissionList();
				MissionListWithNoGameCode mlngc = new MissionListWithNoGameCode();
				if (StringUtils.isNotBlank(mission.getGameCode())) {
					mlngc.setId(mission.getId());
					mlngc.setOrder(mission.getOrderIndex());
					mlngc.setIconUrl(mission.getIconUrl());
					mlngc.setBonus(mission.getBonus());
					mlngc.setContent(mission.getContent());
					mlngc.setStatus(mission.getStatus());
					mlngc.setGameCode(mission.getGameCode());
					mlngc.setIosUrl(mission.getIosUrl());
					mlngc.setType(missionManager.findTypeByGameCode(mission.getGameCode()));
					output.add(mlngc);
				} else {
					ml.setId(mission.getId());
					ml.setOrder(mission.getOrderIndex());
					ml.setIconUrl(mission.getIconUrl());
					ml.setBonus(mission.getBonus());
					ml.setContent(mission.getContent());
					ml.setStatus(mission.getStatus());
					ml.setIosUrl(mission.getIosUrl());
					output.add(ml);
				}
			}
			data.put("list", output);
			data.put("totalCount", fromDB.getTotalCount());
			return result(Config.RETURN_SUCCESS_DO, Config.RETURN_RESULT.get(Config.RETURN_SUCCESS_DO), data);
//		}
//		return result(Config.NO_DIAMOND_VIP, Config.RETURN_RESULT.get(Config.NO_DIAMOND_VIP), data);
	}
}