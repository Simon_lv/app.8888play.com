package tw.tw360.controller.web;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import tw.tw360.constants.Config;
import tw.tw360.controller.BaseController;

/**
 *
 *  API 23, 33
 * @author Beck
 *
 */
@Controller
public class RedirectUrlAction extends BaseController {
	private static final Logger LOG = LoggerFactory.getLogger(RedirectUrlAction.class);

	@RequestMapping(value = "termsPage", produces = "application/json; charset=utf-8")
	public @ResponseBody Map<String, Object> termsPage() {
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			String resultCode = Config.RETURN_SUCCESS_DO;
			String serviceUrl = "http://www.8888play.com/Terms_Member.html";
			String privacyUrl = "http://www.8888play.com/Terms_Privacy.html";
			
			result.put("serviceUrl", serviceUrl);
			result.put("privacyUrl", privacyUrl);
			result.put("code", resultCode);
			result.put("message", Config.RETURN_RESULT.get(resultCode));
			return result;
		} catch(Exception e) {
			LOG.error("[RedirectAction][termsPage]",e);
			String resultCode = Config.RETURN_SYSTEM_ERROR;
			result.put("code", resultCode);
			result.put("message", Config.RETURN_RESULT.get(resultCode));
			return result;
		}
	}
	
	@RequestMapping(value = "bankDeposit", produces = "application/json; charset=utf-8")
	public @ResponseBody Map<String, Object> bankDeposit() {
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			String resultCode = Config.RETURN_SUCCESS_DO;
			String pageUrl = "http://app.8888play.com/deposit/Deposit_Bank.html";
			
			result.put("pageUrl", pageUrl);
			result.put("code", resultCode);
			result.put("message", Config.RETURN_RESULT.get(resultCode));
			return result;
		} catch(Exception e) {
			LOG.error("[RedirectAction][bankDeposit]",e);
			String resultCode = Config.RETURN_SYSTEM_ERROR;
			result.put("code", resultCode);
			result.put("message", Config.RETURN_RESULT.get(resultCode));
			return result;
		}
	}
	
}
