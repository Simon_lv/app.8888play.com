package tw.tw360.controller.web;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springside.modules.orm.Page;
import org.apache.commons.lang3.StringUtils;

import tw.tw360.constants.Config;
import tw.tw360.controller.BaseController;
import tw.tw360.dto.view.GoodStuff;
import tw.tw360.dto.view.GoodStuffs;
import tw.tw360.exception.ExchangeTakenException;
import tw.tw360.exception.IDFAException;
import tw.tw360.service.ExchangeManager;
import tw.tw360.service.GoodStuffManager;
import tw.tw360.service.PrizeManager;
import tw.tw360.utils.URL;

@Controller
public class GoodStuffAction extends BaseController {

	private Page<GoodStuffs> exchangePage = new Page<GoodStuffs>(Config.PAGE_SIZE);

	@Autowired
	private GoodStuffManager goodStuffManager;	
	@Autowired
	private ExchangeManager exchangeManager;
	@Autowired
	private PrizeManager prizeManager;

	/**
	 * 2016/8/24 Simon 查詢好康頁面
	 * 
	 * @param appGameCode
	 * @param uid
	 * @param token
	 * @param type
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "goodStuffList", produces = "application/json; charset=utf-8")
	public @ResponseBody Map<String, Object> goodStuffList(String uid,
			@RequestParam(value = "type", required = false, defaultValue = "1") int type
			) throws Exception {
		Map<String, Object> result = new HashMap<String, Object>();
		
		List<GoodStuffs> goodStuffs = goodStuffManager.queryAll(uid, type);
		int bonus = exchangeManager.getBonusBalence(uid);

		result.put("bonus", bonus);
		result.put("descriptionUrl", URL.GOODSTUFF_DESCRIPTION_URL.toString());		
		result.put("goodStuffs", goodStuffs);					
		return result(Config.RETURN_SUCCESS_DO, Config.RETURN_RESULT.get(Config.RETURN_SUCCESS_DO), result);
	}

	@RequestMapping(value = "goodStuffDetail", produces = "application/json; charset=utf-8")
	public @ResponseBody Map<String, Object> goodStuffDetail(String uid, 
				@RequestParam(value = "id", required=false, defaultValue="0") int prizeId, 
				@RequestParam(value = "type", required=false, defaultValue="1") int type) throws Exception {
		Map<String, Object> result = new HashMap<String, Object>();
		
		if (prizeId <= 0) {
			return result(Config.PARAMETER_ERROR, Config.RETURN_RESULT.get(Config.PARAMETER_ERROR), result);
		}
		
		GoodStuff goodStuff = goodStuffManager.get(uid, prizeId, type);
		int bonus = exchangeManager.getBonusBalence(uid);
		
		result.put("bonus", bonus);
		result.put("descriptionUrl", URL.GOODSTUFF_DESCRIPTION_URL.toString());
		result.put("goodStuff", goodStuff);
		
		return result(Config.RETURN_SUCCESS_DO, Config.RETURN_RESULT.get(Config.RETURN_SUCCESS_DO), result);
	}

	/**
	 * 好康領取
	 * 
	 * @param id
	 * @param uid
	 * @param deviceId
	 * @param token
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "goodStuffReceive", produces = "application/json; charset=utf-8")
	public @ResponseBody Map<String, Object> freeReceive(@RequestParam(value="id", required = false, defaultValue="0") long id,
			String appGameCode, String uid, String deviceId, String token, HttpServletRequest req)
			throws Exception {
		Map<String, Object> data = new LinkedHashMap<String, Object>();
		
		if(StringUtils.isBlank(deviceId)){
			throw new IDFAException();
		}
		
		if(id <= 0 || StringUtils.isBlank(appGameCode) || StringUtils.isBlank(uid) || StringUtils.isBlank(token)){
			return result(Config.PARAMETER_ERROR, Config.RETURN_RESULT.get(Config.PARAMETER_ERROR), data);
		}
		
		// token 檢核
		validate(appGameCode, token, uid, id);

		logger.info("deviceId:" + deviceId);

		// 用戶是否已領過該任務
		if (exchangeManager.findExchangeRecord(uid, id) != null) {
			throw new ExchangeTakenException();
		}

		int bonus = exchangeManager.getBonusBalence(uid);
		
		synchronized(this){
			String sn = prizeManager.getPrizeSn(id, uid, deviceId, getClientIpAddress(req));

			if (StringUtils.isEmpty(sn)) {
				return result(Config.PRIZE_SN_FAIL, Config.RETURN_RESULT.get(Config.PRIZE_SN_FAIL), data);
			}else{
				data.put("sn", sn);
			}
		}
		
		data.put("bonus", bonus);

		return result(Config.RETURN_SUCCESS_DO, Config.RETURN_RESULT.get(Config.RETURN_SUCCESS_DO), data);
	}

	/**
	 * 好康領取記錄
	 * 
	 * @param pageNo
	 * @param uid
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "goodStuffRecord", produces = "application/json; charset=utf-8")
	public @ResponseBody Map<String, Object> goodStuffRecord(
			@RequestParam(value = "page", required = false, defaultValue = "1") int pageNo,
			@RequestParam(value="type", required = false, defaultValue="0") int type, 
			@RequestParam("appGameCode") String appGameCode,
			@RequestParam("uid") String uid, @RequestParam("token") String token) throws Exception {
		Map<String, Object> data = new LinkedHashMap<String, Object>();
		
		if(StringUtils.isBlank(appGameCode) || StringUtils.isBlank(uid) || StringUtils.isBlank(token)){
			return result(Config.PARAMETER_ERROR, Config.RETURN_RESULT.get(Config.PARAMETER_ERROR), data);
		}
		
		// token 檢核
		validate(appGameCode, token, uid, pageNo);

		exchangePage.setPageNo(pageNo);
		exchangePage = exchangeManager.findAll(exchangePage, pageNo, Config.PAGE_SIZE, uid, type);

		data.put("totalCount", exchangePage.getTotalCount());
		data.put("goodStuffs", exchangePage.getResult());

		return result(Config.RETURN_SUCCESS_DO, Config.RETURN_RESULT.get(Config.RETURN_SUCCESS_DO), data);
	}

}
