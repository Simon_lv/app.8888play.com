package tw.tw360.controller.web;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;

import javax.net.ssl.SSLException;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import tw.tw360.constants.Config;
import tw.tw360.controller.BaseController;
import tw.tw360.dto.IosPushToken;
import tw.tw360.dto.PushRecord;
import tw.tw360.service.PushRecordManager;
import tw.tw360.service.PushTokenManager;
import tw.tw360.utils.Configuration;

import com.notnoop.apns.APNS;
import com.notnoop.apns.ApnsService;
import com.notnoop.apns.PayloadBuilder;
import com.relayrides.pushy.apns.ApnsClient;
import com.relayrides.pushy.apns.ApnsClientBuilder;
import com.relayrides.pushy.apns.ClientNotConnectedException;
import com.relayrides.pushy.apns.PushNotificationResponse;
import com.relayrides.pushy.apns.util.ApnsPayloadBuilder;
import com.relayrides.pushy.apns.util.SimpleApnsPushNotification;
import com.relayrides.pushy.apns.util.TokenUtil;

import io.netty.util.concurrent.Future;

/**
 * API 35
 * 
 * @author Beck
 *
 */
@Controller
public class PushAction extends BaseController {

	@Autowired
	private PushRecordManager pushRecordManager;
	@Autowired
	private PushTokenManager pushTokenManager;
	
	@RequestMapping(value = "push", produces = "application/json; charset=utf-8")
	public @ResponseBody Map<String, Object> push(String appGameCode, int id, String token) throws Exception {
		Map<String, Object> result = new HashMap<String, Object>();

		String resultCode = Config.RETURN_SUCCESS_DO;

		validate(appGameCode, token, id);

		String resultCode0 = doPush(id);
		String resultCode1 = doIosPush(id);
		if (resultCode0.equals(Config.RETURN_SUCCESS_DO) && resultCode1.equals(Config.RETURN_SUCCESS_DO)) {
			resultCode = Config.RETURN_SUCCESS_DO;
		} else if (!resultCode0.equals(Config.RETURN_SUCCESS_DO) && resultCode1.equals(Config.RETURN_SUCCESS_DO)) {
			resultCode = Config.RETURN_PUSH_ANDROID_FAIL;
		} else if (resultCode0.equals(Config.RETURN_SUCCESS_DO) && !resultCode1.equals(Config.RETURN_SUCCESS_DO)) {
			resultCode = Config.RETURN_PUSH_IOS_FAIL;
		} else {
			resultCode = Config.RETURN_PUSH_FAIL;
		}
		result.put("code", resultCode);
		result.put("message", Config.RETURN_RESULT.get(resultCode));
		logger.info("[PushAction]result:" + result.toString());
		return result;
	}

	private String doPush(int id) throws JSONException {
		String isTest = Boolean.parseBoolean(Configuration.getInstance().getProperty("isTestServer")) ? "test." : "";
		String androidPushPassword = Configuration.getInstance().getProperty(isTest + "android_push_password");
		// String androidSenderID =
		// Configuration.getInstance().getProperty(isTest+"android_Sender_ID");
		// 前端用的..
		String androidChannel = Configuration.getInstance().getProperty(isTest + "android_Channel");

		PushRecord pushRecord = null;
		try {
			pushRecord = pushRecordManager.findByIdForPush(id);
			if (pushRecord == null) {
				return Config.PARAMETER_ERROR;
			}
		} catch (Exception e) {
			logger.error("[PushAction][findByIdForPush]", e);
			return Config.RETURN_SYSTEM_ERROR;
		}

		String msgCnt = pushRecord.getContent();
		String msgId = "PID[" + pushRecord.getId() + "]TID[" + pushRecord.getTraceId() + "]";
		String img = StringUtils.isNotBlank(pushRecord.getImgUrl()) ? "IMG[" + pushRecord.getImgUrl() + "]" : "";
		String msg = msgCnt + msgId + img;

		try {
			// Prepare JSON containing the GCM message content. What to send and
			// where to send.
			JSONObject jGcmData = new JSONObject();
			JSONObject jData = new JSONObject();
			jData.put("title", pushRecord.getTitle());
			jData.put("message", msg);
			// Where to send GCM message.
			// if (args.length > 1 && args[1] != null) {
			// jGcmData.put("to", args[1].trim());
			// } else {
			jGcmData.put("to", androidChannel);
			// }
			// What to send in GCM message.
			jGcmData.put("data", jData);

			// Create connection to send GCM Message request.
			URL url = new URL("https://android.googleapis.com/gcm/send");
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setRequestProperty("Authorization", "key=" + androidPushPassword);
			conn.setRequestProperty("Content-Type", "application/json");
			conn.setRequestMethod("POST");
			conn.setDoOutput(true);

			// Send GCM message content.
			OutputStream outputStream = conn.getOutputStream();
			outputStream.write(jGcmData.toString().getBytes());

			// Read GCM response.
			InputStream inputStream = conn.getInputStream();
			String resp = IOUtils.toString(inputStream);
			logger.info(resp);
			logger.info("Check your device/emulator for notification or logcat for confirmation of the receipt of the GCM message.");

			if (pushRecordManager.updateBeforeSend(id) == 0) {// , 2
				logger.error("[PushAction][updateBeforeSend]update fail");
				return Config.RETURN_SYSTEM_ERROR;
			}
		} catch (IOException e) {
			logger.error("Unable to send GCM message.");
			logger.error("Please ensure that API_KEY has been replaced by the server API key, and that the device's registration token is correct (if specified).");
			logger.error(e.getMessage());			
		}

		return Config.RETURN_SUCCESS_DO;
	}

	/**
	 * IOS推播註冊
	 * 
	 * @param version
	 * @param deviceType
	 * @param deviceToken
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "registerIosToken", produces = "application/json; charset=utf-8")
	public @ResponseBody Map<String, Object> registerIosToken(@RequestParam("version") String version,
			@RequestParam("deviceType") String deviceType, @RequestParam("deviceToken") String deviceToken)
			throws Exception {

		logger.info("deviceToken: "+deviceToken);
		
		Map<String, Object> data = new LinkedHashMap<String, Object>();

		if (pushTokenManager.findOneByDeviceTokenAndVersion(version, deviceToken) == null) {
			pushTokenManager.addIosPushToken(version, deviceType, deviceToken);
		}
		return result(Config.RETURN_SUCCESS_DO, Config.RETURN_RESULT.get(Config.RETURN_SUCCESS_DO), data);
	}
	
	private String doIosPush(int id) {
		PushRecord pushRecord = null;
		try {
			pushRecord = pushRecordManager.findByIdForIosPush(id);
			if (pushRecord == null) {
				return Config.PARAMETER_ERROR;
			}
		} catch (Exception e) {
			e.printStackTrace();
			return Config.RETURN_SYSTEM_ERROR;
		}

		String msgCnt = pushRecord.getContent();		
		String keyPass = Configuration.getInstance().getProperty("ios_push_password");
		String keyPath = Configuration.getInstance().getProperty("ios.push.key");
		
		List<IosPushToken> tokenList = pushTokenManager.queryAll();
		
		IOSPushy pushy = new IOSPushy(tokenList, msgCnt, keyPath, keyPass);
		Thread t = new Thread(pushy);
		t.start();
		
		if (pushRecordManager.updateBeforeIosSend(id) == 0) {
			logger.error("[PushAction][doIosPush]update fail");
		}
		
		return Config.RETURN_SUCCESS_DO;
	}
	
	class IOSPushy implements Runnable {
		
		List<IosPushToken> tokenList;
		String content;
		String keyPath;
		String keyPass;
		
		IOSPushy(List<IosPushToken> tokenList, String cententStr, String keyPath, String keyPass){
			this.tokenList = tokenList;
			this.content = cententStr;
			this.keyPath = keyPath;
			this.keyPass = keyPass;
		}
		
		
		@Override
		public void run() {
			// TODO Auto-generated method stub
			try {
				ApnsClient apnsClient = new ApnsClientBuilder().setClientCredentials(new File(keyPath), keyPass).build();
				
				final Future<Void> connectFuture = apnsClient.connect(ApnsClient.PRODUCTION_APNS_HOST);
				connectFuture.await();
				
				for(IosPushToken tokens : tokenList){
					final SimpleApnsPushNotification pushNotification;
					{
					    final ApnsPayloadBuilder payloadBuilder = new ApnsPayloadBuilder();
					    payloadBuilder.setAlertBody(content).setSoundFileName(ApnsPayloadBuilder.DEFAULT_SOUND_FILENAME);
					    
					    final String payload = payloadBuilder.buildWithDefaultMaximumLength();
					    
					    final String token = TokenUtil.sanitizeTokenString(tokens.getDeviceToken());

					    pushNotification = new SimpleApnsPushNotification(token, "com.baplay.app", payload);
					}
					
					final Future<PushNotificationResponse<SimpleApnsPushNotification>> sendNotificationFuture = apnsClient.sendNotification(pushNotification);
					
					try {
					    final PushNotificationResponse<SimpleApnsPushNotification> pushNotificationResponse = sendNotificationFuture.get();

					    if (pushNotificationResponse.isAccepted()) {
					        System.out.println("Push notification accepted by APNs gateway.");
					    } else {
					        System.out.println("Notification rejected by the APNs gateway: " +
					                pushNotificationResponse.getRejectionReason());

					        if (pushNotificationResponse.getTokenInvalidationTimestamp() != null) {
					            System.out.println("\t…and the token is invalid as of " +
					                pushNotificationResponse.getTokenInvalidationTimestamp());
					        }
					    }
					} catch (final ExecutionException e) {
					    System.err.println("Failed to send push notification.");
					    e.printStackTrace();

					    if (e.getCause() instanceof ClientNotConnectedException) {
					        System.out.println("Waiting for client to reconnect…");
					        try {
								apnsClient.getReconnectionFuture().await();
							} catch (InterruptedException e1) {
								// TODO Auto-generated catch block
								e1.printStackTrace();
							}
					        System.out.println("Reconnected.");
					    }
					}
				}
				
			} catch (SSLException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} catch (InterruptedException e2) {
				// TODO Auto-generated catch block
				e2.printStackTrace();
			}
		}
		
	}

}
