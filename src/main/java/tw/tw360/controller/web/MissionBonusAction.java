package tw.tw360.controller.web;

import java.util.LinkedHashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import tw.tw360.constants.Config;
import tw.tw360.controller.BaseController;
import tw.tw360.service.MissionBonusManager;

@Controller
public class MissionBonusAction extends BaseController {
	@Autowired
	private MissionBonusManager missionBonusManager;

	@RequestMapping(value = "missionBonus", produces = "application/json; charset=utf-8")
	public @ResponseBody Map<String, Object> findInfo(@RequestParam(value = "uid", required = false, defaultValue = "") String uid,
			@RequestParam(value = "id", required = false, defaultValue = "") String mid,
			@RequestParam(value = "token", required = false, defaultValue = "") String token,
			@RequestParam(value = "appGameCode", required = false, defaultValue = "") String appGameCode,
			@RequestParam(value = "gameCode", required = false, defaultValue = "") String gameCode, HttpServletRequest request) throws Exception {

		Map<String, Object> data = new LinkedHashMap<String, Object>();

		if (StringUtils.isBlank(uid) || StringUtils.isBlank(mid) || StringUtils.isBlank(token)) {
			return result(Config.PARAMETER_ERROR, Config.RETURN_RESULT.get(Config.PARAMETER_ERROR), data);
		}

		// token 檢核
		validate(appGameCode, token, uid, mid);
		
		if(missionBonusManager.bonusNotTaken(uid, mid)){
//		if (missionBonusManager.checkBonus(uid, mid, gameCode)) {
			missionBonusManager.addBonus(uid, mid, super.getClientIpAddress(request));
			return result(Config.RETURN_SUCCESS_DO, Config.RETURN_RESULT.get(Config.RETURN_SUCCESS_DO), data);
		} else {
			if (missionBonusManager.checkMissionExist(mid)) {
				return result(Config.BONUS_TAKEN, Config.RETURN_RESULT.get(Config.BONUS_TAKEN), data);
			} else {
				return result(Config.NO_SUCH_MISSION_OR_GAMECODE, Config.RETURN_RESULT.get(Config.NO_SUCH_MISSION_OR_GAMECODE), data);
			}
		}
	}
}