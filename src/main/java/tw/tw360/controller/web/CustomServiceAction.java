package tw.tw360.controller.web;

import java.io.File;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springside.modules.orm.Page;

import tw.tw360.common.net.FtpUtil;
import tw.tw360.constants.Config;
import tw.tw360.controller.BaseController;
import tw.tw360.dto.GamePlayerProblem;
import tw.tw360.dto.view.Faq;
import tw.tw360.dto.view.ReplyDetail;
import tw.tw360.dto.view.ReplyList;
import tw.tw360.exception.ParamRequiredException;
import tw.tw360.service.CustomServiceManager;
import tw.tw360.service.FaqManager;
import tw.tw360.service.UserManager;
import tw.tw360.utils.DateUtil;
import tw.tw360.utils.FileNameGenerator;
import tw.tw360.utils.URL;

@Controller
public class CustomServiceAction extends BaseController {

	private Page<ReplyList> page = new Page<ReplyList>(Config.PAGE_SIZE);

	@Autowired
	protected CustomServiceManager customServiceManager;
	@Autowired
	protected FaqManager faqManager;
	@Autowired
	protected UserManager userManager;

	/**
	 * 常見問題列表
	 * 
	 * @param type
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "faqList", produces = "application/json; charset=utf-8")
	public @ResponseBody Map<String, Object> faqList(
			@RequestParam(value = "type", required = false, defaultValue = "1") int type,
			@RequestParam(value = "page", required = false, defaultValue = "1") int pageNo) throws Exception {
		Map<String, Object> data = new LinkedHashMap<String, Object>();

		Page<Faq> faqPage = new Page<Faq>(Config.PAGE_SIZE);
		
		switch(type){
		case 1:
			type = 4;
			break;
		case 2:
			break;
		case 3:
			type = 1;
			break;
		case 4:
			type = 5;
			break;
		case 5:
			type = 6;
			break;
		case 6:
			type = 3;
			break;
		}
		
		faqPage.setPageNo(pageNo);
		faqPage = faqManager.pageSearch(faqPage, pageNo, Config.PAGE_SIZE, type);

		data.put("totalCount", faqPage.getTotalCount());
		data.put("faqs", faqPage.getResult());

		return result(Config.RETURN_SUCCESS_DO, Config.RETURN_RESULT.get(Config.RETURN_SUCCESS_DO), data);
	}

	// /**
	// * 常見問題內容
	// * @param id
	// * @return
	// * @throws Exception
	// */
	// @RequestMapping(value="faqDetail", produces="application/json;
	// charset=utf-8")
	// public @ResponseBody Map<String, Object>
	// faqDetail(@RequestParam(value="id", required=true) String faqId) throws
	// Exception {
	// Map<String, Object> data = new LinkedHashMap<String, Object>();
	//
	// if(StringUtils.isBlank(faqId)){
	// return result(Config.PARAMETER_ERROR,
	// Config.RETURN_RESULT.get(Config.PARAMETER_ERROR), data);
	// }
	//
	// Faq faq = faqManager.get(Integer.parseInt(faqId));
	// data.put("faq", faq);
	//
	// return result(Config.RETURN_SUCCESS_DO,
	// Config.RETURN_RESULT.get(Config.RETURN_SUCCESS_DO), data);
	// }

	/**
	 * 我要提問
	 * 
	 * @param token
	 * @param file
	 * @param uid
	 * @param type
	 * @param gameCode
	 * @param serverCode
	 * @param roleId
	 * @param title
	 * @param content
	 * @return
	 */
	@RequestMapping(value = "question", produces = "application/json; charset=utf-8")
	public @ResponseBody Map<String, Object> question(
			@RequestParam(value = "file", required = false) MultipartFile file, @RequestParam("json") String json)
			throws Exception {

		logger.info("file:" + file + " is empty " + (file == null || file.isEmpty()));
		logger.info("json:" + json);

		// Parse Json
		JSONObject jsonObj = new JSONObject(json);

		String appGameCode = jsonObj.has("appGameCode") ? jsonObj.getString("appGameCode") : StringUtils.EMPTY;
		String token = jsonObj.has("token") ? jsonObj.getString("token") : StringUtils.EMPTY;
		String uid = jsonObj.has("uid") ? jsonObj.getString("uid") : StringUtils.EMPTY;
		
		int type = jsonObj.has("type") ? jsonObj.getInt("type") : -1;
		String gameCode = jsonObj.has("gameCode") ? jsonObj.getString("gameCode") : StringUtils.EMPTY;
		String serverCode = jsonObj.has("serverCode") ? jsonObj.getString("serverCode") : StringUtils.EMPTY;
		String roleId = jsonObj.has("roleId") ? jsonObj.getString("roleId") : StringUtils.EMPTY;
		String roleName = jsonObj.has("roleName") ? jsonObj.getString("roleName") : StringUtils.EMPTY;
		String title = jsonObj.has("title") ? jsonObj.getString("title") : StringUtils.EMPTY;
		String content = jsonObj.has("content") ? jsonObj.getString("content") : StringUtils.EMPTY;
		String phone = jsonObj.has("phone") ? jsonObj.getString("phone") : StringUtils.EMPTY; // item1
		String email = jsonObj.has("email") ? jsonObj.getString("email") : StringUtils.EMPTY; // item3
		String deviceVer = jsonObj.has("deviceOsVersion") ? jsonObj.getString("deviceOsVersion") : StringUtils.EMPTY;
		String deviceOs = jsonObj.has("deviceOS") ? jsonObj.getString("deviceOS") : StringUtils.EMPTY;
		String deviceModel = jsonObj.has("deviceModel") ? jsonObj.getString("deviceModel") : StringUtils.EMPTY;

		if (StringUtils.isEmpty(phone) && StringUtils.isEmpty(email)) {
			throw new ParamRequiredException();
		}

		// token 檢核
		validate(appGameCode, token, uid, type, gameCode, serverCode, roleId);

		Map<String, Object> data = new LinkedHashMap<String, Object>();

		String url = fileUPload(file, Config.UPLOAD_FILE_LIMIT);

		// String url = file == null || file.isEmpty() ? StringUtils.EMPTY :
		// HandleFileUpload.upload(file, URL.FTP_8888APP_QUESTION.toString(),
		// Config.UPLOAD_FILE_LIMIT);
		// logger.info("upload completed:" + url);
		customServiceManager.question(uid, type, gameCode, serverCode, roleId, roleName, title, content, url, phone, email,
				deviceVer, deviceOs, deviceModel);
		logger.info("post question and return");
		return result(Config.RETURN_SUCCESS_DO, Config.RETURN_RESULT.get(Config.RETURN_SUCCESS_DO), data);
	}

	/**
	 * 客服回覆列表
	 * 
	 * @param pageNo
	 * @param uid
	 * @param token
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "replyList", produces = "application/json; charset=utf-8")
	public @ResponseBody Map<String, Object> replyList(
			@RequestParam(value = "page", required = false, defaultValue = "1") int pageNo,
			@RequestParam("appGameCode") String appGameCode, @RequestParam("uid") String uid,
			@RequestParam("token") String token) throws Exception {

		// token 檢核
		validate(appGameCode, token, uid, pageNo);

		Map<String, Object> data = new LinkedHashMap<String, Object>();

		page.setPageNo(pageNo);
		page = customServiceManager.queryByUid(page, pageNo, Config.PAGE_SIZE, uid);

		data.put("totalCount", page.getTotalCount());
		data.put("list", page.getResult());

		return result(Config.RETURN_SUCCESS_DO, Config.RETURN_RESULT.get(Config.RETURN_SUCCESS_DO), data);
	}

	/**
	 * 客服回覆詳細
	 * 
	 * @param uid
	 * @param id
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "replyDetail", produces = "application/json; charset=utf-8")
	public @ResponseBody Map<String, Object> replyDetail(@RequestParam("uid") String uid, @RequestParam("id") int id)
			throws Exception {

		Map<String, Object> data = new LinkedHashMap<String, Object>();

		GamePlayerProblem problem = customServiceManager.queryProblemById(id);

		if (problem != null) {
			data.put("title", problem.getQuestionsTitle());
			data.put("date", DateUtil.format(problem.getCreateTime(), "yyyy-MM-dd HH:mm:ss"));
			data.put("replyState", problem.getReplyState() == null ? 2 : problem.getReplyState());

			if (problem.getScore() != null) {
				data.put("score", problem.getScore());
			}

			List<ReplyDetail> list = customServiceManager.queryReplyDetailById(id);

			// Question
			ReplyDetail e = new ReplyDetail();

			e.setContent(problem.getTheQuestions());
			e.setDate(DateUtil.format(problem.getCreateTime(), "yyyy-MM-dd HH:mm:ss"));
			e.setId(problem.getId());

			if (StringUtils.isNotEmpty(problem.getItem4())) {
				// item4 只有放相對路徑
				e.setImgUrl(URL.CDN_CONTEXT_ROOT.toString() + problem.getItem4());
			}

			e.setOrder(1);
			e.setType(2);

			list.add(0, e);

			data.put("list", list);
		}

		return result(Config.RETURN_SUCCESS_DO, Config.RETURN_RESULT.get(Config.RETURN_SUCCESS_DO), data);
	}

	/**
	 * 回覆追問
	 * 
	 * @param token
	 * @param file
	 * @param uid
	 * @param id
	 * @param title
	 * @param content
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "reply", produces = "application/json; charset=utf-8")
	public @ResponseBody Map<String, Object> reply(@RequestParam(value = "file", required = false) MultipartFile file,
			@RequestParam("json") String json) throws Exception {

		logger.info("file:" + file + " is empty " + (file == null || file.isEmpty()));
		logger.info("json:" + json);

		// Parse Json
		JSONObject jsonObj = new JSONObject(json);

		String appGameCode = jsonObj.has("appGameCode") ? jsonObj.getString("appGameCode") : StringUtils.EMPTY;
		String token = jsonObj.has("token") ? jsonObj.getString("token") : StringUtils.EMPTY;
		String uid = jsonObj.has("uid") ? jsonObj.getString("uid") : StringUtils.EMPTY;
		;
		int id = jsonObj.has("id") ? jsonObj.getInt("id") : -1;
		String title = jsonObj.has("title") ? jsonObj.getString("title") : StringUtils.EMPTY;
		String content = jsonObj.has("content") ? jsonObj.getString("content") : StringUtils.EMPTY;
		int score = jsonObj.has("score") ? jsonObj.getInt("score") : -1;

		if (score == -1) {
			throw new ParamRequiredException();
		}

		// token 檢核
		validate(appGameCode, token, uid, id);

		GamePlayerProblem problem = customServiceManager.queryProblemById(id);

		Map<String, Object> data = new LinkedHashMap<String, Object>();

		String url = fileUPload(file, Config.UPLOAD_FILE_LIMIT);
		
//		String url = file == null || file.isEmpty() ? StringUtils.EMPTY
//				: HandleFileUpload.upload(file, URL.FTP_8888APP_QUESTION.toString(), Config.UPLOAD_FILE_LIMIT);

		// Update URL
		if (StringUtils.isNotEmpty(url)) {
			url = URL.CDN_CONTEXT_ROOT.toString() + url;
		}

		customServiceManager.reply(uid, title, content, url, problem, score);

		return result(Config.RETURN_SUCCESS_DO, Config.RETURN_RESULT.get(Config.RETURN_SUCCESS_DO), data);
	}

	/**
	 * 回覆結案
	 * 
	 * @param id
	 * @param uid
	 * @param appGameCode
	 * @param sign
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "replyState", produces = "application/json; charset=utf-8")
	public @ResponseBody Map<String, Object> replyState(
			@RequestParam(value = "id", required = false, defaultValue = "0") int id,
			@RequestParam(value = "score", required = false, defaultValue = "5") int score) throws Exception {
		Map<String, Object> data = new LinkedHashMap<String, Object>();

		if (id <= 0) {
			return result(Config.PARAMETER_ERROR, Config.RETURN_RESULT.get(Config.PARAMETER_ERROR), data);
		}

		if (customServiceManager.updateReplyState(id, score) > 0) {
			return result(Config.RETURN_SUCCESS_SSI, Config.RETURN_RESULT.get(Config.RETURN_SUCCESS_DO), data);
		} else {
			return result(Config.RETURN_SYSTEM_ERROR, Config.RETURN_RESULT.get(Config.RETURN_SYSTEM_ERROR), data);
		}
	}

	private String fileUPload(MultipartFile imgFile, int size) {
		String realPath = System.getProperty("java.io.tmpdir");

		String filename = "";
		
		if (imgFile != null && !imgFile.isEmpty() && imgFile.getSize() <= size) {
//			String type = imgFile.getContentType();
			filename = FileNameGenerator.byTime(imgFile.getOriginalFilename());
			// if (type.equals("image/jpeg")) {
			// filename = filename.concat(".jpg");
			// } else if (type.equals("image/png")) {
			// filename = filename.concat(".png");
			// } else if (type.equals("image/bmp")) {
			// filename = filename.concat(".bmp");
			// } else if (type.equals("image/gif")) {
			// filename = filename.concat(".gif");
			// } else {
			// logger.error("文件類型不正確");
			// result.put("code", "1001");
			// result.put("message", "文件類型不正確！");
			// return result;
			// }
			File file = new File(realPath, filename);
			System.out.println("上傳目錄======" + file.getAbsolutePath());
			try {
				FileUtils.copyInputStreamToFile(imgFile.getInputStream(), file);
				FtpUtil.upload8888(URL.FTP_8888APP_QUESTION.toString(), file);
				
				return URL.FTP_8888APP_QUESTION.toString() + "/"+filename;
			} catch (Exception e) {
				logger.error("文件上傳失敗", e);
				// result.put("code", "1001");
				// result.put("message", "文件上傳失敗!");
				// return result;
			} finally {
				if (file.exists()) {
					file.delete();
				}

			}

		}
		return "";

	}
}