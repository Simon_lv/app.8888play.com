package tw.tw360.controller.web;


import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springside.modules.orm.Page;

import tw.tw360.constants.Config;
import tw.tw360.controller.BaseController;
import tw.tw360.dto.MemberHome;
import tw.tw360.dto.PayDto;
import tw.tw360.dto.view.Games;
import tw.tw360.dto.view.User;
import tw.tw360.exception.DataNotFoundException;
import tw.tw360.exception.SendSMSFailException;
import tw.tw360.service.GamesManager;
import tw.tw360.service.MemberHomeManager;
import tw.tw360.service.PayManager;
import tw.tw360.utils.HttpUtil;
import tw.tw360.utils.URL;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@Controller
@PropertySource("classpath:mission.properties")
public class MemberHomeAction extends BaseController {	
	private static final Logger LOG = LoggerFactory.getLogger(MemberHomeAction.class);
	
    @Autowired
    private MemberHomeManager memberHomeManager;

    @Autowired
    private GamesManager gamesManager;

    @Autowired
    private PayManager payManager;
    
    @Autowired
    private Environment environment;   

    @RequestMapping(value = "memberHome", produces = "application/json; charset=utf-8")
    public
    @ResponseBody
    Map<String, Object> findInfo(@RequestParam(value = "uid", required = false, defaultValue = "") String uid,
                                 @RequestParam(value = "token", required = false, defaultValue = "") String token,
                                 @RequestParam(value = "appGameCode", required = false, defaultValue = "") String appGameCode) throws Exception {

        Map<String, Object> data = new LinkedHashMap<String, Object>();

        if (StringUtils.isBlank(uid) || StringUtils.isBlank(token) || StringUtils.isBlank(appGameCode)) {
            return result(Config.PARAMETER_ERROR, Config.RETURN_RESULT.get(Config.PARAMETER_ERROR), data);
        }

        // token 檢核
        validate(appGameCode, token, uid);

        MemberHome memberHome = memberHomeManager.findInfo(uid);
//        Integer bonus = null;
        if (null != memberHome) {
//            if (memberHomeManager.checkFirstBonus(uid)) {
//                bonus = memberHomeManager.getBonus(environment.getProperty("firstLoginBonus"));
//                memberHomeManager.addBonus(uid, environment.getProperty("firstLoginBonus"), bonus);
//                memberHome.setFirstBonus(bonus);
//            } else {
//                if ("鑽石VIP".equals(memberHome.getLevel())) {
//                    if (!memberHomeManager.checkAddBonusToday(uid)) {
//                        bonus = memberHomeManager.getBonus(environment.getProperty("diamondVipBonus"));
//                        memberHomeManager.addBonus(uid, environment.getProperty("diamondVipBonus"), bonus);
//                        memberHome.setVipBonus(bonus);
//                    }
//                } else {
//                    if (!memberHomeManager.checkAddBonusToday(uid)) {
//                        bonus = memberHomeManager.getBonus(environment.getProperty("dailyBonus"));
//                        memberHomeManager.addBonus(uid, environment.getProperty("dailyBonus"), bonus);
//                        memberHome.setDailyBonus(bonus);
//                    }
//                }
//            }
        	
        	//資料遮蔽(2017/02/24取消)
        	//電話：＊號中間三碼遮蔽（例：0909***123）
        	//信箱：開頭留1碼，其餘皆為＊號遮蔽，＠之後全顯示（例：a*******@gmail.com）
        	
        	if(memberHome.getMobile()!=null && memberHome.getMobile().length() > 0){
        		StringBuilder mobile = new StringBuilder(memberHome.getMobile());
        		
        		mobile.replace(4, 7, "***");
        		memberHome.setMobile(mobile.toString());
        	}
        	if(memberHome.getEmail()!=null && memberHome.getEmail().length() > 0){
        		StringBuilder email = new StringBuilder(memberHome.getEmail());
        		
        		email.replace(1, email.indexOf("@"), "*******");
        		memberHome.setEmail(email.toString());
        	}
        	
        } else {
            memberHome = memberHomeManager.findNotVIPInfo(uid);
            if (null != memberHome) {
                memberHome.setLevel("一般");
//                if (memberHomeManager.checkFirstBonus(uid)) {
//                    bonus = memberHomeManager.getBonus(environment.getProperty("firstLoginBonus"));
//                    memberHomeManager.addBonus(uid, environment.getProperty("firstLoginBonus"), bonus);
//                    memberHome.setFirstBonus(bonus);
//                } else {
//                   if (!memberHomeManager.checkAddBonusToday(uid)) {
//                        bonus = memberHomeManager.getBonus(environment.getProperty("dailyBonus"));
//                        memberHomeManager.addBonus(uid, environment.getProperty("dailyBonus"), bonus);
//                        memberHome.setDailyBonus(bonus);
//                    }
//                }                
            }
            
        }        
        memberHome.setBonus(memberHomeManager.getBonusBalence(uid));
        data.put("info", memberHome);
        return result(Config.RETURN_SUCCESS_DO, Config.RETURN_RESULT.get(Config.RETURN_SUCCESS_DO), data);
    }

    /**
     * 儲值中心/客服中心-遊戲列表
     *
     * @param uid
     * @param type
     * @return
     * @throws Exception
     */
    @ResponseBody
    @RequestMapping(value = "gameList", produces = "application/json; charset=utf-8")
    public Map<String, Object> gameList(@RequestParam(value = "uid", required = false, defaultValue = "") String uid) throws Exception {
        Map<String, Object> data = new LinkedHashMap<String, Object>();
        List<Games> gamesList = gamesManager.getGameCodeAndName(uid);
        data.put("gameNameList", gamesList);
        return result(Config.RETURN_SUCCESS_DO, Config.RETURN_RESULT.get(Config.RETURN_SUCCESS_DO), data);
    }

    /**
     * 儲值紀錄
     *
     * @param uid
     * @param page
     * @param appGameCode
     * @param token
     * @return
     * @throws Exception
     */
    @ResponseBody
    @RequestMapping(value = "payList", produces = "application/json; charset=utf-8")
    public Map<String, Object> payList(@RequestParam("uid") String uid,
    		@RequestParam(value="page", required=false, defaultValue="1") int pageNo,
    		@RequestParam("appGameCode") String appGameCode, @RequestParam("token") String token) throws Exception {

        Map<String, Object> data = new LinkedHashMap<String, Object>();

        if (StringUtils.isBlank(uid) || StringUtils.isBlank(token) || StringUtils.isBlank(appGameCode)) {
            return result(Config.PARAMETER_ERROR, Config.RETURN_RESULT.get(Config.PARAMETER_ERROR), data);
        }

        // token 檢核
        validate(appGameCode, token, uid, pageNo);

        Page<PayDto> list = payManager.getPayList(Integer.parseInt(uid), pageNo, 5);
        
        data.put("totalCount", list.getTotalCount());
        data.put("list", list.getResult());

        return result(Config.RETURN_SUCCESS_DO, Config.RETURN_RESULT.get(Config.RETURN_SUCCESS_DO), data);
    }

    @ResponseBody
    @RequestMapping(value = "updateUser", produces = "application/json; charset=utf-8")
    public Map<String, Object> updateUser(@RequestParam(value = "userId", required = false, defaultValue = "") String userId,
                                          @RequestParam(value = "email", required = false, defaultValue = "") String email,
                                          @RequestParam(value = "phone", required = false, defaultValue = "") String phone,
                                          @RequestParam(value = "authCode", required = false, defaultValue = "") String authCode,
                                          @RequestParam(value = "timestamp", required = false, defaultValue = "") String timestamp,
                                          @RequestParam(value = "appGameCode", required = false, defaultValue = "") String appGameCode,
                                          @RequestParam(value = "sign", required = false, defaultValue = "") String sign) throws Exception {
        Map<String, Object> data = new LinkedHashMap<String, Object>();

        if (StringUtils.isBlank(userId) || StringUtils.isBlank(appGameCode) || StringUtils.isBlank(sign) || (StringUtils.isBlank(email) && StringUtils.isBlank(phone)) || (StringUtils.isNotBlank(phone) && StringUtils.isBlank(authCode))) {
            return result(Config.PARAMETER_ERROR, Config.RETURN_RESULT.get(Config.PARAMETER_ERROR), data);
        }
        
        // token 檢核
        validate(appGameCode, sign, userId, timestamp);

        //email格式校验       
        if(StringUtils.isNotBlank(email)){
        	emailValidate(email);
        }        

        String phoneCode[] = URL.NATIONAL_PHONE_CODE.toString().split(",");
        
        if(!phone.equals("")){
        	for(int i=0;i<phoneCode.length;i++){
            	if(phone.substring(0, 1).equals(" ")){
            		if(phone.length() >= phoneCode[i].length()+1){
            			if(phoneCode[i].substring(1).equals(phone.substring(1, phoneCode[i].length()))){
                			if(phoneCode[i].equals("+886")){                				
                				if(phone.substring(phoneCode[i].length(), phoneCode[i].length()+1).equals("0")){
                    				phone = phone.substring(phoneCode[i].length());
                    			}else{
                    				phone = "0"+phone.substring(phoneCode[i].length());
                    			}
                			}else{
                				phone = phone.substring(phoneCode[i].length());
                			}
                    	}
            		}else{
            			phone = phone.trim();
            		}
            	}else{
            		if(phone.length() >= phoneCode[i].length()+1){
	            		if(phoneCode[i].equals(phone.substring(0, phoneCode[i].length()))){
	            			if(phoneCode[i].equals("+886")){
	            				if(phone.substring(phoneCode[i].length(), phoneCode[i].length()+1).equals("0")){
	            					phone = phone.substring(phoneCode[i].length());
	            				}else{
	            					phone = "0"+phone.substring(phoneCode[i].length());
	            				}
	            			}else{
	            				phone = phone.substring(phoneCode[i].length());
	            			}
	            		}
            		}else{
            			phone = phone.replace("+", "");
            		}
            	}
            }
        }
        
        if (memberHomeManager.updateUser(Integer.parseInt(userId), email, phone, authCode)) {
            return result(Config.RETURN_SUCCESS_SSI, Config.RETURN_RESULT.get(Config.RETURN_SUCCESS_DO), data);
        } else {
            return result(Config.RETURN_SYSTEM_ERROR, Config.RETURN_RESULT.get(Config.RETURN_SYSTEM_ERROR), data);
        }
    }

    @ResponseBody
    @RequestMapping(value = "forgotPwd", produces = "application/json; charset=utf-8")
    public Map<String, Object> forgotPwd(@RequestParam(value = "sign", required = false, defaultValue = "") String sign,
    		 							 @RequestParam(value = "timestamp", required = false, defaultValue = "") String timestamp,
    		 							 @RequestParam(value = "loginName", required = false, defaultValue = "") String loginName,
    		 							 @RequestParam(value = "appGameCode", required = false, defaultValue = "") String appGameCode,    		 							 
    		 							 @RequestParam(value = "email", required = false, defaultValue = "") String email,
    		 							 @RequestParam(value = "phone", required = false, defaultValue = "") String phone) throws Exception {
    	Map<String, Object> data = new LinkedHashMap<String, Object>();
    	
    	if(StringUtils.isBlank(sign) || StringUtils.isBlank(timestamp) || StringUtils.isBlank(loginName) || StringUtils.isBlank(appGameCode) || (StringUtils.isBlank(email) && StringUtils.isBlank(phone))){
    		return result(Config.PARAMETER_ERROR, Config.RETURN_RESULT.get(Config.PARAMETER_ERROR), data);
    	}
    	
    	//email格式校验
    	if(StringUtils.isNotBlank(email)){
    		emailValidate(email);
    	}
    	
    	// token 檢核
        validate(appGameCode, sign, timestamp, loginName);
        boolean sendSMS = false;
        User user = memberHomeManager.checkUser(loginName, email, phone);
        
        if(user != null){
        	
        	if(StringUtils.isNotBlank(email)){
        		if(!memberHomeManager.sendPwdEmail(email)){        			
        			return result(Config.RETURN_SYSTEM_ERROR, Config.RETURN_RESULT.get(Config.RETURN_SYSTEM_ERROR), data);
        		}
        	}else{
        		if(memberHomeManager.checkAuthCode(user.getUserid())){
                	return result(Config.SEND_SMS_TIME_LIMIT_FAIL, Config.RETURN_RESULT.get(Config.SEND_SMS_TIME_LIMIT_FAIL), data);
                }
        		
        		if(!memberHomeManager.sendPwd(user.getUserid(), phone)){
        			return result(Config.RETURN_SYSTEM_ERROR, Config.RETURN_RESULT.get(Config.RETURN_SYSTEM_ERROR), data);
        		}else{
        			sendSMS = true;
        		}
        	}
        	
        	if(memberHomeManager.updatePwd(user.getUserid(), sendSMS)){
        		//清cache
        		Map<String,String> map = new HashMap<String,String>();
        		map.put("gameCode", "app");
        		map.put("loginName", user.getUserName().toLowerCase());
        		
        		String postResult = HttpUtil.sendPost(URL.CLEAN_CACHE_URL.toString(), map);
        		
        		LOG.info("[MemberHomeAction][forgotPwd][cleancache]-"+postResult);
				
        		return result(Config.RETURN_SUCCESS_SSI, Config.RETURN_RESULT.get(Config.RETURN_SUCCESS_SSI), data);
        	}else{        		
        		return result(Config.RETURN_SYSTEM_ERROR, Config.RETURN_RESULT.get(Config.RETURN_SYSTEM_ERROR), data);
        	}
        }else{
        	LOG.info("[MemberHomeManager.forgotPwd][userName:" + loginName + " DataNotFoundException]");
        	throw new DataNotFoundException();
        }
    }
    
    @ResponseBody
    @RequestMapping(value = "sendAuthCode", produces = "application/json; charset=utf-8")
    public Map<String, Object> sendAuthCode(@RequestParam(value = "userId", required=false, defaultValue = "") String userId,
                                            @RequestParam(value = "phone", required=false, defaultValue = "") String phone,
                                            @RequestParam(value = "timestamp", required=false, defaultValue = "") String timestamp,
                                            @RequestParam(value = "appGameCode", required=false, defaultValue = "") String appGameCode,
                                            @RequestParam(value = "sign", required=false, defaultValue = "") String sign) throws Exception {
        Map<String, Object> data = new LinkedHashMap<String, Object>();
        if (StringUtils.isBlank(userId) || StringUtils.isBlank(phone) || StringUtils.isBlank(appGameCode) || StringUtils.isBlank(timestamp) || StringUtils.isBlank(sign)) {
            return result(Config.PARAMETER_ERROR, Config.RETURN_RESULT.get(Config.PARAMETER_ERROR), data);
        }
        
        // token 檢核
        validate(appGameCode, sign, userId, phone, timestamp);
        
        String phoneCode[] = URL.NATIONAL_PHONE_CODE.toString().split(",");
        
        for(int i=0;i<phoneCode.length;i++){
        	if(phone.substring(0, 1).equals(" ")){
        		if(phoneCode[i].substring(1).equals(phone)){        			
            		return result(Config.PARAMETER_ERROR, Config.RETURN_RESULT.get(Config.PARAMETER_ERROR), data);
            	}
        		
        		if(phone.length() >= phoneCode[i].length()+1){
        			if(phone.substring(1, phoneCode[i].length()).equals(phoneCode[i].substring(1))){
            			if(phoneCode[i].equals("+886")){
            				if(phone.substring(phoneCode[i].length(), phoneCode[i].length()+1).equals("0")){
                				phone = phoneCode[i].trim()+phone.substring(phoneCode[i].length()+1);
                			}else{
                				phone = phone.trim();
                			}
            			}else{
            				phone = phone.trim();
            			}
                	}
        		}else{
        			phone = phone.trim();
        		}
        	}else{
        		if(phoneCode[i].equals(phone)){
            		return result(Config.PARAMETER_ERROR, Config.RETURN_RESULT.get(Config.PARAMETER_ERROR), data);
            	}
        		
        		if(phone.length() >= phoneCode[i].length()+1){
	        		if(phone.substring(0, phoneCode[i].length()).equals(phoneCode[i])){
	        			if(phoneCode[i].equals("+886")){
	        				if(phone.substring(phoneCode[i].length(), phoneCode[i].length()+1).equals("0")){
	            				
	            				phone = phoneCode[i].replace("+", "")+phone.substring(phoneCode[i].length()+1);
	            			}else{
	            				phone = phone.replace("+", "");
	            			}
	    				}else{
	    					phone = phone.replace("+", "");
	    				}
	            	}
        		}else{
        			phone = phone.replace("+", "");
        		}
        	}
        }
        
        if(memberHomeManager.checkAuthCode(Long.parseLong(userId))){
        	return result(Config.SEND_SMS_TIME_LIMIT_FAIL, Config.RETURN_RESULT.get(Config.SEND_SMS_TIME_LIMIT_FAIL), data);
        }
        
        if (memberHomeManager.sendAuthCode(Long.parseLong(userId), phone)) {
            return result(Config.RETURN_SUCCESS_SSI, Config.RETURN_RESULT.get(Config.RETURN_SUCCESS_DO), data);
        } else {        	
			LOG.info("[MemberHomeManager.sendAuthCode][userId:" + userId + " SendSMSFailException]");
			throw new SendSMSFailException();
        }
    }
    
    @ResponseBody
    @RequestMapping(value = "upgradeCheck", produces = "application/json; charset=utf-8")
    public Map<String, Object> upgradeCheck(@RequestParam(value = "gameCode", required = false, defaultValue = "") String gameCode,
                                            @RequestParam(value = "version", required = false, defaultValue = "") String version) throws Exception {
    	Map<String, Object> data = new LinkedHashMap<String, Object>();
        
    	if (StringUtils.isBlank(gameCode) || StringUtils.isBlank(version) ) {
            return result(Config.PARAMETER_ERROR, Config.RETURN_RESULT.get(Config.PARAMETER_ERROR), data);
        }

        String storeUrl = memberHomeManager.checkAppVersion(gameCode, version);
        
        data.put("storeUrl", storeUrl);       
        return result(Config.RETURN_SUCCESS_DO, Config.RETURN_RESULT.get(Config.RETURN_SUCCESS_DO), data);       
    }
    
}