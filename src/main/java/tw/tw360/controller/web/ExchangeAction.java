package tw.tw360.controller.web;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import tw.tw360.constants.Config;
import tw.tw360.controller.BaseController;
import tw.tw360.exception.ExchangeTakenException;
import tw.tw360.exception.IDFAException;
import tw.tw360.service.ExchangeManager;

/**
*
*  API 28
* @author Beck
*
*/
@Controller
public class ExchangeAction extends BaseController {
	
	@Autowired
	private ExchangeManager exchangeManager;
	
	@RequestMapping(value="exchange", produces = "application/json; charset=utf-8")
	public @ResponseBody Map<String, Object> exchange(HttpServletRequest request, 
			@RequestParam(value="id", required = false, defaultValue="0") long id,
			String appGameCode, String uid, String deviceId, String token) throws Exception {
		Map<String, Object> result = new HashMap<String, Object>();
		
		String resultCode = Config.RETURN_SUCCESS_DO;
		
		if(StringUtils.isBlank(deviceId)){
			throw new IDFAException();
		}
		
		if(id <= 0 || StringUtils.isBlank(appGameCode) || StringUtils.isBlank(uid) || StringUtils.isBlank(token)){
			return result(Config.PARAMETER_ERROR, Config.RETURN_RESULT.get(Config.PARAMETER_ERROR), result);
		}
		
		validate(appGameCode, token, uid, id);
		
		// 用戶是否已領過該任務
		if (exchangeManager.findExchangeRecord(uid, id) != null) {
			throw new ExchangeTakenException();
		}
		
		int bonus = exchangeManager.getBonusBalence(uid);
		
		synchronized(this){
			String sn = exchangeManager.exchange(uid, id, deviceId, getIpAddr(request));
			
			if(Config.RETURN_RESULT.get(sn) != null) {
				resultCode = sn;
			} else {
				result.put("sn", sn);
			}
		}
		
		result.put("code", resultCode);
		result.put("message", Config.RETURN_RESULT.get(resultCode));
		result.put("bonus", bonus);
		return result;				
	}
	
	protected String getIpAddr(HttpServletRequest request) {
        String ip = request.getHeader( "x-forwarded-for");
         if (StringUtils. isEmpty(ip) || "unknown".equalsIgnoreCase( ip)) {
                     ip = request.getHeader( "Proxy-Client-IP");
        }
         if (StringUtils. isEmpty(ip) || "unknown".equalsIgnoreCase( ip)) {
                     ip = request.getHeader( "WL-Proxy-Client-IP");
        }
         if (StringUtils. isEmpty(ip) || "unknown".equalsIgnoreCase( ip)) {
                     ip = request.getRemoteAddr();
                     if( ip.equals( "127.0.0.1")){
                                InetAddress inet = null;
                                 try {
                                             inet = InetAddress. getLocalHost();
                                } catch (UnknownHostException e) {
                                             e.printStackTrace();
                                }
                                 ip = inet.getHostAddress();
                    }
        }
         return ip .indexOf("," ) > -1 ? ip .substring(0, ip.indexOf(",")) : ip;
	}

	@RequestMapping(value="exchangeDetail", produces = "application/json; charset=utf-8")
	public @ResponseBody Map<String, Object> exchangeDetail(String appGameCode, String uid, long id, String token) throws Exception {
		Map<String, Object> result = new HashMap<String, Object>();
				
		String resultCode = Config.RETURN_SUCCESS_DO;
		
		validate(appGameCode, token, uid, id);
		
		Map<String, Object> detail = exchangeManager.findExchangeDetail(uid, id);
		result.put("detail", detail);
		result.put("code", resultCode);
		result.put("message", Config.RETURN_RESULT.get(resultCode));
		return result;		
	}
}