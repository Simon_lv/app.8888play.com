package tw.tw360.common.net;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.SocketException;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPReply;
import org.apache.log4j.Logger;
import org.bouncycastle.util.Strings;

public class FtpUtil {
	
	/** logger */
	protected static Logger logger = Logger.getLogger(FtpUtil.class);
		
	private static FTPClient ftp;    
	private static final String HOST = "cdn-ftp.idc.hinet.net";
	private static final int PORT = 21;
	private static final String username = "8888picupload";
	private static final String password = "PDoM9Wj7Tuh^";

	private static FTPClient connect(String path, String addr, int port,
			String username, String password) throws SocketException,
			IOException {
		FTPClient ftp = new FTPClient();
		ftp.setControlEncoding("utf8");
		int reply;
		ftp.connect(addr, port);
		ftp.login(username, password);
		ftp.setFileType(FTPClient.BINARY_FILE_TYPE);
		reply = ftp.getReplyCode();
		if (!FTPReply.isPositiveCompletion(reply)) {
			ftp.disconnect();
			return ftp;
		}
		ftp.changeWorkingDirectory("/");
		String[] paths = path.split("/");
		for (int i = 0; i < paths.length; i++) {
			if (StringUtils.isNotBlank(paths[i])) {
				ftp.makeDirectory(paths[i]);
				ftp.changeWorkingDirectory(paths[i]);
			}
		}
		System.out.println("ftp directory: " + path);
		ftp.changeWorkingDirectory(path);

		System.out.println("ftp current directory: "
				+ ftp.printWorkingDirectory());
		return ftp;
	}

	   
	/**  
	 * File transfer
	 *   
	 * @param file   
	 * @throws Exception  
	 */    
	private void transportFile(String folder, File file) throws Exception{
		connect(folder, HOST, PORT, username, password);
			
	    if(file.isDirectory()){           
	    	ftp.makeDirectory(file.getName());                
	        ftp.changeWorkingDirectory(file.getName());      
	        String[] files = file.list();             
	     
	        for(int i = 0; i < files.length; i++) {      
	            File file1 = new File(file.getPath()+"\\"+files[i] );      
	         
	            if(file1.isDirectory()){      
	            	transportFile(folder,file1);      
	                ftp.changeToParentDirectory();      
	            }
	            else {                    
	                File file2 = new File(file.getPath()+"\\"+files[i]);      
	                FileInputStream input = new FileInputStream(file2);      
	                ftp.storeFile(new String(file2.getName().getBytes("utf8"),"iso-8859-1"), input);      
	                input.close();                            
	            }                 
	        }      	       
	    }
	    else {      
	        File file2 = new File(file.getPath());      
	        FileInputStream input = new FileInputStream(file2);      
	        ftp.storeFile(file2.getName(), input);     
	         
	        input.close();
	        
	        boolean deleted = false;
	        
	        // Try to delete the file immediately
	        try {
                deleted = file.delete();
            } 
	        catch(SecurityException e) {
                // Do Nothing
            }

            // Else delete the file when the program ends
            if(deleted) {
            	logger.debug("file deleted");
            } 
            else {
            	file.deleteOnExit();
            	logger.debug("file scheduled for deletion");
            }
	    }      
	        
	    ftp.logout();
	    ftp.disconnect();
	}      	    	  
	 
	/**
	 * Upload
	 * 
	 * @param ftpFolder
	 * @param f
	 */
	public static void upload(String ftpFolder, File f) {
		UploadAsync uploadAsync = new UploadAsync(ftpFolder, f);
		Thread t = new Thread(uploadAsync);
		t.start();
	}
	    	   
	/**
	 * Upload Async
	 *
	 */
	static class UploadAsync implements Runnable {			
		private File file = null;
		private String ftpFolder = "default";
		
		public UploadAsync(String ftpFolder, File file) {
			this.ftpFolder = ftpFolder;
			this.file = file;
		}
		
		public void run() {				
			try {				
				logger.info("ftp upload start-->" + file.getPath());
				new FtpUtil().transportFile(ftpFolder, file);
				logger.info("ftp upload end	-->" + file.getPath());				
			} 
			catch(Exception e) {
				 e.printStackTrace();
			} 
		}
	}
	
	public static void upload8888(String path, File file) throws SocketException, IOException {
		FTPClient ftp = connect(path, HOST, PORT, username, password);
		FileInputStream input = new FileInputStream(file);
		ftp.enterLocalPassiveMode();
		ftp.storeFile(file.getName(), input);
		ftp.logout();
		ftp.disconnect();
	}

	
}