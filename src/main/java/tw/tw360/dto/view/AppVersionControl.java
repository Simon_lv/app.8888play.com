package tw.tw360.dto.view;

import java.util.Date;

import tw.tw360.dto.BaseDto;

public class AppVersionControl extends BaseDto {
	private static final long serialVersionUID = 1L;
	
	private String gameCode;
	private String version;
	private String storeUrl;
	private Integer isUpgrade;
	private Date createTime;
	private Integer status;
	
	public String getGameCode() {
		return gameCode;
	}
	public void setGameCode(String gameCode) {
		this.gameCode = gameCode;
	}
	public String getVersion() {
		return version;
	}
	public void setVersion(String version) {
		this.version = version;
	}
	public String getStoreUrl() {
		return storeUrl;
	}
	public void setStoreUrl(String storeUrl) {
		this.storeUrl = storeUrl;
	}
	public Integer getIsUpgrade() {
		return isUpgrade;
	}
	public void setIsUpgrade(Integer isUpgrade) {
		this.isUpgrade = isUpgrade;
	}
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	
}
