package tw.tw360.dto.view;

import java.util.Date;

import tw.tw360.dto.BaseDto;

public class NotifyList extends BaseDto {
	
	private static final long serialVersionUID = 1L;

	private String title;
	private Date date;
	private int order;
	private int status;
	
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}	
	public int getOrder() {
		return order;
	}
	public void setOrder(int order) {
		this.order = order;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}	
}