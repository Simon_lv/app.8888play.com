package tw.tw360.dto.view;

import tw.tw360.dto.BaseDto;

public class Apps extends BaseDto {

	private static final long serialVersionUID = 1L;

	private String gameCode;
	private Integer type;
	private String name;
	private String subTitle;
	private String packageName;
	private String iconUrl;
	private String apkUrl;
	private String iosUrl;	
	private String content;	
	private Integer order;	
	private Integer status;	
	
	public String getGameCode() {
		return gameCode;
	}
	public void setGameCode(String gameCode) {
		this.gameCode = gameCode;
	}
	public Integer getType() {
		return type;
	}
	public void setType(Integer type) {
		this.type = type;
	}
	public String getIconUrl() {
		return iconUrl;
	}
	public String getSubTitle() {
		return subTitle;
	}
	public void setSubTitle(String subTitle) {
		this.subTitle = subTitle;
	}
	public void setIconUrl(String iconUrl) {
		this.iconUrl = iconUrl;
	}	
	public String getApkUrl() {
		return apkUrl;
	}	
	public void setApkUrl(String apkUrl) {
		this.apkUrl = apkUrl;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public Integer getOrder() {
		return order;
	}
	public void setOrder(Integer order) {
		this.order = order;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPackageName() {
		return packageName;
	}
	public void setPackageName(String packageName) {
		this.packageName = packageName;
	}
	public String getIosUrl() {
		return iosUrl;
	}
	public void setIosUrl(String iosUrl) {
		this.iosUrl = iosUrl;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	
}