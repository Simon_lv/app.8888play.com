package tw.tw360.dto.view;

import java.util.Date;

import tw.tw360.dto.BaseDto;

public class ExchangeList extends BaseDto {

	private static final long serialVersionUID = 1L;
	
	private String gameCode;
	private int order;	
	private String name;
	private String sn;
	private String giftDesc;
	private Date startTime;
	private Date endTime;
	
	public String getGameCode() {
		return gameCode;
	}
	public void setGameCode(String gameCode) {
		this.gameCode = gameCode;
	}
	public int getOrder() {
		return order;
	}
	public void setOrder(int order) {
		this.order = order;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getSn() {
		return sn;
	}
	public void setSn(String sn) {
		this.sn = sn;
	}
	public String getGiftDesc() {
		return giftDesc;
	}
	public void setGiftDesc(String giftDesc) {
		this.giftDesc = giftDesc;
	}
	public Date getStartTime() {
		return startTime;
	}
	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}
	public Date getEndTime() {
		return endTime;
	}
	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}
}