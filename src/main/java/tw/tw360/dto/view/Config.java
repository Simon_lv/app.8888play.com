package tw.tw360.dto.view;

import java.util.Date;

import tw.tw360.dto.BaseDto;

public class Config extends BaseDto {
	private static final long serialVersionUID = 1L;
	
	private String viedoUrl;
	private String appVersion;
	private String appiosVersion;
	private Integer isPay;
	private Integer isBonus;
	public Integer getIsPay() {
		return isPay;
	}
	public void setIsPay(Integer isPay) {
		this.isPay = isPay;
	}
	public Integer getIsBonus() {
		return isBonus;
	}
	public void setIsBonus(Integer isBonus) {
		this.isBonus = isBonus;
	}
	private Date createTime;
	private Integer status;
	
	public String getViedoUrl() {
		return viedoUrl;
	}
	public void setViedoUrl(String viedoUrl) {
		this.viedoUrl = viedoUrl;
	}
	public String getAppVersion() {
		return appVersion;
	}
	public void setAppVersion(String appVersion) {
		this.appVersion = appVersion;
	}
	public String getAppiosVersion() {
		return appiosVersion;
	}
	public void setAppiosVersion(String appiosVersion) {
		this.appiosVersion = appiosVersion;
	}
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}	
	
}
