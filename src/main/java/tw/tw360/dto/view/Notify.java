package tw.tw360.dto.view;

import java.util.Date;

import tw.tw360.dto.BaseDto;

public class Notify extends BaseDto {
	
	private static final long serialVersionUID = 1L;
		
	private String title;
	private Date date;
	private String content;
	
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}	
}