package tw.tw360.dto.view;

import tw.tw360.dto.BaseDto;

public class User extends BaseDto {
	private static final long serialVersionUID = 1L;
	
	private Long uid;
	private Long userid;
	private String userName;
	private String email;
	private String telephone;
	
	public Long getUid() {
		return uid;
	}
	public void setUid(Long uid) {
		this.uid = uid;
	}
	public Long getUserid() {
		return userid;
	}
	public void setUserid(Long userid) {
		this.userid = userid;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getTelephone() {
		return telephone;
	}
	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}	
	
}
