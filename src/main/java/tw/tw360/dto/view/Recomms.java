package tw.tw360.dto.view;

import java.util.Date;

import tw.tw360.dto.BaseDto;

public class Recomms extends BaseDto {
	private static final long serialVersionUID = 1L;
	
	private String gameCode;
	private String bannerUrl;
	private Date createTime;
	private Integer status;
	
	public String getGameCode() {
		return gameCode;
	}
	public void setGameCode(String gameCode) {
		this.gameCode = gameCode;
	}	
	public String getBannerUrl() {
		return bannerUrl;
	}
	public void setBannerUrl(String bannerUrl) {
		this.bannerUrl = bannerUrl;
	}
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	
}
