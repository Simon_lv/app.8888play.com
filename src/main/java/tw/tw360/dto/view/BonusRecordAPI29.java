package tw.tw360.dto.view;

import java.io.Serializable;

public class BonusRecordAPI29 implements Serializable {
	private static final long serialVersionUID = 1L;

	private int id;
	private int order;
	private String date;
	private int point; // bonus/cost
	private String content;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getOrder() {
		return order;
	}

	public void setOrder(int order) {
		this.order = order;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public int getPoint() {
		return point;
	}

	public void setPoint(int point) {
		this.point = point;
	}
}
