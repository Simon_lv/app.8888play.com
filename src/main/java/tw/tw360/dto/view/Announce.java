package tw.tw360.dto.view;

import java.util.Date;

import tw.tw360.dto.BaseDto;

public class Announce extends BaseDto {

	private static final long serialVersionUID = 1L;

	private Integer type;
	private String title;
	private String contentUrl;
	private Date date;
	private String gameName;
	
	public Integer getType() {
		return type;
	}
	public void setType(Integer type) {
		this.type = type;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}	
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public String getContentUrl() {
		return contentUrl;
	}
	public void setContentUrl(String contentUrl) {
		this.contentUrl = contentUrl;
	}
	public String getGameName() {
		return gameName;
	}
	public void setGameName(String gameName) {
		this.gameName = gameName;
	}	
}