package tw.tw360.dto.view;

import java.util.Date;

import tw.tw360.dto.BaseDto;

public class GoodStuff extends BaseDto {
	private static final long serialVersionUID = 1L;
	
	private String gameName;
	private String gameCode;
	private String iconUrl;
	private String desc;
	private String content;
	private String usage;
	private Date endTime;
	private Integer status;
	private String remain;
		
	private Integer order;
	
	public String getGameName() {
		return gameName;
	}	
	public void setGameName(String gameName) {
		this.gameName = gameName;
	}
	public String getGameCode() {
		return gameCode;
	}
	public void setGameCode(String gameCode) {
		this.gameCode = gameCode;
	}	
	public Integer getOrder() {
		return order;
	}
	public void setOrder(Integer order) {
		this.order = order;
	}
	public String getRemain() {
		return remain;
	}
	public void setRemain(String remain) {
		this.remain = remain;
	}
	public String getIconUrl() {
		return iconUrl;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public void setIconUrl(String iconUrl) {
		this.iconUrl = iconUrl;
	}
	public String getDesc() {
		return desc;
	}
	public void setDesc(String desc) {
		this.desc = desc;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public String getUsage() {
		return usage;
	}
	public void setUsage(String usage) {
		this.usage = usage;
	}
	public Date getEndTime() {
		return endTime;
	}
	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}	
	
}
