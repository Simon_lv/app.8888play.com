package tw.tw360.dto.view;

import tw.tw360.dto.BaseDto;

public class Faq extends BaseDto {
	private static final long serialVersionUID = 1L;
	
	private String title;
	
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}	

}
