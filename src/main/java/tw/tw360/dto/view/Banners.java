package tw.tw360.dto.view;

import tw.tw360.dto.BaseDto;

public class Banners extends BaseDto {

	private static final long serialVersionUID = 1L;
	
	private String imgUrl;
	private String pageUrl;
	private Integer order;
	private String gameCode;	
	
	public String getImgUrl() {
		return imgUrl;
	}
	public void setImgUrl(String imgUrl) {
		this.imgUrl = imgUrl;
	}
	public String getPageUrl() {
		return pageUrl;
	}
	public void setPageUrl(String pageUrl) {
		this.pageUrl = pageUrl;
	}
	public Integer getOrder() {
		return order;
	}	
	public void setOrder(Integer order) {
		this.order = order;
	}
	public String getGameCode() {
		return gameCode;
	}
	public void setGameCode(String gameCode) {
		this.gameCode = gameCode;
	}	
}