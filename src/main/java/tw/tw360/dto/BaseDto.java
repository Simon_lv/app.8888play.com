package tw.tw360.dto;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

/**
 * DTO基类的标识
 * */
@JsonInclude(Include.NON_NULL)
public class BaseDto implements Serializable { 

	private static final long serialVersionUID = 1L;

	public static enum STATUS {		
		NONE(0), NORMAL(1), DELETE(2), READ(2), UNREAD(1);
		
		private int value;
		
		STATUS(int value) {
			this.value = value;
		}
		
		public int getValue() {
	        return value;
	    }
	}

	private Long id;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
}