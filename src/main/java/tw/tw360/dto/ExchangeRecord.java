package tw.tw360.dto;

import java.util.Date;

public class ExchangeRecord extends BaseDto {
	private static final long serialVersionUID = 1L;

	private long prizeId;
	private long prizeSnId;
	private String userId;
	private int cost;
	private String sourceIp;
	private String deviceId;
	private Date createTime;

	public long getPrizeId() {
		return prizeId;
	}

	public void setPrizeId(long prizeId) {
		this.prizeId = prizeId;
	}

	public long getPrizeSnId() {
		return prizeSnId;
	}

	public void setPrizeSnId(long prizeSnId) {
		this.prizeSnId = prizeSnId;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public int getCost() {
		return cost;
	}

	public void setCost(int cost) {
		this.cost = cost;
	}

	public String getSourceIp() {
		return sourceIp;
	}

	public void setSourceIp(String sourceIp) {
		this.sourceIp = sourceIp;
	}

	public String getDeviceId() {
		return deviceId;
	}

	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

}
