package tw.tw360.dto;

import java.util.Date;

public class GamePlayerProblem extends BaseDto {

	private static final long serialVersionUID = 1L;

	private long tgppid;
	private String gameCode;
	private String serverCode;
	private String roleid;
	private String palayName;
	private String uid;
	private Date createTime;
	private String questionsTitle;
	private String theQuestions;
	private Integer questionType;
	private Integer replyState;
	private Long tcspid;
	private String plateform;
	private String img;
	private Integer score;
	private String item1;
	private String item2;
	private String item3;
	private String item4;
	private String item5;
	private String item6;
	private Date item7;
	private String item8;
	private String item9;
	private String item10;
	
	private String deviceVer;
	private String deviceOs;
	private String deviceModel;
	
	public String getDeviceVer() {
		return deviceVer;
	}
	public void setDeviceVer(String deviceVer) {
		this.deviceVer = deviceVer;
	}
	public String getDeviceOs() {
		return deviceOs;
	}
	public void setDeviceOs(String deviceOs) {
		this.deviceOs = deviceOs;
	}
	public String getDeviceModel() {
		return deviceModel;
	}
	public void setDeviceModel(String deviceModel) {
		this.deviceModel = deviceModel;
	}
	public long getTgppid() {
		return tgppid;
	}
	public void setTgppid(long tgppid) {
		this.tgppid = tgppid;
	}
	public String getGameCode() {
		return gameCode;
	}
	public void setGameCode(String gameCode) {
		this.gameCode = gameCode;
	}
	public String getServerCode() {
		return serverCode;
	}
	public void setServerCode(String serverCode) {
		this.serverCode = serverCode;
	}
	public String getRoleid() {
		return roleid;
	}
	public void setRoleid(String roleid) {
		this.roleid = roleid;
	}
	public String getPalayName() {
		return palayName;
	}
	public void setPalayName(String palayName) {
		this.palayName = palayName;
	}
	public String getUid() {
		return uid;
	}
	public void setUid(String uid) {
		this.uid = uid;
	}
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	public String getQuestionsTitle() {
		return questionsTitle;
	}
	public void setQuestionsTitle(String questionsTitle) {
		this.questionsTitle = questionsTitle;
	}
	public String getTheQuestions() {
		return theQuestions;
	}
	public void setTheQuestions(String theQuestions) {
		this.theQuestions = theQuestions;
	}
	public Integer getQuestionType() {
		return questionType;
	}
	public void setQuestionType(Integer questionType) {
		this.questionType = questionType;
	}
	public Integer getReplyState() {
		return replyState;
	}
	public void setReplyState(Integer replyState) {
		this.replyState = replyState;
	}
	public Long getTcspid() {
		return tcspid;
	}
	public void setTcspid(Long tcspid) {
		this.tcspid = tcspid;
	}
	public String getPlateform() {
		return plateform;
	}
	public void setPlateform(String plateform) {
		this.plateform = plateform;
	}
	public String getImg() {
		return img;
	}
	public void setImg(String img) {
		this.img = img;
	}
	public Integer getScore() {
		return score;
	}
	public void setScore(Integer score) {
		this.score = score;
	}
	public String getItem1() {
		return item1;
	}
	public void setItem1(String item1) {
		this.item1 = item1;
	}
	public String getItem2() {
		return item2;
	}
	public void setItem2(String item2) {
		this.item2 = item2;
	}
	public String getItem3() {
		return item3;
	}
	public void setItem3(String item3) {
		this.item3 = item3;
	}
	public String getItem4() {
		return item4;
	}
	public void setItem4(String item4) {
		this.item4 = item4;
	}
	public String getItem5() {
		return item5;
	}
	public void setItem5(String item5) {
		this.item5 = item5;
	}
	public String getItem6() {
		return item6;
	}
	public void setItem6(String item6) {
		this.item6 = item6;
	}
	public Date getItem7() {
		return item7;
	}
	public void setItem7(Date item7) {
		this.item7 = item7;
	}
	public String getItem8() {
		return item8;
	}
	public void setItem8(String item8) {
		this.item8 = item8;
	}
	public String getItem9() {
		return item9;
	}
	public void setItem9(String item9) {
		this.item9 = item9;
	}
	public String getItem10() {
		return item10;
	}
	public void setItem10(String item10) {
		this.item10 = item10;
	}		
}