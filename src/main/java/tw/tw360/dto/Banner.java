package tw.tw360.dto;

import java.util.Date;

public class Banner extends BaseDto {
	
	private static final long serialVersionUID = 1L;
		
	private Integer position;
	private String title;
	private String bannerUrl;
	private String linkUrl;
	private Integer orderIndex;
	private Date startTime;
	private Date endTime;
	private Date createTime;
	private Integer status;
	private Integer isOpen;
	private String gameCode;
	
	public void setPosition(Integer value) {
		this.position = value;
	}

	public Integer getPosition() {
		return this.position;
	}

	public void setTitle(String value) {
		this.title = value;
	}

	public String getTitle() {
		return this.title;
	}

	public void setBannerUrl(String value) {
		this.bannerUrl = value;
	}

	public String getBannerUrl() {
		return this.bannerUrl;
	}

	public void setLinkUrl(String value) {
		this.linkUrl = value;
	}

	public String getLinkUrl() {
		return this.linkUrl;
	}

	public void setOrderIndex(Integer value) {
		this.orderIndex = value;
	}

	public Integer getOrderIndex() {
		return this.orderIndex;
	}

	public void setStartTime(Date value) {
		this.startTime = value;
	}

	public Date getStartTime() {
		return this.startTime;
	}

	public void setEndTime(Date value) {
		this.endTime = value;
	}

	public Date getEndTime() {
		return this.endTime;
	}

	public void setCreateTime(Date value) {
		this.createTime = value;
	}

	public Date getCreateTime() {
		return this.createTime;
	}

	public void setStatus(Integer value) {
		this.status = value;
	}

	public Integer getStatus() {
		return this.status;
	}

	public void setIsOpen(Integer value) {
		this.isOpen = value;
	}

	public Integer getIsOpen() {
		return this.isOpen;
	}
	
	public String getGameCode() {
		return gameCode;
	}

	public void setGameCode(String gameCode) {
		this.gameCode = gameCode;
	}
}