package tw.tw360.dto;

import java.sql.Timestamp;

public class PrizeSn extends BaseDto {
	
	private static final long serialVersionUID = 1L;
	
	private int PrizeId;
	private String sn;
	private String userId;
	private String sourceIp;
	private String deviceId;
	private Timestamp receiveTime;
	private Timestamp createTime;
	
	public int getPrizeId() {
		return PrizeId;
	}
	public void setPrizeId(int prizeId) {
		PrizeId = prizeId;
	}
	public String getSn() {
		return sn;
	}
	public void setSn(String sn) {
		this.sn = sn;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getSourceIp() {
		return sourceIp;
	}
	public void setSourceIp(String sourceIp) {
		this.sourceIp = sourceIp;
	}
	public String getDeviceId() {
		return deviceId;
	}
	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}
	public Timestamp getReceiveTime() {
		return receiveTime;
	}
	public void setReceiveTime(Timestamp receiveTime) {
		this.receiveTime = receiveTime;
	}
	public Timestamp getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Timestamp createTime) {
		this.createTime = createTime;
	}	
}