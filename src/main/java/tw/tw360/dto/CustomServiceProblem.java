package tw.tw360.dto;

import java.util.Date;

public class CustomServiceProblem extends BaseDto {

	private static final long serialVersionUID = 1L;

	private long tcspid;
	private String gameCode;
	private String serverCode;
	private long tgppid;
	private Date replyTime;
	private String replyUid;
	private String replyContent;
	private String plateform;
	private String item1;
	private String item2;
	private String item3;
	private String item4;
	private String item5;
	
	public long getTcspid() {
		return tcspid;
	}
	public void setTcspid(long tcspid) {
		this.tcspid = tcspid;
	}
	public String getGameCode() {
		return gameCode;
	}
	public void setGameCode(String gameCode) {
		this.gameCode = gameCode;
	}
	public String getServerCode() {
		return serverCode;
	}
	public void setServerCode(String serverCode) {
		this.serverCode = serverCode;
	}
	public long getTgppid() {
		return tgppid;
	}
	public void setTgppid(long tgppid) {
		this.tgppid = tgppid;
	}
	public Date getReplyTime() {
		return replyTime;
	}
	public void setReplyTime(Date replyTime) {
		this.replyTime = replyTime;
	}
	public String getReplyUid() {
		return replyUid;
	}
	public void setReplyUid(String replyUid) {
		this.replyUid = replyUid;
	}
	public String getReplyContent() {
		return replyContent;
	}
	public void setReplyContent(String replyContent) {
		this.replyContent = replyContent;
	}
	public String getPlateform() {
		return plateform;
	}
	public void setPlateform(String plateform) {
		this.plateform = plateform;
	}
	public String getItem1() {
		return item1;
	}
	public void setItem1(String item1) {
		this.item1 = item1;
	}
	public String getItem2() {
		return item2;
	}
	public void setItem2(String item2) {
		this.item2 = item2;
	}
	public String getItem3() {
		return item3;
	}
	public void setItem3(String item3) {
		this.item3 = item3;
	}
	public String getItem4() {
		return item4;
	}
	public void setItem4(String item4) {
		this.item4 = item4;
	}
	public String getItem5() {
		return item5;
	}
	public void setItem5(String item5) {
		this.item5 = item5;
	}		
}