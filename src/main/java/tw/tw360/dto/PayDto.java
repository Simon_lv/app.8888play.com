package tw.tw360.dto;

public class PayDto extends BaseDto {
	
	private static final long serialVersionUID = 1L;
	
	private String orderId;
	private Integer stone;	
	private String time;
	 
	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public Integer getStone() {
		return stone;
	}

	public void setStone(Integer stone) {
		this.stone = stone;
	}
	
}