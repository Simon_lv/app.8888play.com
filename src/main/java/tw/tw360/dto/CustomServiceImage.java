package tw.tw360.dto;

import java.util.Date;

public class CustomServiceImage extends BaseDto {

	private static final long serialVersionUID = 1L;

	private long tcspid;
	private String url;
	private Date createTime;
	
	public long getTcspid() {
		return tcspid;
	}
	public void setTcspid(long tcspid) {
		this.tcspid = tcspid;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}		
}