package tw.tw360.dto;

import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;

public class BonusRecord extends BaseDto {
	private static final long serialVersionUID = 1L;

	private int missionId;
	private String userId;
	private int bonus;
	private String sourceIp;
	private String deviceId;
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date createTime;

	public int getMissionId() {
		return missionId;
	}

	public void setMissionId(int missionId) {
		this.missionId = missionId;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public int getBonus() {
		return bonus;
	}

	public void setBonus(int bonus) {
		this.bonus = bonus;
	}

	public String getSourceIp() {
		return sourceIp;
	}

	public void setSourceIp(String sourceIp) {
		this.sourceIp = sourceIp;
	}

	public String getDeviceId() {
		return deviceId;
	}

	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

}
