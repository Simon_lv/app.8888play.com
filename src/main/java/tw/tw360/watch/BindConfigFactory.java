package tw.tw360.watch;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Properties;
import java.util.Timer;
import java.util.TimerTask;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class BindConfigFactory {
	private static BindConfigFactory instance = null;
	private static String location = "/bindConfig.properties";
	private static long period = 60000;
	private static Properties bindConfigProps = new Properties();
	private static final Logger LOG = LoggerFactory.getLogger(BindConfigFactory.class);

	
    protected BindConfigFactory() {
    	URL url = this.getClass().getResource(location);
    	File propFile = null;
		try {
			propFile = new File(url.toURI());
			loadData(propFile);
		} catch (URISyntaxException e) {
			LOG.error("BindConfigFactory error :", e);
			e.printStackTrace();
		}

		TimerTask task = new FileWatcher( propFile ) {
		     protected void onChange( File file ) {
		    	 bindConfigProps.clear();
		    	 loadData(file);
		    	
		         LOG.debug( "File "+ file.getName() +" have change !" );
		     }
		};
		
		Timer timer = new Timer();
  	    // repeat the check every second
  	    timer.schedule( task , 0, period );

    }
      
    protected void loadData(File file) {
    	InputStream inputStream = null;
		try {
			inputStream = new FileInputStream(file);
			bindConfigProps.load(inputStream); 
		} catch (FileNotFoundException e) {
			LOG.error("BindConfigFactory.loadData error :", e);
			e.printStackTrace();
		} catch (IOException e) {
			LOG.error("BindConfigFactory.loadData error :", e);
			e.printStackTrace();
		
		} finally {
			if (inputStream != null) {
				try {
					inputStream.close();
				} catch (IOException e) {
					LOG.error("BindConfigFactory.loadData error :", e);
					e.printStackTrace();
				}
			}
		} 
 
    }
    
    
    public static BindConfigFactory getInstance() {
        if(instance == null) {
           instance = new BindConfigFactory();
        }
        return instance;
    }
    
    
    public static Properties getBlockNameProps() {
		return getInstance().bindConfigProps;
	}

	public static void main(String args[]) {
    	
		Properties bnSet = BindConfigFactory.getBlockNameProps();
    	System.out.println(bnSet.get("ah"));
    }
 }
