package tw.tw360.watch;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Properties;
import java.util.Timer;
import java.util.TimerTask;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class PayConfigFactory {
	private static PayConfigFactory instance = null;
	private static String location = "/pay.properties";
	private static long period = 60000;
	private static Properties payConfigProps = new Properties();
	private static final Logger LOG = LoggerFactory.getLogger(PayConfigFactory.class);

	
    protected PayConfigFactory() {
    	
    	URL url = this.getClass().getResource(location);
    	File propFile = null;
		try {
			propFile = new File(url.toURI());
			loadData(propFile);
		} catch (URISyntaxException e) {
			LOG.error("PayConfigFactory error :", e);
			e.printStackTrace();
		}

		TimerTask task = new FileWatcher( propFile ) {
		     protected void onChange( File file ) {
		    	 payConfigProps.clear();
		    	 loadData(file);
		    	
		         LOG.debug( "File "+ file.getName() +" have change !" );
		     }
		};
		
		Timer timer = new Timer();
  	    // repeat the check every second
  	    timer.schedule( task , 0, period );

    }
      
    protected void loadData(File file) {
    	InputStream inputStream = null;
		try {
			inputStream = new FileInputStream(file);
			payConfigProps.load(inputStream); 
		} catch (FileNotFoundException e) {
			LOG.error("PayConfigFactory.loadData error :", e);
			e.printStackTrace();
		} catch (IOException e) {
			LOG.error("PayConfigFactory.loadData error :", e);
			e.printStackTrace();
		
		} finally {
			if (inputStream != null) {
				try {
					inputStream.close();
				} catch (IOException e) {
					LOG.error("PayConfigFactory.loadData error :", e);
					e.printStackTrace();
				}
			}
		} 
 
    }
    
    
    public static PayConfigFactory getInstance() {
        if(instance == null) {
           instance = new PayConfigFactory();
        }
        return instance;
    }
    
    
    public static Properties getProps() {
		return getInstance().payConfigProps;
	}

	public static void main(String args[]) {
    	
		Properties bnSet = PayConfigFactory.getProps();
    	System.out.println(bnSet.get("version"));
    }
 }
