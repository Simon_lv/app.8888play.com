package tw.tw360.constants;

import java.util.HashMap;

public class Config {

	public final static int PAGE_SIZE = 20;	
	public final static int ANNOUNCEMENT_SIZE = 8;
	
	public final static String FORGOT_PWD = "123456";
	
	public static final int UPLOAD_FILE_LIMIT = 500 * 1024; // 上傳檔案大小限制

	public static final String RETURN_SUCCESS_DO = "0000";
	public static final String RETURN_SUCCESS_SSI = "1000";
	public static final String RETURN_SYSTEM_ERROR = "9999";
	public static final String PARAMETER_ERROR = "9001";
	public static final String TOKEN_FAIL = "9002";
	public static final String BONUS_FAIL = "9003";
	public static final String PRIZE_SN_FAIL = "9004";
	public static final String PRIZE_FAIL = "9005";
	public static final String BONUS_TAKEN = "9006";
	public static final String UPLOAD_OVERSIZED = "9007";
	public static final String UPLOAD_EMPTY = "9008";
	public static final String DATA_NOT_FOUND = "9009";
	public static final String RETURN_PUSH_ANDROID_SUCCESS = "9010";
	public static final String RETURN_PUSH_IOS_SUCCESS = "9011";
	public static final String RETURN_PUSH_ANDROID_FAIL = "9012";
	public static final String RETURN_PUSH_IOS_FAIL = "9013";
	public static final String RETURN_PUSH_FAIL = "9014";
	public static final String EXCHANGE_TAKEN = "9015";
	public static final String NO_SUCH_MISSION_OR_GAMECODE = "9016";
	public static final String NO_DIAMOND_VIP = "9017";
	public static final String AUTHCODE_FAIL = "9018";
	//三十分鐘內重複寄送簡訊錯誤
	public static final String SEND_SMS_TIME_LIMIT_FAIL = "9019";
	public static final String SEND_SMS_FAIL = "9020";
	public static final String SEND_EMAIL_FAIL = "9021";
	public static final String EMAIL_FORMAT_FAIL = "9022";
	public static final String NO_BOND_FAIL = "9023";
	public static final String IDFA_FAIL = "9024";
	
	public static final HashMap<String, String> RETURN_RESULT = new HashMap<String, String>();

	static {
		RETURN_RESULT.put(RETURN_SUCCESS_DO, "成功");
		RETURN_RESULT.put(RETURN_SUCCESS_SSI, "成功");
		RETURN_RESULT.put(RETURN_SYSTEM_ERROR, "系統錯誤");
		RETURN_RESULT.put(PARAMETER_ERROR, "輸入錯誤");
		RETURN_RESULT.put(TOKEN_FAIL, "token fail");
		RETURN_RESULT.put(BONUS_FAIL, "紅利不夠");
		RETURN_RESULT.put(PRIZE_SN_FAIL, "序號已兌換完畢");
		RETURN_RESULT.put(PRIZE_FAIL, "獎勵時間已截止");
		RETURN_RESULT.put(BONUS_TAKEN, "您已經取得此紅利");
		RETURN_RESULT.put(UPLOAD_OVERSIZED, "上傳檔案需小於"+UPLOAD_FILE_LIMIT/1024+"KB");
		RETURN_RESULT.put(UPLOAD_EMPTY, "未上傳檔案");
		RETURN_RESULT.put(DATA_NOT_FOUND, "查無此資料");
		RETURN_RESULT.put(RETURN_PUSH_ANDROID_SUCCESS, "android推播成功");
		RETURN_RESULT.put(RETURN_PUSH_IOS_SUCCESS, "ios推播成功");
		RETURN_RESULT.put(RETURN_PUSH_ANDROID_FAIL, "android推播失敗");
		RETURN_RESULT.put(RETURN_PUSH_IOS_FAIL, "ios推播失敗");
		RETURN_RESULT.put(RETURN_PUSH_FAIL, "推播失敗");
		RETURN_RESULT.put(EXCHANGE_TAKEN, "您已經取得此紅利獎勵");
		RETURN_RESULT.put(NO_SUCH_MISSION_OR_GAMECODE, "查無此遊戲或遊戲任務");
		RETURN_RESULT.put(NO_DIAMOND_VIP, "抱歉，您並非鑽石VIP");
		RETURN_RESULT.put(AUTHCODE_FAIL, "驗證碼輸入錯誤");
		RETURN_RESULT.put(SEND_SMS_TIME_LIMIT_FAIL, "30分鐘內無法重複發送簡訊");
		RETURN_RESULT.put(SEND_SMS_FAIL, "簡訊發送失敗");
		RETURN_RESULT.put(SEND_EMAIL_FAIL, "郵件寄送失敗");
		RETURN_RESULT.put(EMAIL_FORMAT_FAIL, "郵件格式錯誤");
		RETURN_RESULT.put(NO_BOND_FAIL, "郵件或電話未綁定");
		RETURN_RESULT.put(IDFA_FAIL, "玩家可以前往「設定」>「隱私權」>「廣告」，將「限制廣告追蹤」之功能關閉後重試，感謝您的配合!");
	}
	
}