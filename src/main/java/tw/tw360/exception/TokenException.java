package tw.tw360.exception;

/**
 * Token 異常
 * 
 * @author Eric
 *
 */
public class TokenException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public TokenException() {
        super();
    }
}
