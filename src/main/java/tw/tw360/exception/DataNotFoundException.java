package tw.tw360.exception;

/**
 * Data Not Found 異常
 * 
 * @author Eric
 *
 */
public class DataNotFoundException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public DataNotFoundException() {
        super();
    }
}