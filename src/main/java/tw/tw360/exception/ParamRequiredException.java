package tw.tw360.exception;

/**
 * 必填參數缺漏
 * 
 * @author Eric
 *
 */
public class ParamRequiredException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public ParamRequiredException() {
        super();
    }
}