package tw.tw360.exception;

public class ExchangeTakenException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public ExchangeTakenException() {
        super();
    }
}
