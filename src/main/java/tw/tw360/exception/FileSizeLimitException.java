package tw.tw360.exception;

public class FileSizeLimitException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public FileSizeLimitException() {
        super();
    }
}