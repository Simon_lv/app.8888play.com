package tw.tw360.exception;

public class EmptyFileException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public EmptyFileException() {
        super();
    }
}