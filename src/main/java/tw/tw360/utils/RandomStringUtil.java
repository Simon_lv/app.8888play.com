package tw.tw360.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.UUID;

public class RandomStringUtil {

	private static final Logger logger = LoggerFactory.getLogger(RandomStringUtil.class);

	/**
	 * 产生5位随机数字
	 * @return
     */
	public static String generateCheckCode()
	{
		String chars = "0123456789";
		char[] rands = new char[5];
		for (int i = 0; i < 5; i++)
		{
			int rand = (int) (Math.random() * 10);
			rands[i] = chars.charAt(rand);
		}
		return   new  String(rands);
	}


    private static String[] chars = new String[] { "A", "B", "C", "D", "E", "F",
            "G", "H", "J", "K","L","M", "N", "P", "Q", "R", "S",
            "T", "U", "V", "W", "X", "Y", "Z", "2", "3", "4", "5",
            "6", "7", "8", "9"};

    /**
     * 生成8位英数组合字符串
     */
    public static String generateShortUuid() {
        StringBuffer shortBuffer = new StringBuffer();
        String uuid = UUID.randomUUID().toString().replace("-", "");
        for (int i = 0; i < 8; i++) {
            String str = uuid.substring(i * 4, i * 4 + 4);
            int x = Integer.parseInt(str, 16);
            shortBuffer.append(chars[x % 0x20]);
        }
        return shortBuffer.toString();

    }


    /**
     * 测试重复性 运行了1个小时没发现重复
     */
//    public static void main(String[] args) {
//        List<String> list = new ArrayList<>();
//        Integer i=0;
//        while(true){
//            //System.out.println(i);
//            i=i+1;
//            String str=RandomStringUtil.generateShortUuid();
//            System.out.println(str);
//            if(list.contains(str)){
//                break;
//            }
//            else{
//                list.add(str);
//            }
//
//        }
//
//        System.out.println(i);
//
//    }


}
