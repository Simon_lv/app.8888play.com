package tw.tw360.utils;


import org.jdom.Document;
import org.jdom.Element;
import org.jdom.input.SAXBuilder;
import org.tempuri.FpMsServiceLocator;
import org.tempuri.FpMsServiceSoap;

import java.io.StringReader;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

public class IteSmsUtil {

    private final String smsUser = "8888play";
    private final String smsPass = "8888play";
    private final SimpleDateFormat msdf = new SimpleDateFormat("yyyy/MM/dd HH:mm");

    /**
     * 銓力
     *
     * @param msg
     * @param mobile
     * @param validSec
     * @return ex:Map<String, String> retHM = this.sendSMS("簡訊內容",
     * zga.getMobile(), "7200", smsRecordService);
     * if ("0".equals(retHM.get("status"))) //表示簡訊發送成功
     */
    public Map<String, String> sendSMS(
            String msg, String mobile, String validSec) throws Exception {
        if (mobile.startsWith("+88609")) {
            mobile = mobile.substring(4);
//	    } else if (mobile.startsWith("+852") || mobile.startsWith("+853")) {
//	    	mobile = mobile.substring(1);
        }
        FpMsServiceLocator fpMsServiceLocator = new FpMsServiceLocator();

        FpMsServiceSoap stub = fpMsServiceLocator.getfpMsServiceSoap();
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.SECOND, Integer.parseInt(validSec));
        String response = stub.sendSMS(mobile, msg, smsUser, Base64.encode(smsPass.getBytes()), "", msdf.format(cal.getTime()), false);
        System.out.println("sendSms response: " + response);
        SAXBuilder bSAX = new SAXBuilder(false);
        Document docJDOM = bSAX.build(new StringReader(response));
        Element elmtRoot = docJDOM.getRootElement();
        String seqNo = elmtRoot.getChildText("SEQ");
        String errNo = elmtRoot.getChildText("ERR");
        Map<String, String> retHM = new HashMap<String, String>();
        retHM.put("MessageID", seqNo);
        if ("0".equals(errNo)) {
            retHM.put("UsedCredit", "1");
        }
        retHM.put("status", errNo);

        return retHM;
    }
}
