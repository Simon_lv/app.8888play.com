package tw.tw360.utils;

/**
 * URL for configuration.properties
 * <p>
 * String, Key of configuration.properties <br>
 * boolean, 是否支援測試機模式
 *
 */
public enum URL {
	
	/** 好康說明*/
	GOODSTUFF_DESCRIPTION_URL("goodStuff.description.url", false),	
	/** 我要提問 */
	FTP_8888APP_QUESTION("ftp.8888app.question", true),
	/** CDN */
	CDN_CONTEXT_ROOT("cdn.context.root", false),
	/** 公告內容 */
	ANNOUNCE_URL("announce.url", false),
	/** 首頁影片 */
	HOME_VIDEO_URL("home.video.url", false),
	
	CLEAN_CACHE_URL("clean.cache.url", true),
	
	NATIONAL_PHONE_CODE("national.phone.code", false),
	
	MISSION_BONUS_URL("mission.bonus.url", true)
	
	;
	
	private String key;
	
	private URL(String key, boolean testable) {
		//auto add "test_" prefix if running a test server
		if(isTest() && testable) {
		    this.key = "test." + key;
		}    
		else {
		    this.key = key;
		}    
	}

	/* (non-Javadoc)
	 * @see java.lang.Enum#toString()
	 */
	@Override
	public String toString() {
		return Configuration.getInstance().getProperty(key);
	}
	
	/**
	 * AP Server 是否測試機
	 * 
	 * @return
	 */
	private boolean isTest() {
		return "true".equalsIgnoreCase(Configuration.getInstance().getProperty("isTestServer"));
	}	
}