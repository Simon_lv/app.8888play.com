package tw.tw360.utils;

import java.io.File;

import org.apache.log4j.Logger;
import org.springframework.web.multipart.MultipartFile;

import tw.tw360.exception.EmptyFileException;
import tw.tw360.exception.FileSizeLimitException;
import tw.tw360.service.FileUploadService;

public class HandleFileUpload { 
	
	/** logger */
	protected static Logger logger = Logger.getLogger(HandleFileUpload.class);
	
	/**
	 * 檔案上傳
	 * 
	 * @param file
	 * @param remoteFilePath
	 * @param sizeLimit
	 * @throws Exception
	 */
	public static String upload(MultipartFile file, String remoteFilePath, int sizeLimit) throws Exception {
		if(!file.isEmpty()) {
        	if(file.getBytes().length > sizeLimit) {
        		throw new FileSizeLimitException();        		
        	}
        	
        	String filename = FileNameGenerator.byTime(file.getOriginalFilename());
        	
        	File file2Upload = new File(filename);
        	file.transferTo(file2Upload);
        	System.out.println("file2Upllad length:"+ file2Upload.length());
        	FileUploadService.toFtp(file2Upload, remoteFilePath);
        	
        	return remoteFilePath + "/" + filename;
        } 
        else {
        	throw new EmptyFileException();
        }        
	}	
}